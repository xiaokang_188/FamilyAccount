package cool.xiaokang.familyaccount.service.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cool.xiaokang.familyaccount.dao.InvestDao;
import cool.xiaokang.familyaccount.pojo.Invest;
import cool.xiaokang.familyaccount.service.InvestService;

@Service
@Transactional
public class InvestServiceImpl implements InvestService {
	@Autowired
	private InvestDao investDao;

	// 保存投资账单
	@Override
	public void save(Invest invest) {
		investDao.save(invest);
	}

	// 查找所有投资账单数据
	@Override
	public List<Invest> findList(DetachedCriteria dc) {
		return investDao.findByCriteria(dc);
	}

	// 根据id查询投资账单
	@Override
	public Invest findById(String invest_id) {
		return investDao.findById(invest_id);
	}

	// 修改投资账单
	@Override
	public void update(Invest invest) {
		investDao.update(invest);
	}

	// 根据id删除投资账单
	@Override
	public void deleteById(String invest_id) {
		investDao.executeUpdate("invest.delete", invest_id);
	}

	@Override
	public int getTotal() {
		DetachedCriteria dc = DetachedCriteria.forClass(Invest.class);
		dc.setProjection(Projections.rowCount());
		Integer totalCount = investDao.getTotalCount(dc);
		return totalCount;
	}

}
