package cool.xiaokang.familyaccount.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cool.xiaokang.familyaccount.dao.MenuDao;
import cool.xiaokang.familyaccount.dao.RoleDao;
import cool.xiaokang.familyaccount.pojo.Menu;
import cool.xiaokang.familyaccount.pojo.Role;
import cool.xiaokang.familyaccount.service.RoleService;
import cool.xiaokang.familyaccount.utils.PageBean;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {
	private static final Logger LOGGER = Logger.getRootLogger();
	@Autowired
	private RoleDao roleDao;
	@Autowired
	private MenuDao menuDao;

	// 添加角色
	public void save(Role role, String menuIds) {
		if (StringUtils.isNotBlank(menuIds)) {
			String[] menu_ids = menuIds.split(",");
			for (String menuId : menu_ids) {
				LOGGER.info("【RoleServiceImpl-save】>>>menuId:" + menuId);
				Menu menu = menuDao.load(menuId);
				role.getMenus().add(menu);
			}
		}
		roleDao.saveOrUpdate(role);
	}

	// 分页查询
	public void pageQuery(PageBean pb) {
		roleDao.pageQuery(pb);
	}

	@Override
	public List<Role> findAll() {
		return roleDao.findAll();
	}

	// 根据id查找角色
	public Role findById(String role_id) {
		return roleDao.findById(role_id);
	}

	// 修改
	public void update(Role role, String menuIds) {
		if (StringUtils.isNotBlank(menuIds)) {
			String[] menu_ids = menuIds.split(",");
			for (String menuId : menu_ids) {
				LOGGER.info("【RoleServiceImpl-update】>>>menuId:" + menuId);
				Menu menu = menuDao.load(menuId);
				role.getMenus().add(menu);
			}
		}
		roleDao.update(role);
	}
}
