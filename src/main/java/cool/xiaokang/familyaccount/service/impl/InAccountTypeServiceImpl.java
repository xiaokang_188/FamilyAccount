package cool.xiaokang.familyaccount.service.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cool.xiaokang.familyaccount.dao.InAccountTypeDao;
import cool.xiaokang.familyaccount.pojo.InAccountType;
import cool.xiaokang.familyaccount.service.InAccountTypeService;

@Service
@Transactional
public class InAccountTypeServiceImpl implements InAccountTypeService {

	@Autowired
	private InAccountTypeDao inAccountTypeDao;

	// 查找所有收入类型(没有任何条件)
	public List<InAccountType> findAll() {
		List<InAccountType> list = inAccountTypeDao.findAll();
		return list;
	}

	// 根据ID查询收入类型
	public InAccountType findById(Long inaccounttype_id) {
		InAccountType inAccountType = inAccountTypeDao.findById(inaccounttype_id);
		return inAccountType;
	}

	// 根据条件查询所有收入类型
	public List<InAccountType> findList(DetachedCriteria dc) {
		return inAccountTypeDao.findByCriteria(dc);
	}

	// 添加收入类型
	public void save(InAccountType inAccountType) {
		inAccountTypeDao.save(inAccountType);
	}

	// 修改收入类型
	public void update(InAccountType inAccountType) {
		inAccountTypeDao.update(inAccountType);
	}

	// 根据ID删除收入类型
	public void deleteById(Long inaccounttype_id) {
		inAccountTypeDao.executeUpdate("inaccounttype.delete", inaccounttype_id);
	}

	// 根据用户id查询所有收入账单类型名称
	public List<Object> findInAccountTypeName(Long user_id) {
		return inAccountTypeDao.findInAccountTypeName(user_id);
	}

	// 根据收入类型名查找收入类型实体
	@Override
	public InAccountType findByTypeNameAndUserId(String typename, Long user_id) {
		return inAccountTypeDao.findByTypeNameAndUserId(typename, user_id);
	}

}
