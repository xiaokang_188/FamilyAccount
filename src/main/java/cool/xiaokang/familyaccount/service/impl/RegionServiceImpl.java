package cool.xiaokang.familyaccount.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cool.xiaokang.familyaccount.dao.RegionDao;
import cool.xiaokang.familyaccount.pojo.Region;
import cool.xiaokang.familyaccount.service.RegionService;

@Service
@Transactional
public class RegionServiceImpl implements RegionService {

	@Autowired
	private RegionDao regionDao;

	// 查找所有大区
	public List<Region> findAll() {
		return regionDao.findAll();
	}

	@Override
	public Region findById(Integer region_id) {
		return regionDao.findById(region_id);
	}

	@Override
	public void save(Region region) {
		regionDao.save(region);
	}

	@Override
	public void update(Region region) {
		regionDao.update(region);
	}

	public void delete(String region_id) {
		regionDao.executeUpdate("region.delete", Integer.parseInt(region_id));
	}
}
