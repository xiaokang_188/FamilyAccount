package cool.xiaokang.familyaccount.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cool.xiaokang.familyaccount.dao.MenuDao;
import cool.xiaokang.familyaccount.pojo.Manager;
import cool.xiaokang.familyaccount.pojo.Menu;
import cool.xiaokang.familyaccount.service.MenuService;
import cool.xiaokang.familyaccount.utils.PageBean;

@Service
@Transactional
public class MenuServceImpl implements MenuService {

	@Autowired
	private MenuDao menuDao;

	public void pageQuery(PageBean pb) {
		menuDao.pageQuery(pb);
	}

	@Override
	public List<Menu> findAjax() {
		return menuDao.findAjax();
	}

	public List<Menu> findAll() {
		return menuDao.findAll();
	}

	@Override
	public void save(Menu menu) {
		Menu parent = menu.getParentMenu();
		if (parent == null || parent.getMenu_id().equals("")) {
			menu.setParentMenu(null);
		}
		menuDao.merge(menu);
	}

	@Override
	public Menu findById(String id) {
		return menuDao.load(id);
	}

	@Override
	public void update(Menu menu) {
		menuDao.updatepersist(menu);
		if (menu.getParentMenu() != null || !menu.getParentMenu().getMenu_id().equals("")) {
			String pid = menu.getParentMenu().getMenu_id();
			Menu parentMenu = new Menu(pid);
			menu.setParentMenu(parentMenu);
		} else {
			menu.setParentMenu(null);
		}
	}

	// 根据角色查询权限
	public List<Menu> findByRole(String role_id) {
		return menuDao.findByRole(role_id);
	}

	@Override
	public List<Menu> findMenu(Manager manager) {
		if (manager.getManager_name().equals("admin")) {
			return menuDao.findAllMenu();
		} else {
			return menuDao.findMenuByManagerId(manager.getManager_id());
		}
	}

	@Override
	public List<Menu> findMenuForRole() {
		return menuDao.findAllMenuForRole();
	}
}
