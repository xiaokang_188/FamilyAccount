package cool.xiaokang.familyaccount.service.impl;

import java.util.Date;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cool.xiaokang.familyaccount.dao.OutAccountDao;
import cool.xiaokang.familyaccount.dao.OutAccoutTypeDao;
import cool.xiaokang.familyaccount.pojo.OutAccount;
import cool.xiaokang.familyaccount.pojo.OutAccountType;
import cool.xiaokang.familyaccount.pojo.User;
import cool.xiaokang.familyaccount.service.OutAccountService;

@Service
@Transactional
public class OutAccountServiceImpl implements OutAccountService {
	@Autowired
	private OutAccountDao outAccountDao;
	@Autowired
	private OutAccoutTypeDao outAccoutTypeDao;

	@Override
	public void saveOutAccount(List<OutAccount> list, Long user_id) {
		for (OutAccount outAccount : list) {
			String typename = outAccount.getOutAccountType().getOutaccounttype_name();
			OutAccountType outAccountType = outAccoutTypeDao.findByTypeNameAndUserId(typename, user_id);
			if (outAccountType != null) {
				outAccount.setOutAccountType(outAccountType);
				outAccountDao.save(outAccount);
			} else {
				throw new RuntimeException("类型名称不匹配异常信息");
			}
		}

	}

	// 根据ID删除支出账单
	public void deleteById(Long outaccount_id) {
		outAccountDao.executeUpdate("outaccount.delete", outaccount_id);
	}

	// 添加支出账单
	public void save(OutAccount outAccount) {
		User user = outAccount.getUser();
		OutAccountType outAccountType = outAccoutTypeDao
				.findById(outAccount.getOutAccountType().getOutaccounttype_id());
		outAccount.setUser(user);
		outAccount.setOutAccountType(outAccountType);
		outAccount.setOutaccount_createtime(new Date());
		outAccountDao.save(outAccount);
	}

	// 通过条件查找支出账单
	public List<OutAccount> findList(DetachedCriteria dc) {
		List<OutAccount> list = outAccountDao.findByCriteria(dc);
		return list;
	}

	// 根据id查找支出账单
	public OutAccount findById(Long outaccount_id) {
		return outAccountDao.findById(outaccount_id);
	}

	// 修改支出账单
	public void update(OutAccount outAccount) {
		OutAccountType outtype = outAccoutTypeDao.findById(outAccount.getOutAccountType().getOutaccounttype_id());
		outAccount.setOutAccountType(outtype);
		outAccount.setOutaccount_createtime(new Date());
		outAccountDao.update(outAccount);
	}

	// 根据用户id和账单类型分类，查询当年或某年支出账单金额总和
	public List<Object> findOutAccountGroupByOutAccountTypeName(Long user_id, String year) {
		return outAccountDao.findOutAccountGroupByOutAccountTypeName(user_id, year);
	}

	// 根据用户id和账单类型分类，查询当年或某年各个月的支出账单金额总和
	public List<Object> findOutAccountWithYearly(Long user_id, String year) {
		return outAccountDao.findOutAccountWithYearly(user_id, year);
	}

	// 查询当前用户支出账单前6条
	public List<OutAccount> findSixOutAccount(Long user_id) {
		DetachedCriteria dc = DetachedCriteria.forClass(OutAccount.class);
		dc.add(Restrictions.eq("user.user_id", user_id));
		dc.addOrder(Order.desc("outaccount_money"));
		return outAccountDao.getPageList(dc, 0, 6);
	}

	// 查询当年当月支出总额
	public Double findCurrentMonthTotalMoney(Long user_id) {
		return outAccountDao.findCurrentMonthTotalMoney(user_id);
	}

	// 查询当年或某年各个月月支出平均额
	public List<Object> findPerMonthAvgMoney(Long user_id, String year) {
		return outAccountDao.findPerMonthAvgMoney(user_id, year);
	}

	// 统计
	public int getTotal() {
		DetachedCriteria dc = DetachedCriteria.forClass(OutAccount.class);
		dc.setProjection(Projections.rowCount());
		return outAccountDao.getTotalCount(dc);
	}
}
