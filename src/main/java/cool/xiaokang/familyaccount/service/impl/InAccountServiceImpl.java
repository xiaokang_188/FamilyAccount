package cool.xiaokang.familyaccount.service.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cool.xiaokang.familyaccount.dao.InAccountDao;
import cool.xiaokang.familyaccount.dao.InAccountTypeDao;
import cool.xiaokang.familyaccount.pojo.InAccount;
import cool.xiaokang.familyaccount.pojo.InAccountType;
import cool.xiaokang.familyaccount.service.InAccountService;

@Service
@Transactional
public class InAccountServiceImpl implements InAccountService {
	@Autowired
	private InAccountDao inAccountDao;
	@Autowired
	private InAccountTypeDao inAccountTypeDao;

	// 添加收入账单
	public void save(InAccount inAccount) {
		inAccountDao.save(inAccount);
	}

	// 批量添加收入账单
	public void saveInAccount(List<InAccount> list, Long user_id) {
		for (InAccount inAccount : list) {
			String typename = inAccount.getInAccountType().getInaccounttype_name();
			// 根据类型名称查找类型
			InAccountType inAccountType = inAccountTypeDao.findByTypeNameAndUserId(typename, user_id);
			if (inAccountType != null) {
				inAccount.setInAccountType(inAccountType);
				inAccountDao.save(inAccount);
			} else {
				throw new RuntimeException("类型名称不匹配异常信息");
			}
		}

	}

	// 查找所有收入账单数据
	public List<InAccount> findList(DetachedCriteria dc) {
		return inAccountDao.findByCriteria(dc);
	}

	// 根据ID删除收入账单
	public void deleteById(Long inaccount_id) {
		inAccountDao.executeUpdate("inaccount.delete", inaccount_id);
	}

	// 根据ID查找收入账单
	public InAccount findById(Long inaccount_id) {
		return inAccountDao.findById(inaccount_id);
	}

	// 修改收入账单
	public void update(InAccount inAccount) {
		inAccountDao.update(inAccount);
	}

	public List<Object> findInAccountGroupByInAccountTypeName(Long user_id, String year) {
		return inAccountDao.findInAccountGroupByInAccountTypeName(user_id, year);
	}

	public List<Object> findInAccountWithYearly(Long user_id, String year) {
		return inAccountDao.findInAccountWithYearly(user_id, year);
	}

	// 查询当前用户收入账单前6条
	public List<InAccount> findSixInAccount(Long user_id) {
		DetachedCriteria dc = DetachedCriteria.forClass(InAccount.class);
		dc.add(Restrictions.eq("user.user_id", user_id));
		dc.addOrder(Order.desc("inaccount_money"));
		return inAccountDao.getPageList(dc, 0, 6);
	}

	public Double findCurrentMonthTotalMoney(Long user_id) {
		return inAccountDao.findCurrentMonthTotalMoney(user_id);
	}

	public List<Object> findPerMonthAvgMoney(Long user_id, String year) {
		return inAccountDao.findPerMonthAvgMoney(user_id, year);
	}

	// 总记录数
	public int getTotal() {
		DetachedCriteria dc = DetachedCriteria.forClass(InAccount.class);
		dc.setProjection(Projections.rowCount());
		Integer totalCount = inAccountDao.getTotalCount(dc);
		dc.setProjection(null);
		return totalCount;
	}
}
