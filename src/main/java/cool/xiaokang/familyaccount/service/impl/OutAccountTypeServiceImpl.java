package cool.xiaokang.familyaccount.service.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cool.xiaokang.familyaccount.dao.OutAccoutTypeDao;
import cool.xiaokang.familyaccount.pojo.OutAccountType;
import cool.xiaokang.familyaccount.service.OutAccountTypeService;

@Service
@Transactional
public class OutAccountTypeServiceImpl implements OutAccountTypeService {
	@Autowired
	private OutAccoutTypeDao outAccountTypeDao;

	@Override
	public List<OutAccountType> findList(DetachedCriteria dc) {
		return outAccountTypeDao.findByCriteria(dc);
	}

	@Override
	public void save(OutAccountType outAccountType) {
		outAccountTypeDao.save(outAccountType);
	}

	@Override
	public OutAccountType findById(Long outaccounttype_id) {
		return outAccountTypeDao.findById(outaccounttype_id);
	}

	@Override
	public void update(OutAccountType outAccountType) {
		outAccountTypeDao.update(outAccountType);
	}

	@Override
	public void deleteById(Long outaccounttype_id) {
		outAccountTypeDao.executeUpdate("outaccounttype.delete", outaccounttype_id);
	}

	@Override
	public OutAccountType findByTypeNameAndUserId(String typename, Long user_id) {
		return outAccountTypeDao.findByTypeNameAndUserId(typename, user_id);
	}

	// 根据用户id查询所有支出账单类型名称
	public List<Object> findOutAccountTypeName(Long user_id) {
		return outAccountTypeDao.findOutAccountTypeName(user_id);
	}

}
