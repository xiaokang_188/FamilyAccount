package cool.xiaokang.familyaccount.service.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cool.xiaokang.familyaccount.dao.LoanDao;
import cool.xiaokang.familyaccount.pojo.Loan;
import cool.xiaokang.familyaccount.service.LoanService;

@Service
@Transactional
public class LoanServiceImpl implements LoanService {
	@Autowired
	private LoanDao loanDao;

	@Override
	public void saveLoan(Loan loan) {
		loanDao.save(loan);
	}

	@Override
	public List<Loan> findList(DetachedCriteria dc) {
		return loanDao.findByCriteria(dc);
	}

	// 根据ID查找借款账单
	@Override
	public Loan findById(String loan_id) {
		return loanDao.findById(loan_id);
	}

	// 修改借款账单
	@Override
	public void update(Loan loan) {
		loanDao.update(loan);
	}

	// 根据ID删除借款账单
	@Override
	public void deleteById(String loan_id) {
		loanDao.executeUpdate("loan.delete", loan_id);
	}

	// 统计
	public int getTotal() {
		DetachedCriteria dc = DetachedCriteria.forClass(Loan.class);
		dc.setProjection(Projections.rowCount());
		Integer totalCount = loanDao.getTotalCount(dc);
		dc.setProjection(null);
		return totalCount;
	}

}
