package cool.xiaokang.familyaccount.service;

import java.util.List;

import cool.xiaokang.familyaccount.pojo.Manager;
import cool.xiaokang.familyaccount.pojo.Menu;
import cool.xiaokang.familyaccount.utils.PageBean;

public interface MenuService {

	public void pageQuery(PageBean pb);

	public List<Menu> findAjax();

	public List<Menu> findAll();

	public void save(Menu menu);

	public Menu findById(String id);

	public void update(Menu menu);

	public List<Menu> findByRole(String role_id);

	public List<Menu> findMenu(Manager model);

	public List<Menu> findMenuForRole();

}
