package cool.xiaokang.familyaccount.service;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import cool.xiaokang.familyaccount.pojo.InAccount;

public interface InAccountService {

	/**
	 * 保存收入账单
	 */
	public void save(InAccount inAccount);

	/**
	 * 批量保存收入账单
	 * @param user_id 
	 */
	public void saveInAccount(List<InAccount> list, Long user_id);

	/**
	 * 查找所有收入账单数据
	 * 
	 * @param dc
	 */
	public List<InAccount> findList(DetachedCriteria dc);

	/**
	 * 根据ID删除收入账单
	 * 
	 * @param inaccount_id
	 */
	public void deleteById(Long inaccount_id);

	/**
	 * 根据ID查找收入账单
	 * 
	 * @param inaccount_id
	 * @return
	 */
	public InAccount findById(Long inaccount_id);

	/**
	 * 修改收入账单
	 * 
	 * @param inAccount
	 */
	public void update(InAccount inAccount);

	public List<Object> findInAccountGroupByInAccountTypeName(Long user_id, String year);

	public List<Object> findInAccountWithYearly(Long user_id, String year);

	public List<InAccount> findSixInAccount(Long user_id);

	public Double findCurrentMonthTotalMoney(Long user_id);

	public List<Object> findPerMonthAvgMoney(Long user_id, String year);

	public int getTotal();
}
