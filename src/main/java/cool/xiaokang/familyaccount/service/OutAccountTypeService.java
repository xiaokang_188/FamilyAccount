package cool.xiaokang.familyaccount.service;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import cool.xiaokang.familyaccount.pojo.OutAccountType;

public interface OutAccountTypeService {

	public List<OutAccountType> findList(DetachedCriteria dc);

	public void save(OutAccountType outAccountType);

	public OutAccountType findById(Long outaccounttype_id);

	public void update(OutAccountType outAccountType);

	public void deleteById(Long outaccounttype_id);

	public OutAccountType findByTypeNameAndUserId(String typename, Long user_id);

	public List<Object> findOutAccountTypeName(Long user_id);

}
