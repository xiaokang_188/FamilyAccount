package cool.xiaokang.familyaccount.service;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import cool.xiaokang.familyaccount.pojo.InAccountType;

public interface InAccountTypeService {

	public List<InAccountType> findList(DetachedCriteria dc);

	public InAccountType findById(Long inaccounttype_id);

	public void save(InAccountType inAccountType);

	public void update(InAccountType inAccountType);


	public void deleteById(Long inaccounttype_id);

	public List<Object> findInAccountTypeName(Long user_id);

	public InAccountType findByTypeNameAndUserId(String typename, Long user_id);
}
