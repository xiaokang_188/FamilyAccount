package cool.xiaokang.familyaccount.service;

import cool.xiaokang.familyaccount.pojo.User;
import cool.xiaokang.familyaccount.utils.PageBean;

public interface UserService {

	/**
	 * 用户注册方法
	 */
	public void saveUser(User user) throws Exception;

	/**
	 * 修改个人资料
	 * 
	 * @param user
	 */
	public void update(User user);

	public void save(User currentUser);

	public void persistUser(User user);

	public boolean findUserByName(String user_name);

	public void updatePwd(User user);

	public boolean findUserByEmail(String email);

	public void pageQuery(PageBean pb);

	public void stop(String user_ids);

	public void open(String user_ids);

	public void editLevel(User user);

	public User findUserByKeyWord(String keyword);
}
