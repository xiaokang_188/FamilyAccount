package cool.xiaokang.familyaccount.service;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import cool.xiaokang.familyaccount.pojo.Loan;

public interface LoanService {

	public void saveLoan(Loan loan);

	public Loan findById(String loan_id);

	public void update(Loan loan);

	public void deleteById(String loan_id);

	public int getTotal();

	List<Loan> findList(DetachedCriteria dc);

}
