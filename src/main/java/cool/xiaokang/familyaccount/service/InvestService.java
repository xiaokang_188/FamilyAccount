package cool.xiaokang.familyaccount.service;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import cool.xiaokang.familyaccount.pojo.Invest;

public interface InvestService {

	public void save(Invest invest);

	public Invest findById(String invest_id);

	public void update(Invest invest);

	public void deleteById(String invest_id);

	public int getTotal();

	List<Invest> findList(DetachedCriteria dc);

}
