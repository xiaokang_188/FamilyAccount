package cool.xiaokang.familyaccount.service;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import cool.xiaokang.familyaccount.pojo.OutAccount;

public interface OutAccountService {

	public void saveOutAccount(List<OutAccount> list, Long user_id);

	public void deleteById(Long outaccount_id);

	public void save(OutAccount outAccount);

	public List<OutAccount> findList(DetachedCriteria dc);

	public OutAccount findById(Long outaccount_id);

	public void update(OutAccount outAccount);

	public List<Object> findOutAccountGroupByOutAccountTypeName(Long user_id, String year);

	public List<Object> findOutAccountWithYearly(Long user_id, String year);

	public List<OutAccount> findSixOutAccount(Long user_id);

	public Double findCurrentMonthTotalMoney(Long user_id);

	public List<Object> findPerMonthAvgMoney(Long user_id, String year);

	public int getTotal();

}
