package cool.xiaokang.familyaccount.service;

import java.util.List;

import cool.xiaokang.familyaccount.pojo.Role;
import cool.xiaokang.familyaccount.utils.PageBean;

public interface RoleService {

	public void save(Role role, String menuIds);

	public void pageQuery(PageBean pb);

	public List<Role> findAll();

	public Role findById(String role_id);

	public void update(Role role, String menuIds);
}
