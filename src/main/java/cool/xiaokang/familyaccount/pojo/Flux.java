package cool.xiaokang.familyaccount.pojo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @Description: 日志流量实体类
 * @author 小康
 * @version V1.0.0 2020年4月24日 下午4:15:09
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Flux {
	private String ReportTime;// 产生日期
	private Long PV;// 访问量
	private Long UV;// 独立访客数
	private Long VV;// 独立会话数
	private Double BR;// 跳出率
	private Long NewIP;// 新增IP数
	private Long NewCust;// 新增访客数
	private Double AvgDeep;// 平均访问时长
	private Double AvgTime;// 平均访问深度
}
