package cool.xiaokang.familyaccount.pojo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import com.alibaba.fastjson.annotation.JSONField;

import cool.xiaokang.familyaccount.utils.DateFormateUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @Description: 用户实体类
 * @author 小康
 * @version V1.0.0 2020年1月23日 下午2:05:01
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
	private Long user_id;// 用户编号
	private String user_name; // 用户昵称
	private String user_password;// 用户密码
	private Character user_sex;// 性别
	private String user_phone;// 手机号
	private Date birthday;// 出生日期
	private String idcard;// 身份证号
	private String address;// 详细地址
	private String realname;// 真实姓名
	private String email;// 邮箱
	private Date register_date;// 注册时间
	private String face;// 头像
	private Character state;// 状态
	private Integer level;// 用户级别
	private Date loginDate;// 上次登录时间

	@JSONField(serialize = false)
	private Set<InAccount> inaccounts;
	@JSONField(serialize = false)
	private Set<OutAccount> outaccounts;

	@JSONField(serialize = false)
	private Set<InAccountType> inaccounttypes;
	@JSONField(serialize = false)
	private Set<OutAccountType> outaccounttypes;

	@JSONField(serialize = false)
	private Set<Invest> invests;
	@JSONField(serialize = false)
	private Set<Loan> loans;

	@JSONField(serialize = false)
	private Set<Topic> topics;
	@JSONField(serialize = false)
	private Set<Reply> replys;

	public String getBirthday_s() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(birthday);
	}

	public String getRegister_date_s() {
		return DateFormateUtil.transferForCN(register_date);
	}

	public String getLoginDate_s() {
		return DateFormateUtil.transferHaveTime(loginDate);
	}
}
