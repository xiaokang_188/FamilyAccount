package cool.xiaokang.familyaccount.pojo;

import java.io.Serializable;
import java.util.Date;

import cool.xiaokang.familyaccount.utils.DateFormateUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @Description: 回复贴实体类
 * @author 小康
 * @version V1.0.0 2020年1月23日 下午5:44:56
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Reply implements Serializable {

	private Long reply_id;// 回复贴编号
	private String reply_content;// 回复内容
	private Date reply_datetime;// 回复日期
	private Integer zan;// 赞数量
	private Integer bad;// 鄙视数
	private Integer status;// 是否被查看
	private User user;
	private Topic topic;

	public String getReply_datetime_s() {
		return DateFormateUtil.transferHaveTime(reply_datetime);
	}

}
