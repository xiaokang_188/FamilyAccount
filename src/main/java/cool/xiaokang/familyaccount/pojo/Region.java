package cool.xiaokang.familyaccount.pojo;

import java.io.Serializable;
import java.util.Set;

import com.alibaba.fastjson.annotation.JSONField;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @Description: 交流大区实体类
 * @author 小康
 * @version V1.0.0 2020年1月23日 下午5:23:26
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Region implements Serializable {
	private Integer region_id;// 交流大区编号
	private String region_name;// 交流大区名称
	private String region_desc;// 交流大区备注
	@JSONField(serialize = false)
	private Set<Topic> topics;

}
