package cool.xiaokang.familyaccount.pojo;

import java.io.Serializable;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @Description: 角色实体类
 * @author 小康
 * @version V1.0.0 2020年1月23日 下午2:06:28
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Role implements Serializable {
	private String role_id;// 编号
	private String role_name;// 名称
	private String role_desc;// 描述
	// 角色关联的权限集合
	private Set<Menu> menus;
	// 角色关联的管理员集合
	private Set<Manager> managers;

	public Role(String role_id2) {
		this.role_id = role_id2;
	}

	public String getId() {
		return role_id;
	}

	public String getText() {
		return role_name;
	}

}
