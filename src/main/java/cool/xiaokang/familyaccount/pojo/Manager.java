package cool.xiaokang.familyaccount.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import cool.xiaokang.familyaccount.utils.DateFormateUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @Description: 管理员实体类
 * @author 小康
 * @version V1.0.0 2020年1月23日 下午2:05:20
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Manager implements Serializable {

	private String manager_id;
	private String manager_name;
	private String manager_pwd;
	private String manager_phone;
	private Character manager_sex;
	private Date birthday;
	private String idcard;
	private String address;
	private Set<Role> roles;

	public String getRolenames() {
		String rolenames = "";
		for (Role role : roles) {
			rolenames += role.getRole_name() + " ";
		}
		return rolenames;
	}

	public String getBirthday_s() {
		return DateFormateUtil.transferNotTime(birthday);
	}

}
