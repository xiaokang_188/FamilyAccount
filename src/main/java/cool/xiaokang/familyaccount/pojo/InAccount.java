package cool.xiaokang.familyaccount.pojo;

import java.io.Serializable;
import java.util.Date;

import cool.xiaokang.familyaccount.utils.DateFormateUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @Description: 收入账单实体类
 * @author 小康
 * @version V1.0.0 2020年1月23日 下午4:07:09
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InAccount implements Serializable {
	private Long inaccount_id;// 收入账单编号
	private Date inaccount_datetime;// 收入日期时间
	private Double inaccount_money;// 收入金额
	private Date inaccount_createtime;// 收入账单创建时间
	private String inaccount_desc;// 收入账单备注
	private User user;
	private InAccountType inAccountType;

	public InAccount(Date inaccount_datetime, Double inaccount_money, Date inaccount_createtime,
			String inaccount_desc) {
		this.inaccount_datetime = inaccount_datetime;
		this.inaccount_money = inaccount_money;
		this.inaccount_createtime = inaccount_createtime;
		this.inaccount_desc = inaccount_desc;
	}

	public String getInaccount_datetime_s() {
		return DateFormateUtil.transferNotTime(inaccount_datetime);
	}

	public String getInaccount_createtime_s() {
		return DateFormateUtil.transferHaveTime(inaccount_createtime);
	}

}
