package cool.xiaokang.familyaccount.pojo;

import java.io.Serializable;
import java.util.Date;

import cool.xiaokang.familyaccount.utils.DateFormateUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @Description: 支出账单实体类
 * @author 小康
 * @version V1.0.0 2020年1月23日 下午4:40:36
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OutAccount implements Serializable {

	private Long outaccount_id;
	private Date outaccount_datetime;
	private Double outaccount_money;
	private String outaccount_desc;
	private Date outaccount_createtime;
	private User user;
	private OutAccountType outAccountType;

	public OutAccount(Date outaccount_datetime, Double outaccount_money, String outaccount_desc,
			Date outaccount_createtime) {
		this.outaccount_datetime = outaccount_datetime;
		this.outaccount_money = outaccount_money;
		this.outaccount_desc = outaccount_desc;
		this.outaccount_createtime = outaccount_createtime;
	}

	public String getOutaccount_datetime_s() {
		return DateFormateUtil.transferNotTime(outaccount_datetime);
	}

	public String getOutaccount_createtime_s() {
		return DateFormateUtil.transferHaveTime(outaccount_createtime);
	}

}
