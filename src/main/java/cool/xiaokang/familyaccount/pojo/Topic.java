package cool.xiaokang.familyaccount.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import com.alibaba.fastjson.annotation.JSONField;

import cool.xiaokang.familyaccount.utils.DateFormateUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @Description: 主题帖实体类
 * @author 小康
 * @version V1.0.0 2020年1月23日 下午5:29:12
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Topic implements Serializable {

	private Long topic_id;// 主题帖编号
	private String topic_title;// 主题帖标题
	private String topic_content;// 主题帖内容
	private Date topic_datetime;// 主题帖发布日期
	private Character is_top;// 是否置顶
	private Character is_good;// 是否加精
	private Character is_end;// 是否结贴
	private Integer look_count;// 浏览数量
	private Integer topic_zan;// 点赞数
	private Integer topic_bad;// 鄙视数
	private Character del;// 是否删除 1:删除 0：未删除
	private Character solrDel;// solr中是否删除 1:删除 0：未删除
	private User user;
	private Region region;
	@JSONField(serialize = false)
	private Set<Reply> replys;

	public String getTopic_datetime_s() {
		return DateFormateUtil.transferHaveTime(topic_datetime);
	}

}
