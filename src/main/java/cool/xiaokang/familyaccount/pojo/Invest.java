package cool.xiaokang.familyaccount.pojo;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import cool.xiaokang.familyaccount.utils.DateFormateUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @Description: 投资理财实体类
 * @author 小康
 * @version V1.0.0 2020年1月23日 下午5:58:12
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Invest implements Serializable {
	private String invest_id;// 投资理财编号
	private String invest_name;// 投资理财名称
	private Date invest_datetime;// 时间
	private Integer invest_year;// 投资周期
	private String invest_target;// 投资目标
	private Date invest_createtime;// 创建时间
	private Double interest_rates;// 利率
	private Double invest_money;// 投资金额
	private String invest_desc;// 投资理财备注
	private User user;

	// 计算是否到期
	public String getOvertime() {
		Calendar ca = Calendar.getInstance();
		ca.setTime(invest_datetime);
		ca.add(ca.YEAR, invest_year);
		Long end = ca.getTime().getTime();
		Date date = new Date();
		Long now = date.getTime();
		Long result = now - end;
		if (result < 0) {
			return "未到期";
		}
		return "<span style='color:red;'>已到期</span>";
	}

	public Float getProfit() {
		double profit = (float) (invest_money * interest_rates * invest_year / 100);
		if (profit != 0) {
			return (float) profit;
		}
		return 0F;
	}

	public String getInvest_datetime_s() {
		return DateFormateUtil.transferForCN(invest_datetime);
	}

	public String getInvest_createtime_s() {
		return DateFormateUtil.transferHaveTime(invest_createtime);
	}

}
