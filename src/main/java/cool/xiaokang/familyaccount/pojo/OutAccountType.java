package cool.xiaokang.familyaccount.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import com.alibaba.fastjson.annotation.JSONField;

import cool.xiaokang.familyaccount.utils.DateFormateUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @Description: 支出类型实体类
 * @author 小康
 * @version V1.0.0 2020年1月23日 下午4:50:20
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OutAccountType implements Serializable {

	private Long outaccounttype_id;// 支出类型编号
	private String outaccounttype_name;// 支出类型名称
	private Date outaccounttype_createtime;// 支出类型创建时间
	private String outaccounttype_desc;// 支出类型备注
	private User user;
	@JSONField(serialize = false)
	private Set<OutAccount> outaccounts;

	public String getOutaccounttype_createtime_s() {
		return DateFormateUtil.transferHaveTime(outaccounttype_createtime);
	}

}
