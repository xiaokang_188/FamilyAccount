package cool.xiaokang.familyaccount.pojo;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import cool.xiaokang.familyaccount.utils.DateFormateUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @Description: 借款还贷实体类
 * @author 小康
 * @version V1.0.0 2020年1月23日 下午6:14:23
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Loan implements Serializable {
	private String loan_id;// 借款还贷编号
	private String loan_name;// 借款还贷名称
	private Date loan_datetime;// 借款还贷日期
	private Integer loan_year;// 周期（按年）
	private Double interest_rates;// 利息率
	private Double loan_money;// 借款金额
	private String loan_source;// 借款来源
	private String loan_desc;// 借款还贷备注
	private Date loan_createtime;// 创建时间
	private User user;

	// 计算是否到期
	public String getOvertime() {
		Calendar ca = Calendar.getInstance();
		ca.setTime(loan_datetime);
		ca.add(ca.YEAR, loan_year);
		Long end = ca.getTime().getTime();
		Date date = new Date();
		Long now = date.getTime();
		Long result = now - end;
		if (result < 0) {
			return "未到期";
		}
		return "<span style='color:red;'>已到期</span>";
	}

	// 计算预期应还利息
	public Float getLixi() {
		return (float) (loan_money * interest_rates * loan_year / 100);
	}

	public String getLoan_datetime_s() {
		return DateFormateUtil.transferForCN(loan_datetime);
	}

	public String getLoan_createtime_s() {
		return DateFormateUtil.transferHaveTime(loan_createtime);
	}
}
