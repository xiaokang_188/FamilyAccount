package cool.xiaokang.familyaccount.pojo;

import java.io.Serializable;
import java.util.Set;

import com.alibaba.fastjson.annotation.JSONField;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @Description: 权限实体类
 * @author 小康
 * @version V1.0.0 2020年1月23日 下午2:06:50
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Menu implements Serializable {
	private String menu_id;// 权限编号
	private String menu_name;// 权限名称
	private String menu_code;// 权限关键字
	private String menu_desc;// 权限描述
	private String menu_page;// 权限url
	private Integer zindex;// 优先级
	private String generatemenu;// 是否生成菜单
	// 角色集合
	@JSONField(serialize = false)
	private Set<Role> roles;
	// 上级权限
	private Menu parentMenu;
	// 下级权限
	@JSONField(serialize = false)
	private Set<Menu> children;

	public Menu(String menu_id) {
		this.menu_id = menu_id;
	}

	// zTree中pId
	public String getpId() {
		if (parentMenu == null) {
			return "0";
		}
		return parentMenu.getMenu_id();
	}

	// zTree中name
	public String getName() {
		return menu_name;
	}

	// zTree中page
	public String getPage() {
		return menu_page;
	}

	// easyui-combotree中text
	public String getText() {
		return menu_name;
	}
}
