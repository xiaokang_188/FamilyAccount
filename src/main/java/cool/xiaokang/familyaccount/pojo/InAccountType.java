package cool.xiaokang.familyaccount.pojo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import com.alibaba.fastjson.annotation.JSONField;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @Description: 收入类型实体类
 * @author 小康
 * @version V1.0.0 2020年1月23日 下午4:33:48
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InAccountType implements Serializable {

	private Long inaccounttype_id;// 收入类型编号
	private String inaccounttype_name;// 收入类型名称
	private Date inaccounttype_createtime;// 收入类型创建时间
	private String inaccounttype_desc;// 收入类型备注
	private User user;
	@JSONField(serialize = false)
	private Set<InAccount> inaccounts;

	public InAccountType(Long inaccounttype_id) {
		this.inaccounttype_id = inaccounttype_id;
	}

	public String getInaccounttype_createtime_s() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日  HH:mm");
		return format.format(this.inaccounttype_createtime);
	}

}
