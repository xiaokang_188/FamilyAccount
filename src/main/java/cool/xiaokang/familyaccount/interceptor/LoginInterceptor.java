package cool.xiaokang.familyaccount.interceptor;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

import cool.xiaokang.familyaccount.pojo.Manager;
import cool.xiaokang.familyaccount.pojo.User;

/**
 * 
 * @Description: 自定义登录方法拦截器拦截器
 * @author 小康
 * @version V1.0.0 2020年2月6日 上午9:00:54
 */
public class LoginInterceptor extends MethodFilterInterceptor {
	private static final long serialVersionUID = -6674052341270002153L;
	private static final Logger LOGGER = Logger.getRootLogger();

	@Override
	protected String doIntercept(ActionInvocation invocation) throws Exception {
		User user = (User) ActionContext.getContext().getSession().get("currentUser");
		Manager manager = (Manager) ActionContext.getContext().getSession().get("currentManager");
		if (user != null || manager != null) {
			String actionMethod = invocation.getInvocationContext().getName();
			LOGGER.info("方法:【" + actionMethod + "】进入自定义struts方法拦截器");
			return invocation.invoke();
		}
		return "toLogin";
	}

}
