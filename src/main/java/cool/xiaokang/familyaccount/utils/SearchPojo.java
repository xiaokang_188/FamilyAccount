package cool.xiaokang.familyaccount.utils;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @Description: 搜索帖子实体类
 * @author 小康
 * @version V1.0.0 2020年2月2日 上午11:33:15
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SearchPojo {
	private String id;
	private String topic_title;
	private String topic_content;
	private Date topic_datetime;
	private String look_count;

	public String getTopic_datetime_s() {
		return DateFormateUtil.transferHaveTime(topic_datetime);
	}

}
