package cool.xiaokang.familyaccount.utils;

import java.util.Date;
import java.util.Properties;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * 
 * @Description: 邮件工具类
 * @author 小康
 * @version V1.0.0 2020年2月2日 上午11:37:20
 */
public class MainMail {

	public static void sendMyEMail(String receiveMailAccount, String userName, String checkCode) throws Exception {
		// 发件人的 邮箱 和 密码（替换为自己的邮箱和密码）
		// 开启了独立密码的邮箱的话必需使用这个独立密码（授权码）。
		String myEmailAccount = "xiaokang.188@qq.com";
		String myEmailPassword = "oyxijvfhijzqjagi";
		// 发件人邮箱的 SMTP 服务器地址
		String myEmailSMTPHost = "smtp.qq.com";

		// 1. 创建参数配置, 用于连接邮件服务器的参数配置
		Properties props = new Properties();
		props.setProperty("mail.transport.protocol", "smtp");
		props.setProperty("mail.smtp.host", myEmailSMTPHost);
		props.setProperty("mail.smtp.auth", "true");

		// QQ邮箱服务器要求SMTP连接需要使用 SSL安全认证
		final String smtpPort = "465";
		props.setProperty("mail.smtp.port", smtpPort);
		props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.setProperty("mail.smtp.socketFactory.fallback", "false");
		props.setProperty("mail.smtp.socketFactory.port", smtpPort);

		// 2. 根据配置创建会话对象, 用于和邮件服务器交互
		Session session = Session.getInstance(props);
		// 设置为debug模式, 可以查看详细的发送 log
		session.setDebug(true);

		// 3. 创建一封邮件
		// 1. 创建一封邮件
		MimeMessage message = new MimeMessage(session);

		// 2. From: 发件人（昵称有广告嫌疑，避免被邮件服务器误认为是滥发广告以至返回失败，请修改昵称）
		message.setFrom(new InternetAddress(myEmailAccount, "家庭记账管理系统", "UTF-8"));

		// 3. To: 收件人（可以增加多个收件人、抄送、密送）
		message.setRecipient(MimeMessage.RecipientType.TO,
				new InternetAddress(receiveMailAccount, userName + "用户", "UTF-8"));

		// 4. Subject: 邮件主题（标题有广告嫌疑，避免被邮件服务器误认为是滥发广告以至返回失败，请修改标题）
		message.setSubject("找回密码", "UTF-8");

		// 5. Content: 邮件正文（可以使用html标签）（内容有广告嫌疑，避免被邮件服务器误认为是滥发广告以至返回失败，请修改发送内容）
		message.setContent("【" + userName + "】用户你好, 您的验证码为【" + checkCode + "】", "text/html;charset=UTF-8");
		// 6. 设置发件时间
		message.setSentDate(new Date());
		// 7. 保存设置
		message.saveChanges();

		// 4. 根据 Session 获取邮件传输对象
		Transport transport = session.getTransport();

		// 5. 使用 邮箱账号 和 密码 连接邮件服务器, 这里认证的邮箱必须与 message 中的发件人邮箱一致, 否则报错
		transport.connect(myEmailAccount, myEmailPassword);

		// 6. 发送邮件, 发到所有的收件地址
		// message.getAllRecipients() 获取到的是在创建邮件对象时添加的所有收件人, 抄送人,密送人
		transport.sendMessage(message, message.getAllRecipients());

		// 7. 关闭连接
		transport.close();
	}

	public static void main(String[] args) throws Exception {
		MainMail.sendMyEMail("xiaokang.188@qq.com", "xiaokang", "66666666");
	}

}
