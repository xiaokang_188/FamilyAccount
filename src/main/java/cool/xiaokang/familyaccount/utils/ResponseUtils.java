package cool.xiaokang.familyaccount.utils;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.PropertyFilter;
import com.alibaba.fastjson.serializer.SerializerFeature;

import cool.xiaokang.familyaccount.pojo.User;

/**
 * 
 * @Description: 响应工具类
 * @author 小康
 * @version V1.0.0 2020年2月18日 下午7:58:20
 */
public class ResponseUtils {

	/**
	 * 
	 * @Description: 将指定的PageBean对象转化为json字符串返回
	 * @return void
	 * @param response
	 * @param pb
	 * @param filter
	 */
	public static void respPageBean(HttpServletResponse response, PageBean pb, PropertyFilter filter) {
		try {
			response.setContentType("text/json;charset=utf-8");
			response.getWriter().print(JSON.toJSONString(pb, filter));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Description: 将指定的PageBean对象转化为json字符串返回(禁止以引用方式引用对象)
	 * @return void
	 * @param response
	 * @param pb
	 * @param filter
	 */
	public static void respPageBeanRef(HttpServletResponse response, PageBean pb, PropertyFilter filter) {
		try {
			response.setContentType("text/json;charset=utf-8");
			response.getWriter().print(JSON.toJSONString(pb, filter, SerializerFeature.DisableCircularReferenceDetect));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Description: 将指定的List对象转化为json字符串返回
	 * @return void
	 * @param response
	 * @param pb
	 * @param filter
	 */
	public static void respList(HttpServletResponse response, List list, PropertyFilter filter) {
		try {
			response.setContentType("text/json;charset=utf-8");
			response.getWriter().print(JSON.toJSONString(list, filter));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Description: 将指定的List对象转化为json字符串返回(禁止以引用方式引用对象)
	 * @return void
	 * @param response
	 * @param pb
	 * @param filter
	 */
	public static void respListRef(HttpServletResponse response, List list, PropertyFilter filter) {
		try {
			response.setContentType("text/json;charset=utf-8");
			response.getWriter()
					.print(JSON.toJSONString(list, filter, SerializerFeature.DisableCircularReferenceDetect));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Description: 将List对象转化为json字符串返回
	 * @return void
	 * @param response
	 * @param pb
	 */
	public static void respList(HttpServletResponse response, List list) {
		try {
			response.setContentType("text/json;charset=utf-8");
			response.getWriter().print(JSON.toJSONString(list));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Description: 将List对象转化为json字符串返回(禁止以引用方式引用对象)
	 * @return void
	 * @param response
	 * @param pb
	 */
	public static void respListRef(HttpServletResponse response, List list) {
		try {
			response.setContentType("text/json;charset=utf-8");
			response.getWriter().print(JSON.toJSONString(list, SerializerFeature.DisableCircularReferenceDetect));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Description: 将指定的Object对象转化为json字符串返回
	 * @return void
	 * @param response
	 * @param obj
	 */
	public static void respObj(HttpServletResponse response, Object obj) {
		try {
			response.setContentType("text/json;charset=utf-8");
			response.getWriter().print(JSON.toJSONString(obj));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Description: 将指定的Object对象转化为json字符串返回
	 * @return void
	 * @param response
	 * @param obj
	 * @param filter
	 */
	public static void respObjAndFilter(HttpServletResponse response, Object obj, PropertyFilter filter) {
		try {
			response.setContentType("text/json;charset=utf-8");
			response.getWriter().print(JSON.toJSONString(obj, filter));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Description: 将指定的JSONObject转化为json字符串返回
	 * @return void
	 * @param response
	 * @param jsonObject
	 */
	public static void respJsonWithJSONObject(HttpServletResponse response, JSONObject jsonObject) {
		try {
			response.setContentType("text/json;charset=utf-8");
			response.getWriter().write(JSON.toJSONString(jsonObject));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 将User对象封装为json字符串
	 * @param user
	 * @return
	 */
	public static String fieldToJson(User user) {
		if (user.getUser_sex()==null) {
			user.setUser_sex('x');
		}
		if (user.getIdcard()==null) {
			user.setIdcard("x");
		}
		return "{\"user_id\":\""+user.getUser_id()+"\","+
				"\"user_name\":\""+user.getUser_name()+"\","+
				"\"idcard\":\""+user.getIdcard()+"\","+
				"\"user_sex\":\""+user.getUser_sex()+"\","+
				"\"user_phone\":\""+user.getUser_phone()+"\","+
				"\"state\":\""+user.getState()+"\","+
				"\"level\":\""+user.getLevel()+"\","+
				"\"loginDate_s\":\""+user.getLoginDate_s()+"\"}";
	}
	
	/**
	 * 将json字符串返回
	 * @param response
	 * @param jsonString
	 */
	public static void respJsonString(HttpServletResponse response, String jsonString) {
		try {
			response.setContentType("text/json;charset=utf-8");
			response.getWriter().write(jsonString);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
