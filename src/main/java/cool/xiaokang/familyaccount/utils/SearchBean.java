package cool.xiaokang.familyaccount.utils;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @Description: 搜索分页实体类
 * @author 小康
 * @version V1.0.0 2020年2月2日 上午11:34:54
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SearchBean {

	private Integer currentPage;// 当前页
	private Integer pagesize;// 每页记录数
	private Long totalCount;// 总记录数
	private Integer totalPage;
	private List<SearchPojo> rows = new ArrayList<SearchPojo>();// 数据集合

}
