package cool.xiaokang.familyaccount.utils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @Description: 时间工具类
 * @author 小康
 * @version V1.0.0 2020年2月2日 上午11:38:28
 */
public class DateFormateUtil {
	public static Date transfer(Date date) {
		SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
		String datestr = formate.format(date);
		try {
			return formate.parse(datestr);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Date transferYear(String year) {
		if (year != null && !"".equals(year)) {
			SimpleDateFormat formate = new SimpleDateFormat("yyyy");
			try {
				return formate.parse(year);
			} catch (ParseException e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	public static String transferNotTime(Date date) {
		if (date != null) {
			SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
			return formate.format(date);
		}
		return null;
	}

	public static String transferForCN(Date date) {
		if (date != null) {
			SimpleDateFormat formate = new SimpleDateFormat("yyyy年MM月dd日");
			return formate.format(date);
		}
		return null;
	}

	public static String transferHaveTime(Date date) {
		if (date != null) {
			SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return formate.format(date);
		}
		return null;
	}

	public static Date transferString(String string) {
		if (!string.equals("") && string != null) {
			SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd");
			try {
				return formate.parse(string);
			} catch (ParseException e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	public static Double formatMoney(Double money) {
		DecimalFormat df = new DecimalFormat("#.00");
		return Double.parseDouble(df.format(money));
	}
}
