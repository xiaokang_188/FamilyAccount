package cool.xiaokang.familyaccount.utils;

import org.apache.shiro.crypto.hash.Md5Hash;

/**
 * 
 * @Description: MD5盐值加密
 * @author 小康
 * @version V1.0.0 2020年1月31日 下午8:02:12
 */
public class CryptographyUtil {

	public static final String FAMILY = "family.xiaokang.cool";

	/**
	 * Md5加密
	 * 
	 * @param str
	 * @param salt
	 * @return
	 */
	public static String md5(String str, String salt) {
		return new Md5Hash(str, salt).toString();
	}

	public static void main(String[] args) {
//		String users[] = { "family", "xiaokangxxs", "k1583223", "g1500813439", "g1379370111", "z544158188", "xiaokang",
//				"z1029181448", "ll765268279", "j1171653263", "h1047055548" };
//		for (String string : users) {
//			String md5 = CryptographyUtil.md5("123456", CryptographyUtil.md5("family.xiaokang.cool", string));
//			System.out.println(string + "-" + md5);
//		}
//		String pwd = "xiaokang";
		String pwd = "123456";
//		String pwd = "zhuti";
		String md5 = CryptographyUtil.md5(pwd, CryptographyUtil.md5("family.xiaokang.cool", "xk1997"));
//		String md5 = CryptographyUtil.md5(pwd, CryptographyUtil.md5("family.xiaokang.cool", "xiaokang1"));
		System.out.println(md5);
//		System.out.println(UUIDUtils.generate().toString());

	}
}
