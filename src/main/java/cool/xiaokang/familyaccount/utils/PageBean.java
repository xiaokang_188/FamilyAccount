package cool.xiaokang.familyaccount.utils;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @Description: 分页实体类
 * @author 小康
 * @version V1.0.0 2020年2月2日 上午11:31:20
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PageBean {
	private Integer currentPage;// 当前页
	private Integer pageSize;// 每页显示记录数
	private DetachedCriteria detachedCriteria;// 查询条件
	private Integer total;// 总记录数
	private List rows;// 数据集合
}
