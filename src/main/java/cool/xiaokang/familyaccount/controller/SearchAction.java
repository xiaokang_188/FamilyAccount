package cool.xiaokang.familyaccount.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cool.xiaokang.familyaccount.service.SearchService;
import cool.xiaokang.familyaccount.utils.SearchBean;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Controller
@Scope("prototype")
public class SearchAction extends ActionSupport {
	private static final long serialVersionUID = 5829622718278763996L;

	@Autowired
	private SearchService searchService;

	private Integer currentPage;
	private String keyword;

	public String search() {
		if (StringUtils.isNotBlank(keyword)) {
			SearchBean searchBean = searchService.searchByKeyWord(keyword, currentPage);
			ActionContext.getContext().put("searchBean", searchBean);
			return "list";
		}
		return "home";
	}

}
