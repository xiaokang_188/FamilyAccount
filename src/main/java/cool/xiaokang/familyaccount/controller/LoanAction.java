package cool.xiaokang.familyaccount.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.serializer.PropertyFilter;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cool.xiaokang.familyaccount.pojo.Loan;
import cool.xiaokang.familyaccount.pojo.User;
import cool.xiaokang.familyaccount.service.LoanService;
import cool.xiaokang.familyaccount.utils.ResponseUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Controller
@Scope("prototype")
public class LoanAction extends ActionSupport implements ModelDriven<Loan> {
	private static final long serialVersionUID = -9195978355907898408L;

	@Autowired
	private LoanService loanService;

	private Date startTime;
	private Date endTime;
	private String loanname;
	private String desc;
	private Loan loan = new Loan();

	// 添加借款账单
	public String add() {
		User user = (User) ServletActionContext.getContext().getSession().get("currentUser");
		loan.setUser(user);
		loan.setLoan_createtime(new Date());
		loanService.saveLoan(loan);
		return "toShow";
	}

	// 进入借款账单jsp
	public String list() throws Exception {
		return "list";
	}

	// 当前用户的借款账单（投资时间降序）
	public String listCurrentUser() {
		DetachedCriteria dc = DetachedCriteria.forClass(Loan.class);
		User user = (User) ServletActionContext.getContext().getSession().get("currentUser");
		dc.add(Restrictions.eq("user.user_id", user.getUser_id()));
		dc.addOrder(Order.desc("loan_datetime"));
		if (startTime != null || endTime != null) {
			if (startTime != null && endTime != null) {
				dc.add(Restrictions.between("loan_datetime", startTime, endTime));
			} else {
				dc.add(Restrictions.like("loan_datetime", startTime != null ? startTime : endTime));
			}
		}
		if (StringUtils.isNotBlank(loanname)) {
			dc.add(Restrictions.like("loan_name", "%" + loanname + "%"));
		}
		if (StringUtils.isNotBlank(desc)) {
			dc.add(Restrictions.like("loan_desc", "%" + desc + "%"));
		}
		List<Loan> loans = loanService.findList(dc);
		PropertyFilter filter = new PropertyFilter() {
			@Override
			public boolean apply(Object object, String name, Object value) {
				boolean flag = true;
				if (name.equals("user")) {
					flag = false;
				}
				return flag;
			}
		};
		ResponseUtils.respList(ServletActionContext.getResponse(), loans, filter);
		return NONE;
	}

	// 根据id查找借款
	public String findById() throws IOException {
		if (loan.getLoan_id() != null && !"".equals(loan.getLoan_id())) {
			Loan loan1 = loanService.findById(loan.getLoan_id());
			PropertyFilter filter = new PropertyFilter() {
				@Override
				public boolean apply(Object object, String name, Object value) {
					boolean flag = true;
					if (name.equals("user")) {
						flag = false;
					}
					return flag;
				}
			};
			ResponseUtils.respObjAndFilter(ServletActionContext.getResponse(), loan1, filter);
		}
		return NONE;
	}

	// 修改借款
	public String update() {
		User user = (User) ServletActionContext.getContext().getSession().get("currentUser");
		loan.setUser(user);
		loanService.update(loan);
		return "toShow";
	}

	// 根据ID删除借款
	public String delete() {
		if (loan.getLoan_id() != null && !"".equals(loan.getLoan_id())) {
			loanService.deleteById(loan.getLoan_id());
		}
		return "toShow";
	}

	@Override
	public Loan getModel() {
		return loan;
	}
}
