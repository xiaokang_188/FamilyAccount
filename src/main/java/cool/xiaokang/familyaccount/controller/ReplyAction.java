package cool.xiaokang.familyaccount.controller;

import java.io.IOException;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.struts2.ServletActionContext;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.serializer.PropertyFilter;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cool.xiaokang.familyaccount.pojo.Reply;
import cool.xiaokang.familyaccount.pojo.Topic;
import cool.xiaokang.familyaccount.pojo.User;
import cool.xiaokang.familyaccount.service.ReplyService;
import cool.xiaokang.familyaccount.service.TopicService;
import cool.xiaokang.familyaccount.utils.PageBean;
import cool.xiaokang.familyaccount.utils.ResponseUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Controller
@Scope("prototype")
public class ReplyAction extends ActionSupport implements ModelDriven<Reply> {
	private static final long serialVersionUID = 528946094310986318L;
	private Reply reply = new Reply();
	@Autowired
	private TopicService topicService;
	@Autowired
	private ReplyService replyService;
	private Integer rows;
	private Integer page;

	public String add() {
		User user = (User) ActionContext.getContext().getSession().get("currentUser");
		Topic topic = topicService.showContent(reply.getTopic().getTopic_id());
		reply.setUser(user);
		reply.setTopic(topic);
		replyService.save(reply);
		ActionContext.getContext().put("topic_id", topic.getTopic_id());
		return "toshow";
	}

	// 阅读回复
	public String read() throws IOException {
		String flag = "1";

		try {
			replyService.readById(reply.getReply_id());
			ServletActionContext.getResponse().getWriter().write(flag);
		} catch (Exception e) {
			flag = "0";
			ServletActionContext.getResponse().getWriter().write(flag);
			e.printStackTrace();
		}
		return NONE;
	}

	// 删除回复
	public String deleteById() throws IOException {
		String flag = "1";
		try {
			replyService.deleteById(reply.getReply_id());
			ServletActionContext.getResponse().getWriter().write(flag);
		} catch (Exception e) {
			flag = "0";
			ServletActionContext.getResponse().getWriter().write(flag);
			e.printStackTrace();
		}
		return NONE;
	}

	// 点赞
	public String zan() throws IOException {
		String flag = "1";
		try {
			replyService.zan(reply.getReply_id());
		} catch (Exception e) {
			flag = "0";
		}
		ServletActionContext.getResponse().getWriter().write(flag);
		return NONE;
	}

	// 鄙视
	public String bad() throws IOException {
		String flag = "1";
		try {
			replyService.bad(reply.getReply_id());
		} catch (Exception e) {
			flag = "0";
		}
		ServletActionContext.getResponse().getWriter().write(flag);
		return NONE;
	}

	@RequiresPermissions("reply-confirm-page")
	public String pageQuery() throws IOException {
		DetachedCriteria dc = DetachedCriteria.forClass(Reply.class);
		dc.add(Restrictions.ge("bad", 10));
		PageBean pb = new PageBean();
		pb.setDetachedCriteria(dc);
		pb.setCurrentPage(page);
		pb.setPageSize(rows);
		replyService.pageQuery(pb);
		PropertyFilter filter = new PropertyFilter() {
			@Override
			public boolean apply(Object object, String name, Object value) {
				boolean flag = true;
				if (name.equals("region")) {
					flag = false;
				} else if (name.equals("detachedCriteria")) {
					flag = false;
				} else if (name.equals("pageSize")) {
					flag = false;
				} else if (name.equals("currentPage")) {
					flag = false;
				} else if (name.equals("age")) {
					flag = false;
				}
				return flag;
			}
		};
		ResponseUtils.respPageBeanRef(ServletActionContext.getResponse(), pb, filter);
		return NONE;
	}

	@RequiresPermissions("reply-confirm")
	public String confirm() {
		replyService.confirm(reply.getReply_id());
		return "adminconfirm";
	}

	@Override
	public Reply getModel() {
		return reply;
	}

}
