package cool.xiaokang.familyaccount.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.serializer.PropertyFilter;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cool.xiaokang.familyaccount.pojo.Invest;
import cool.xiaokang.familyaccount.pojo.User;
import cool.xiaokang.familyaccount.service.InvestService;
import cool.xiaokang.familyaccount.utils.ResponseUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Controller
@Scope("prototype")
public class InvestAction extends ActionSupport implements ModelDriven<Invest> {
	private static final long serialVersionUID = -3339626765130219644L;

	@Autowired
	private InvestService investService;

	private Date startTime;
	private Date endTime;
	private String investname;
	private String desc;
	private Invest invest = new Invest();

	// 添加投资账单
	public String add() {
		User user = (User) ServletActionContext.getContext().getSession().get("currentUser");
		invest.setUser(user);
		if (StringUtils.isNotBlank(invest.getInvest_name()) && invest.getInvest_datetime() != null
				&& invest.getInvest_money() != 0.0 && invest.getInterest_rates() != 0.0
				&& StringUtils.isNotBlank(invest.getInvest_target())) {
			invest.setInvest_createtime(new Date());
			investService.save(invest);
		}
		return "toShow";
	}

	// 进入投资账单jsp
	public String list() throws Exception {
		return "list";
	}

	// 当前用户的投资账单（投资时间降序）
	public String listCurrentUser() {
		DetachedCriteria dc = DetachedCriteria.forClass(Invest.class);
		User user = (User) ServletActionContext.getContext().getSession().get("currentUser");
		dc.add(Restrictions.eq("user.user_id", user.getUser_id()));
		dc.addOrder(Order.desc("invest_datetime"));
		if (startTime != null || endTime != null) {
			if (startTime != null && endTime != null) {
				dc.add(Restrictions.between("invest_datetime", startTime, endTime));
			} else {
				dc.add(Restrictions.like("invest_datetime", startTime != null ? startTime : endTime));
			}
		}
		if (StringUtils.isNotBlank(investname)) {
			dc.add(Restrictions.like("invest_name", "%" + investname + "%"));
		}
		if (StringUtils.isNotBlank(desc)) {
			dc.add(Restrictions.like("invest_desc", "%" + desc + "%"));
		}
		List<Invest> invests = investService.findList(dc);
		PropertyFilter filter = new PropertyFilter() {
			@Override
			public boolean apply(Object object, String name, Object value) {
				boolean flag = true;
				if (name.equals("user")) {
					flag = false;
				}
				return flag;
			}
		};
		ResponseUtils.respList(ServletActionContext.getResponse(), invests, filter);
		return NONE;
	}

	// 根据id查找投资账单
	public String findById() throws IOException {
		if (StringUtils.isNotBlank(invest.getInvest_id())) {
			Invest invest1 = investService.findById(invest.getInvest_id());
			PropertyFilter filter = new PropertyFilter() {
				@Override
				public boolean apply(Object object, String name, Object value) {
					boolean flag = true;
					if (name.equals("user")) {
						flag = false;
					}
					return flag;
				}
			};
			ResponseUtils.respObjAndFilter(ServletActionContext.getResponse(), invest1, filter);
		}
		return NONE;
	}

	// 修改投资账单
	public String update() {
		User user = (User) ServletActionContext.getContext().getSession().get("currentUser");
		invest.setUser(user);
		investService.update(invest);
		return "toShow";
	}

	// 根据id删除投资账单
	public String delete() {
		if (StringUtils.isNotBlank(invest.getInvest_id())) {
			investService.deleteById(invest.getInvest_id());
		}
		return "toShow";
	}

	@Override
	public Invest getModel() {
		return invest;
	}

}
