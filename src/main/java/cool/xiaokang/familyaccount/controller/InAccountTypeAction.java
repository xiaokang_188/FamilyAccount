package cool.xiaokang.familyaccount.controller;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.PropertyFilter;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cool.xiaokang.familyaccount.pojo.InAccountType;
import cool.xiaokang.familyaccount.pojo.User;
import cool.xiaokang.familyaccount.service.InAccountTypeService;
import cool.xiaokang.familyaccount.utils.ResponseUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Controller
@Scope("prototype")
public class InAccountTypeAction extends ActionSupport implements ModelDriven<InAccountType> {
	private static final long serialVersionUID = 2116864150584106165L;
	private JSONObject jsonObject = new JSONObject();

	@Autowired
	private InAccountTypeService inAccountTypeService;

	private String intypeName;
	private InAccountType inAccountType = new InAccountType();

	// 页面ajax加载下拉框
	public String listAjax() throws IOException {
		DetachedCriteria dc = DetachedCriteria.forClass(InAccountType.class);
		User user = (User) ActionContext.getContext().getSession().get("currentUser");
		dc.add(Restrictions.eq("user.user_id", user.getUser_id()));
		List<InAccountType> inAccountTypes = inAccountTypeService.findList(dc);
		PropertyFilter filter = new PropertyFilter() {
			@Override
			public boolean apply(Object object, String name, Object value) {
				boolean flag = true;
				if (name.equals("inaccounts")) {
					flag = false;
				} else if (name.equals("user")) {
					flag = false;
				} else if (name.equals("inaccounttype_createtime")) {
					flag = false;
				}
				return flag;
			}
		};
		ResponseUtils.respList(ServletActionContext.getResponse(), inAccountTypes, filter);
		return NONE;
	}

	// 进入收入账单类型jsp
	public String list() {
		return "list";
	}

	// 当前用户的收入账单类型（类型id升序）
	public String listCurrentUser() {
		DetachedCriteria dc = DetachedCriteria.forClass(InAccountType.class);
		User user = (User) ActionContext.getContext().getSession().get("currentUser");
		dc.add(Restrictions.eq("user.user_id", user.getUser_id()));
		dc.addOrder(Order.asc("inaccounttype_id"));
		if (StringUtils.isNotBlank(intypeName)) {
			inAccountType.setInaccounttype_name(intypeName);
			dc.add(Restrictions.like("inaccounttype_name", "%" + inAccountType.getInaccounttype_name() + "%"));
		}
		PropertyFilter filter = new PropertyFilter() {
			@Override
			public boolean apply(Object object, String name, Object value) {
				boolean flag = true;
				if (name.equals("user")) {
					flag = false;
				}
				return flag;
			}
		};
		List<InAccountType> inAccountTypes = inAccountTypeService.findList(dc);
		ResponseUtils.respListRef(ServletActionContext.getResponse(), inAccountTypes, filter);
		return NONE;
	}

	// 添加收入类型
	public String add() {
		inAccountType.setInaccounttype_createtime(new Date());
		User user = (User) ActionContext.getContext().getSession().get("currentUser");
		inAccountType.setUser(user);
		inAccountTypeService.save(inAccountType);
		return "toShow";
	}

	// 检查收入账单类型名称
	public String checkName() {
		User user = (User) ActionContext.getContext().getSession().get("currentUser");
		InAccountType findByName = inAccountTypeService.findByTypeNameAndUserId(intypeName, user.getUser_id());
		if (findByName != null) {
			jsonObject.put("isExist", true);
		} else {
			jsonObject.put("isExist", false);
		}
		ResponseUtils.respJsonWithJSONObject(ServletActionContext.getResponse(), jsonObject);
		return NONE;
	}

	// 根据ID查找收入类型信息
	public String findById() throws Exception {
		InAccountType inAccountType2 = inAccountTypeService.findById(inAccountType.getInaccounttype_id());
		PropertyFilter filter = new PropertyFilter() {
			@Override
			public boolean apply(Object object, String name, Object value) {
				boolean flag = true;
				if (name.equals("inaccounts")) {
					flag = false;
				} else if (name.equals("user")) {
					flag = false;
				} else if (name.equals("inaccounttype_createtime")) {
					flag = false;
				}
				return flag;
			}
		};
		ResponseUtils.respObjAndFilter(ServletActionContext.getResponse(), inAccountType2, filter);
		return NONE;
	}

	// 修改收入类型
	public String update() {
		User u = (User) ServletActionContext.getContext().getSession().get("currentUser");
		inAccountType.setUser(u);
		inAccountTypeService.update(inAccountType);
		return "toShow";
	}

	// 根据ID删除收入类型
	public String delete() throws IOException {
		String flag = "0";
		try {
			inAccountTypeService.deleteById(inAccountType.getInaccounttype_id());
		} catch (Exception e) {
			flag = "1";
			e.printStackTrace();
		}
		ServletActionContext.getResponse().setContentType("text/json;charset=utf-8");
		ServletActionContext.getResponse().getWriter().write(flag);
		return NONE;
	}

	// 查询所有收入账单类型名称
	public String findInAccountTypeName() throws Exception {
		User user = (User) ActionContext.getContext().getSession().get("currentUser");
		List<Object> list = inAccountTypeService.findInAccountTypeName(user.getUser_id());
		Map<String, Integer> outTypeMap = new HashMap<String, Integer>();
		int i = 1;
		for (Object object : list) {
			outTypeMap.put(object.toString(), i);
			i++;
		}
		ResponseUtils.respObj(ServletActionContext.getResponse(), outTypeMap);
		return NONE;
	}

	public InAccountType getModel() {
		return inAccountType;
	}

}
