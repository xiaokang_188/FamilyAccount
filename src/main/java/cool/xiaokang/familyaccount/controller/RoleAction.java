package cool.xiaokang.familyaccount.controller;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.struts2.ServletActionContext;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.serializer.PropertyFilter;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cool.xiaokang.familyaccount.pojo.Role;
import cool.xiaokang.familyaccount.service.RoleService;
import cool.xiaokang.familyaccount.utils.PageBean;
import cool.xiaokang.familyaccount.utils.ResponseUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Controller
@Scope("prototype")
public class RoleAction extends ActionSupport implements ModelDriven<Role> {
	private static final long serialVersionUID = 8004609617024214107L;
	private static final Logger LOGGER = Logger.getRootLogger();
	private Role role = new Role();
	private Integer page;
	private Integer rows;

	@Autowired
	private RoleService roleService;

	private String menuIds;

	@RequiresPermissions("role-add")
	public String add() {
		LOGGER.info("【RoleAction_add】>>>menuIds:" + menuIds);
		roleService.save(role, menuIds);
		return "list";
	}

	@RequiresPermissions("role-list")
	public String pageQuery() throws IOException {
		DetachedCriteria dc = DetachedCriteria.forClass(Role.class);
		PageBean pb = new PageBean();
		pb.setDetachedCriteria(dc);
		pb.setCurrentPage(page);
		pb.setPageSize(rows);
		roleService.pageQuery(pb);
		PropertyFilter filter = new PropertyFilter() {
			@Override
			public boolean apply(Object object, String name, Object value) {
				boolean flag = true;
				if (name.equals("detachedCriteria")) {
					flag = false;
				} else if (name.equals("currentPage")) {
					flag = false;
				} else if (name.equals("pageSize")) {
					flag = false;
				} else if (name.equals("managers")) {
					flag = false;
				} else if (name.equals("menus")) {
					flag = false;
				}
				return flag;
			}
		};
		ResponseUtils.respPageBean(ServletActionContext.getResponse(), pb, filter);
		return NONE;
	}

	public String listAjax() throws IOException {
		List<Role> roles = roleService.findAll();
		PropertyFilter filter = new PropertyFilter() {
			@Override
			public boolean apply(Object object, String name, Object value) {
				boolean flag = true;
				if (name.equals("menus")) {
					flag = false;
				} else if (name.equals("managers")) {
					flag = false;
				}
				return flag;
			}
		};
		ResponseUtils.respList(ServletActionContext.getResponse(), roles, filter);
		return NONE;
	}

	@RequiresPermissions("role-update")
	public String foredit() {
		Role model = roleService.findById(role.getRole_id());
		ActionContext.getContext().put("role", model);
		return "edit";
	}

	@RequiresPermissions("role-update")
	public String update() {
		LOGGER.info("【RoleAction_update】>>>menuIds:" + menuIds);
		roleService.update(role, menuIds);
		return "list";
	}

	@Override
	public Role getModel() {
		return role;
	}

}
