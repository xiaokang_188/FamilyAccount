package cool.xiaokang.familyaccount.controller;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.PropertyFilter;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cool.xiaokang.familyaccount.pojo.OutAccountType;
import cool.xiaokang.familyaccount.pojo.User;
import cool.xiaokang.familyaccount.service.OutAccountTypeService;
import cool.xiaokang.familyaccount.utils.ResponseUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Controller
@Scope("prototype")
public class OutAccountTypeAction extends ActionSupport implements ModelDriven<OutAccountType> {
	private static final long serialVersionUID = -4348729002967633523L;
	private JSONObject jsonObject = new JSONObject();

	@Autowired
	private OutAccountTypeService outAccountTypeService;

	private String outtypeName;
	private OutAccountType outAccountType = new OutAccountType();

	// 页面ajax加载下拉框
	public String listAjax() throws IOException {
		DetachedCriteria dc = DetachedCriteria.forClass(OutAccountType.class);
		User user = (User) ActionContext.getContext().getSession().get("currentUser");
		dc.add(Restrictions.eq("user.user_id", user.getUser_id()));
		List<OutAccountType> list = outAccountTypeService.findList(dc);
		PropertyFilter filter = new PropertyFilter() {
			@Override
			public boolean apply(Object object, String name, Object value) {
				boolean flag = true;
				if (name.equals("outaccounts")) {
					flag = false;
				} else if (name.equals("user")) {
					flag = false;
				} else if (name.equals("outaccounttype_createtime")) {
					flag = false;
				}
				return flag;
			}
		};
		ResponseUtils.respObjAndFilter(ServletActionContext.getResponse(), list, filter);
		return NONE;
	}

	// 进入支出账单类型jsp
	public String list() {
		return "list";
	}

	// 当前用户的支出账单类型（类型id升序）
	public String listCurrentUser() {
		DetachedCriteria dc = DetachedCriteria.forClass(OutAccountType.class);
		User user = (User) ActionContext.getContext().getSession().get("currentUser");
		dc.add(Restrictions.eq("user.user_id", user.getUser_id()));
		dc.addOrder(Order.asc("outaccounttype_id"));
		if (StringUtils.isNotBlank(outtypeName)) {
			outAccountType.setOutaccounttype_name(outtypeName);
			dc.add(Restrictions.like("outaccounttype_name", "%" + outAccountType.getOutaccounttype_name() + "%"));
		}
		PropertyFilter filter = new PropertyFilter() {
			@Override
			public boolean apply(Object object, String name, Object value) {
				boolean flag = true;
				if (name.equals("user")) {
					flag = false;
				}
				return flag;
			}
		};
		List<OutAccountType> outAccountTypes = outAccountTypeService.findList(dc);
		ResponseUtils.respListRef(ServletActionContext.getResponse(), outAccountTypes, filter);
		return NONE;
	}

	// 添加支出类型
	public String add() {
		outAccountType.setOutaccounttype_createtime(new Date());
		User user = (User) ActionContext.getContext().getSession().get("currentUser");
		outAccountType.setUser(user);
		outAccountTypeService.save(outAccountType);
		return "toShow";
	}

	// 检查支出账单类型名称
	public String checkName() {
		User user = (User) ActionContext.getContext().getSession().get("currentUser");
		OutAccountType findByName = outAccountTypeService.findByTypeNameAndUserId(outtypeName, user.getUser_id());
		if (findByName != null) {
			jsonObject.put("isExist", true);
		} else {
			jsonObject.put("isExist", false);
		}
		ResponseUtils.respJsonWithJSONObject(ServletActionContext.getResponse(), jsonObject);
		return NONE;
	}

	// 根据ID查找支出类型信息
	public String findById() throws Exception {
		OutAccountType outAccountType2 = outAccountTypeService.findById(outAccountType.getOutaccounttype_id());
		PropertyFilter filter = new PropertyFilter() {
			@Override
			public boolean apply(Object object, String name, Object value) {
				boolean flag = true;
				if (name.equals("outaccounts")) {
					flag = false;
				} else if (name.equals("user")) {
					flag = false;
				} else if (name.equals("outaccounttype_createtime")) {
					flag = false;
				}
				return flag;
			}
		};
		ResponseUtils.respObjAndFilter(ServletActionContext.getResponse(), outAccountType2, filter);
		return NONE;
	}

	// 修改支出类型
	public String update() {
		User u = (User) ServletActionContext.getContext().getSession().get("currentUser");
		outAccountType.setUser(u);
		outAccountTypeService.update(outAccountType);
		return "toShow";
	}

	// 根据ID删除支出类型
	public String delete() throws IOException {
		String flag = "0";
		try {
			outAccountTypeService.deleteById(outAccountType.getOutaccounttype_id());
		} catch (Exception e) {
			flag = "1";
			e.printStackTrace();
		}
		ServletActionContext.getResponse().setContentType("text/json;charset=utf-8");
		ServletActionContext.getResponse().getWriter().write(flag);
		return NONE;
	}

	// 查询所有支出账单类型名称
	public String findOutAccountTypeName() throws Exception {
		User user = (User) ActionContext.getContext().getSession().get("currentUser");
		List<Object> list = outAccountTypeService.findOutAccountTypeName(user.getUser_id());
		Map<String, Integer> outTypeMap = new HashMap<String, Integer>();
		int i = 1;
		for (Object object : list) {
			outTypeMap.put(object.toString(), i);
			i++;
		}
		ResponseUtils.respObj(ServletActionContext.getResponse(), outTypeMap);
		return NONE;
	}

	@Override
	public OutAccountType getModel() {
		return outAccountType;
	}

}
