package cool.xiaokang.familyaccount.controller;

import java.io.IOException;
import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.struts2.ServletActionContext;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.alibaba.fastjson.serializer.PropertyFilter;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cool.xiaokang.familyaccount.pojo.Manager;
import cool.xiaokang.familyaccount.pojo.Menu;
import cool.xiaokang.familyaccount.service.MenuService;
import cool.xiaokang.familyaccount.utils.PageBean;
import cool.xiaokang.familyaccount.utils.ResponseUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Controller
@Scope("prototype")
public class MenuAction extends ActionSupport implements ModelDriven<Menu> {
	private static final long serialVersionUID = 3669156728721717873L;

	@Autowired
	private MenuService menuService;

	private Menu menu = new Menu();
	private Integer page;
	private Integer rows;
	private String role_id;

	public String listAjax() throws IOException {
		List<Menu> menus = menuService.findAjax();
		PropertyFilter filter = new PropertyFilter() {
			@Override
			public boolean apply(Object object, String name, Object value) {
				boolean flag = true;
				if (name.equals("parentMenu")) {
					flag = false;
				}
				return flag;
			}
		};
		ResponseUtils.respList(ServletActionContext.getResponse(), menus, filter);
		return NONE;
	}

	// 给角色授权
	public String authForRole() throws IOException {
		List<Menu> menus = menuService.findMenuForRole();
		PropertyFilter filter = new PropertyFilter() {
			@Override
			public boolean apply(Object object, String name, Object value) {
				boolean flag = true;
				if (name.equals("parentMenu")) {
					flag = false;
				} else if (name.equals("menu_name")) {
					flag = false;
				} else if (name.equals("menu_page")) {
					flag = false;
				}
				return flag;
			}
		};
		ResponseUtils.respList(ServletActionContext.getResponse(), menus, filter);
		return NONE;
	}

	// 查询权限
	@RequiresPermissions("menu-list")
	public String pageQuery() throws IOException {
		DetachedCriteria dc = DetachedCriteria.forClass(Menu.class);
		dc.addOrder(Order.asc("zindex"));
		PageBean pb = new PageBean();
		pb.setDetachedCriteria(dc);
		pb.setCurrentPage(page);
		pb.setPageSize(rows);
		menuService.pageQuery(pb);
		PropertyFilter filter = new PropertyFilter() {
			@Override
			public boolean apply(Object object, String name, Object value) {
				boolean flag = true;
				if (name.equals("detachedCriteria")) {
					flag = false;
				} else if (name.equals("currentPage")) {
					flag = false;
				} else if (name.equals("pageSize")) {
					flag = false;
				} else if (name.equals("parentMenu")) {
					flag = false;
				}
				return flag;
			}
		};
		ResponseUtils.respPageBean(ServletActionContext.getResponse(), pb, filter);
		return NONE;
	}

	@RequiresPermissions("menu-add")
	public String add() {
		menuService.save(menu);
		return "list";
	}

	@RequiresPermissions("menu-update")
	public String foredit() {
		Menu model = menuService.findById(menu.getMenu_id());
		System.out.println(model.getMenu_name());
		ActionContext.getContext().put("model", model);
		return "edit";
	}

	@RequiresPermissions("menu-update")
	public String update() {
		menuService.update(menu);
		return "list";
	}

	public String findByRole() throws IOException {
		List<Menu> menus = menuService.findByRole(role_id);
		PropertyFilter filter = new PropertyFilter() {
			@Override
			public boolean apply(Object object, String name, Object value) {
				boolean flag = true;
				if (name.equals("parentMenu")) {
					flag = false;
				}
				return flag;
			}
		};
		ResponseUtils.respList(ServletActionContext.getResponse(), menus, filter);
		return NONE;
	}

	// 根据登录人查询菜单
	public String findMenu() throws IOException {
		Manager manager = (Manager) ActionContext.getContext().getSession().get("currentManager");
		List<Menu> menus = menuService.findMenu(manager);
		PropertyFilter filter = new PropertyFilter() {
			@Override
			public boolean apply(Object object, String name, Object value) {
				boolean flag = true;
				if (name.equals("parentMenu")) {
					flag = false;
				} else if (name.equals("menu_name")) {
					flag = false;
				} else if (name.equals("menu_page")) {
					flag = false;
				}
				return flag;
			}
		};
//		System.out.println(JSON.toJSONString(menus,filter));
		ResponseUtils.respList(ServletActionContext.getResponse(), menus, filter);
		return NONE;
	}

	@Override
	public Menu getModel() {
		return menu;
	}
}
