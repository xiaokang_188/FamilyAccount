package cool.xiaokang.familyaccount.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cool.xiaokang.familyaccount.pojo.Reply;
import cool.xiaokang.familyaccount.pojo.Topic;
import cool.xiaokang.familyaccount.pojo.User;
import cool.xiaokang.familyaccount.service.InAccountService;
import cool.xiaokang.familyaccount.service.OutAccountService;
import cool.xiaokang.familyaccount.service.ReplyService;
import cool.xiaokang.familyaccount.service.TopicService;
import cool.xiaokang.familyaccount.utils.DateFormateUtil;

@Controller
@Scope("prototype")
public class MainAction extends ActionSupport {
	private static final long serialVersionUID = -2188256986739037734L;

	@Autowired
	private TopicService topicService;

	@Autowired
	private ReplyService replyService;

	@Autowired
	private InAccountService inAccountService;

	@Autowired
	private OutAccountService outAccountService;

	public String goIndex() {
		User user = (User) ActionContext.getContext().getSession().get("currentUser");
		// 当前用户最近发布的3条帖子
		List<Topic> topiclist = topicService.findThreeTopic(user.getUser_id());
		// 当前用户最近回复的3条帖子
		List<Reply> replylist = replyService.findThreeReply(user.getUser_id());
		// 当前用户当月总收入和总支出
		Double totalIn = inAccountService.findCurrentMonthTotalMoney(user.getUser_id());
		Double totalOut = outAccountService.findCurrentMonthTotalMoney(user.getUser_id());
		if (totalIn == null) {
			totalIn = 0D;
		}
		if (totalOut == null) {
			totalOut = 0D;
		}
		ActionContext context = ActionContext.getContext();
		context.put("topiclist", topiclist);
		context.put("replylist", replylist);
		context.put("in_money", DateFormateUtil.formatMoney(totalIn));
		context.put("out_money", DateFormateUtil.formatMoney(totalOut));
		return "main";
	}

}
