package cool.xiaokang.familyaccount.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate5.HibernateCallback;
import org.springframework.stereotype.Repository;

import cool.xiaokang.familyaccount.dao.OutAccoutTypeDao;
import cool.xiaokang.familyaccount.dao.base.impl.BaseDaoImpl;
import cool.xiaokang.familyaccount.pojo.OutAccountType;

@Repository
public class OutAccountTypeDaoImpl extends BaseDaoImpl<OutAccountType> implements OutAccoutTypeDao {

	// 通过支出类型名称和用户id查找支出类型
	public OutAccountType findByTypeNameAndUserId(final String typename, final Long user_id) {
		return this.getHibernateTemplate().execute(new HibernateCallback<OutAccountType>() {
			@Override
			public OutAccountType doInHibernate(Session session) throws HibernateException {
				String hql = "FROM OutAccountType where outaccounttype_name=? and user.user_id=?";
				Query query = session.createQuery(hql);
				query.setParameter(0, typename);
				query.setParameter(1, user_id);
				OutAccountType outAccountType = (OutAccountType) query.uniqueResult();
				return outAccountType;
			}
		});
	}

	@SuppressWarnings("unchecked")
	public List<Object> findOutAccountTypeName(Long user_id) {
		String hql = "SELECT oat.outaccounttype_name FROM OutAccountType oat LEFT JOIN oat.user u where u.user_id=?";
		return (List<Object>) this.getHibernateTemplate().find(hql, user_id);
	}
}
