package cool.xiaokang.familyaccount.dao.impl;

import org.springframework.stereotype.Repository;

import cool.xiaokang.familyaccount.dao.LoanDao;
import cool.xiaokang.familyaccount.dao.base.impl.BaseDaoImpl;
import cool.xiaokang.familyaccount.pojo.Loan;

@Repository
public class LoanDaoImpl extends BaseDaoImpl<Loan> implements LoanDao {
}
