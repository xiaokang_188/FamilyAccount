package cool.xiaokang.familyaccount.dao.impl;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate5.HibernateCallback;
import org.springframework.stereotype.Repository;

import cool.xiaokang.familyaccount.dao.ManagerDao;
import cool.xiaokang.familyaccount.dao.base.impl.BaseDaoImpl;
import cool.xiaokang.familyaccount.pojo.Manager;

@Repository
public class ManagerDaoImpl extends BaseDaoImpl<Manager> implements ManagerDao {

	@Override
	public Manager findManagerByName(final String manager_name) {
		return this.getHibernateTemplate().execute(new HibernateCallback<Manager>() {

			@Override
			public Manager doInHibernate(Session session) throws HibernateException {
				String hql = "FROM Manager WHERE manager_name=:manager_name";
				Query query = session.createQuery(hql);
				query.setParameter("manager_name", manager_name);
				return (Manager) query.uniqueResult();
			}
		});

	}

}
