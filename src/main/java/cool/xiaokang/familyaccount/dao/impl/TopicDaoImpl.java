package cool.xiaokang.familyaccount.dao.impl;

import org.springframework.stereotype.Repository;

import cool.xiaokang.familyaccount.dao.TopicDao;
import cool.xiaokang.familyaccount.dao.base.impl.BaseDaoImpl;
import cool.xiaokang.familyaccount.pojo.Topic;

@Repository
public class TopicDaoImpl extends BaseDaoImpl<Topic> implements TopicDao {
}
