package cool.xiaokang.familyaccount.dao.impl;

import org.springframework.stereotype.Repository;

import cool.xiaokang.familyaccount.dao.InvestDao;
import cool.xiaokang.familyaccount.dao.base.impl.BaseDaoImpl;
import cool.xiaokang.familyaccount.pojo.Invest;

@Repository
public class InvestDaoImpl extends BaseDaoImpl<Invest> implements InvestDao {
}
