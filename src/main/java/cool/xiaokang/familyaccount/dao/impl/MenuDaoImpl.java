package cool.xiaokang.familyaccount.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import cool.xiaokang.familyaccount.dao.MenuDao;
import cool.xiaokang.familyaccount.dao.base.impl.BaseDaoImpl;
import cool.xiaokang.familyaccount.pojo.Menu;

@Repository
@SuppressWarnings("unchecked")
public class MenuDaoImpl extends BaseDaoImpl<Menu> implements MenuDao {

	@Override
	public List<Menu> findAjax() {
		String hql = "FROM Menu m WHERE m.parentMenu is null";
		List<Menu> list = (List<Menu>) this.getHibernateTemplate().find(hql);
		return list;
	}

	@Override
	public List<Menu> findByRole(String role_id) {
		String hql = "SELECT DISTINCT m FROM Menu m LEFT JOIN m.roles r WHERE r.role_id=?";
		List<Menu> list = (List<Menu>) this.getHibernateTemplate().find(hql, role_id);
		return list;
	}

	// 根据管理员ID查询对应权限
	public List<Menu> findByManagerId(String manager_id) {
		String hql = "SELECT DISTINCT m FROM Menu m LEFT JOIN m.roles r LEFT OUTER JOIN r.managers ma WHERE ma.manager_id=?";
		List<Menu> list = (List<Menu>) this.getHibernateTemplate().find(hql, manager_id);
		return list;
	}

	// 根据管理员id查询对应菜单
	public List<Menu> findMenuByManagerId(String manager_id) {
		String hql = "SELECT DISTINCT m FROM Menu m LEFT JOIN m.roles r LEFT JOIN r.managers ma WHERE  m.generatemenu = '1' AND ma.manager_id=?  ORDER BY m.zindex DESC";
		List<Menu> list = (List<Menu>) this.getHibernateTemplate().find(hql, manager_id);
		return list;
	}

	// 查询所有菜单
	public List<Menu> findAllMenu() {
		String hql = "FROM Menu m WHERE m.generatemenu = '1' ORDER BY m.zindex DESC";
		List<Menu> list = (List<Menu>) this.getHibernateTemplate().find(hql);
		return list;
	}

	// 给角色授权
	public List<Menu> findAllMenuForRole() {
		String hql = "FROM Menu ORDER BY zindex DESC";
		List<Menu> list = (List<Menu>) this.getHibernateTemplate().find(hql);
		return list;
	}

}
