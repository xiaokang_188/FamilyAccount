package cool.xiaokang.familyaccount.dao.impl;

import org.springframework.stereotype.Repository;

import cool.xiaokang.familyaccount.dao.RegionDao;
import cool.xiaokang.familyaccount.dao.base.impl.BaseDaoImpl;
import cool.xiaokang.familyaccount.pojo.Region;

@Repository
public class RegionDaoimpl extends BaseDaoImpl<Region> implements RegionDao {
}
