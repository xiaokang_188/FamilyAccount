package cool.xiaokang.familyaccount.dao.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import cool.xiaokang.familyaccount.dao.OutAccountDao;
import cool.xiaokang.familyaccount.dao.base.impl.BaseDaoImpl;
import cool.xiaokang.familyaccount.pojo.OutAccount;
import cool.xiaokang.familyaccount.utils.DateFormateUtil;

@Repository
public class OutAccountDaoImpl extends BaseDaoImpl<OutAccount> implements OutAccountDao {
	// 根据用户id和账单类型分类，查询当前年或某年支出账单金额总和
	@SuppressWarnings("unchecked")
	public List<Object> findOutAccountGroupByOutAccountTypeName(Long user_id, String year) {
		String hql = "";
		if (StringUtils.isNotBlank(year)) {
			hql = "SELECT oat.outaccounttype_name,SUM(oa.outaccount_money) FROM OutAccount oa LEFT JOIN oa.outAccountType oat LEFT JOIN oat.user u where u.user_id=? AND YEAR(oa.outaccount_datetime)=YEAR(?) GROUP BY oat.outaccounttype_name";
			return (List<Object>) this.getHibernateTemplate().find(hql, user_id, DateFormateUtil.transferYear(year));
		} else {
			hql = "SELECT oat.outaccounttype_name,SUM(oa.outaccount_money) FROM OutAccount oa LEFT JOIN oa.outAccountType oat LEFT JOIN oat.user u where u.user_id=? AND YEAR(oa.outaccount_datetime)=YEAR(NOW()) GROUP BY oat.outaccounttype_name";
			return (List<Object>) this.getHibernateTemplate().find(hql, user_id);
		}

	}

	// 查询当月总支出总额
	@SuppressWarnings("rawtypes")
	public Double findCurrentMonthTotalMoney(Long user_id) {
		String hql = "SELECT SUM(oa.outaccount_money) FROM OutAccount oa  WHERE oa.user.user_id=? AND MONTH(outaccount_datetime)=MONTH(NOW())";
		List list = this.getHibernateTemplate().find(hql, user_id);
		if (list != null && list.size() > 0) {
			Double money = (Double) list.get(0);
			return money;
		}
		return null;
	}

	// 根据用户id和账单类型分类，查询当前年或某年，各月的支出账单总额
	@SuppressWarnings("unchecked")
	public List<Object> findOutAccountWithYearly(Long user_id, String year) {
		String hql = "";
		if (StringUtils.isNotBlank(year)) {
			hql = "SELECT oat.outaccounttype_name,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '1' then oa.outaccount_money else 0 end)) as Jan,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '2' then oa.outaccount_money else 0 end)) as Feb,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '3' then oa.outaccount_money else 0 end)) as Mar,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '4' then oa.outaccount_money else 0 end)) as Apr,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '5' then oa.outaccount_money else 0 end)) as May,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '6' then oa.outaccount_money else 0 end)) as Jun,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '7' then oa.outaccount_money else 0 end)) as Jul,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '8' then oa.outaccount_money else 0 end)) as Aug,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '9' then oa.outaccount_money else 0 end)) as Sep,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '10' then oa.outaccount_money else 0 end)) as Oct,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '11' then oa.outaccount_money else 0 end)) as Nov,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '12' then oa.outaccount_money else 0 end)) as Dec"
					+ " FROM OutAccount oa LEFT JOIN oa.outAccountType oat LEFT JOIN oat.user u where u.user_id=? AND YEAR(oa.outaccount_datetime)=YEAR(?) GROUP BY oat.outaccounttype_name";
			return (List<Object>) this.getHibernateTemplate().find(hql, user_id, DateFormateUtil.transferYear(year));
		} else {
			hql = "SELECT oat.outaccounttype_name,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '1' then oa.outaccount_money else 0 end)) as Jan,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '2' then oa.outaccount_money else 0 end)) as Feb,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '3' then oa.outaccount_money else 0 end)) as Mar,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '4' then oa.outaccount_money else 0 end)) as Apr,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '5' then oa.outaccount_money else 0 end)) as May,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '6' then oa.outaccount_money else 0 end)) as Jun,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '7' then oa.outaccount_money else 0 end)) as Jul,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '8' then oa.outaccount_money else 0 end)) as Aug,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '9' then oa.outaccount_money else 0 end)) as Sep,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '10' then oa.outaccount_money else 0 end)) as Oct,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '11' then oa.outaccount_money else 0 end)) as Nov,"
					+ "SUM((case MONTH(oa.outaccount_datetime) when '12' then oa.outaccount_money else 0 end)) as Dec"
					+ " FROM OutAccount oa LEFT JOIN oa.outAccountType oat LEFT JOIN oat.user u where u.user_id=? AND YEAR(oa.outaccount_datetime)=YEAR(NOW()) GROUP BY oat.outaccounttype_name";
			return (List<Object>) this.getHibernateTemplate().find(hql, user_id);
		}
	}

	// 查询今年或某年每个月的平均支出
	@SuppressWarnings("unchecked")
	public List<Object> findPerMonthAvgMoney(Long user_id, String year) {
		String hql = "";
		if (StringUtils.isNotBlank(year)) {
			hql = "SELECT "
					+ "AVG((case MONTH(oa.outaccount_datetime) when '1' then oa.outaccount_money else 0 end)) as Jan,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '2' then oa.outaccount_money else 0 end)) as Feb,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '3' then oa.outaccount_money else 0 end)) as Mar,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '4' then oa.outaccount_money else 0 end)) as Apr,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '5' then oa.outaccount_money else 0 end)) as May,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '6' then oa.outaccount_money else 0 end)) as Jun,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '7' then oa.outaccount_money else 0 end)) as Jul,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '8' then oa.outaccount_money else 0 end)) as Aug,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '9' then oa.outaccount_money else 0 end)) as Sep,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '10' then oa.outaccount_money else 0 end)) as Oct,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '11' then oa.outaccount_money else 0 end)) as Nov,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '12' then oa.outaccount_money else 0 end)) as Dec"
					+ " FROM OutAccount oa  WHERE oa.user.user_id=? AND YEAR(oa.outaccount_datetime)=YEAR(?) GROUP BY MONTH(oa.outaccount_datetime)";
			return (List<Object>) this.getHibernateTemplate().find(hql, user_id, DateFormateUtil.transferYear(year));
		} else {
			hql = "SELECT "
					+ "AVG((case MONTH(oa.outaccount_datetime) when '1' then oa.outaccount_money else 0 end)) as Jan,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '2' then oa.outaccount_money else 0 end)) as Feb,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '3' then oa.outaccount_money else 0 end)) as Mar,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '4' then oa.outaccount_money else 0 end)) as Apr,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '5' then oa.outaccount_money else 0 end)) as May,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '6' then oa.outaccount_money else 0 end)) as Jun,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '7' then oa.outaccount_money else 0 end)) as Jul,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '8' then oa.outaccount_money else 0 end)) as Aug,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '9' then oa.outaccount_money else 0 end)) as Sep,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '10' then oa.outaccount_money else 0 end)) as Oct,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '11' then oa.outaccount_money else 0 end)) as Nov,"
					+ "AVG((case MONTH(oa.outaccount_datetime) when '12' then oa.outaccount_money else 0 end)) as Dec"
					+ " FROM OutAccount oa  WHERE oa.user.user_id=? AND YEAR(oa.outaccount_datetime)=YEAR(NOW()) GROUP BY MONTH(oa.outaccount_datetime)";
			return (List<Object>) this.getHibernateTemplate().find(hql, user_id);
		}
	}

}
