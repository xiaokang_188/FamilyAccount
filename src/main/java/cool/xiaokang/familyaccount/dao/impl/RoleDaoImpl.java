package cool.xiaokang.familyaccount.dao.impl;

import org.springframework.stereotype.Repository;

import cool.xiaokang.familyaccount.dao.RoleDao;
import cool.xiaokang.familyaccount.dao.base.impl.BaseDaoImpl;
import cool.xiaokang.familyaccount.pojo.Role;

@Repository
public class RoleDaoImpl extends BaseDaoImpl<Role> implements RoleDao {
}
