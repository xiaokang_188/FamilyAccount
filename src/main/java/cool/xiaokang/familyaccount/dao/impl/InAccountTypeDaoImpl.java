package cool.xiaokang.familyaccount.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate5.HibernateCallback;
import org.springframework.stereotype.Repository;

import cool.xiaokang.familyaccount.dao.InAccountTypeDao;
import cool.xiaokang.familyaccount.dao.base.impl.BaseDaoImpl;
import cool.xiaokang.familyaccount.pojo.InAccountType;

@Repository
public class InAccountTypeDaoImpl extends BaseDaoImpl<InAccountType> implements InAccountTypeDao {

	@Override
	public InAccountType findByTypeNameAndUserId(final String typename, final Long user_id) {
		return this.getHibernateTemplate().execute(new HibernateCallback<InAccountType>() {
			@Override
			public InAccountType doInHibernate(Session session) throws HibernateException {
				String hql = "from InAccountType where inaccounttype_name=? and user.user_id=?";
				Query query = session.createQuery(hql);
				query.setParameter(0, typename);
				query.setParameter(1, user_id);
				InAccountType inAccountType = (InAccountType) query.uniqueResult();
				return inAccountType;
			}
		});
	}

	@SuppressWarnings("unchecked")
	public List<Object> findInAccountTypeName(Long user_id) {
		String hql = "SELECT iat.inaccounttype_name FROM InAccountType iat LEFT JOIN iat.user u where u.user_id=?";
		return (List<Object>) this.getHibernateTemplate().find(hql, user_id);
	}
}
