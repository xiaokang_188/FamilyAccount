package cool.xiaokang.familyaccount.dao;

import java.util.List;

import cool.xiaokang.familyaccount.dao.base.BaseDao;
import cool.xiaokang.familyaccount.pojo.OutAccount;

public interface OutAccountDao extends BaseDao<OutAccount> {

	public List<Object> findOutAccountGroupByOutAccountTypeName(Long user_id, String year);

	public Double findCurrentMonthTotalMoney(Long user_id);

	public List<Object> findOutAccountWithYearly(Long user_id, String year);

	public List<Object> findPerMonthAvgMoney(Long user_id, String year);

}
