package cool.xiaokang.familyaccount.dao;

import java.util.List;

import cool.xiaokang.familyaccount.dao.base.BaseDao;
import cool.xiaokang.familyaccount.pojo.InAccount;

public interface InAccountDao extends BaseDao<InAccount> {

	public List<Object> findInAccountGroupByInAccountTypeName(Long user_id, String year);

	public List<Object> findInAccountWithYearly(Long user_id, String year);

	public Double findCurrentMonthTotalMoney(Long user_id);

	public List<Object> findPerMonthAvgMoney(Long user_id, String year);

}
