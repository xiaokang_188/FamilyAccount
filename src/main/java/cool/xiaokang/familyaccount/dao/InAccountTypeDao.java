package cool.xiaokang.familyaccount.dao;

import java.util.List;

import cool.xiaokang.familyaccount.dao.base.BaseDao;
import cool.xiaokang.familyaccount.pojo.InAccountType;

public interface InAccountTypeDao extends BaseDao<InAccountType> {

	/**
	 * 根据用户id查找所有收入账单类型名称
	 * 
	 * @param user_id
	 * @return
	 */
	public List<Object> findInAccountTypeName(Long user_id);

	public InAccountType findByTypeNameAndUserId(String typename, Long user_id);

}
