package cool.xiaokang.familyaccount.dao;

import java.util.List;

import cool.xiaokang.familyaccount.dao.base.BaseDao;
import cool.xiaokang.familyaccount.pojo.Menu;

public interface MenuDao extends BaseDao<Menu> {
	public List<Menu> findAjax();

	public List<Menu> findByRole(String role_id);

	public List<Menu> findByManagerId(String manager_id);

	public List<Menu> findAllMenu();

	public List<Menu> findAllMenuForRole();

	public List<Menu> findMenuByManagerId(String manager_id);

}
