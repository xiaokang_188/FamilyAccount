package cool.xiaokang.familyaccount.dao;

import java.util.List;

import cool.xiaokang.familyaccount.dao.base.BaseDao;
import cool.xiaokang.familyaccount.pojo.Reply;

public interface ReplyDao extends BaseDao<Reply> {
	public List<Reply> findByTopicAndUser(Long user_id);
}
