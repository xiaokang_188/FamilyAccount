package cool.xiaokang.familyaccount.dao;

import cool.xiaokang.familyaccount.dao.base.BaseDao;
import cool.xiaokang.familyaccount.pojo.Topic;

public interface TopicDao extends BaseDao<Topic> {
}
