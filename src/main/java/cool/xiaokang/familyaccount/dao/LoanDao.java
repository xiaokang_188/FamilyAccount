package cool.xiaokang.familyaccount.dao;

import cool.xiaokang.familyaccount.dao.base.BaseDao;
import cool.xiaokang.familyaccount.pojo.Loan;

public interface LoanDao extends BaseDao<Loan> {
}
