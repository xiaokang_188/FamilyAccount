package cool.xiaokang.familyaccount.dao;

import cool.xiaokang.familyaccount.dao.base.BaseDao;
import cool.xiaokang.familyaccount.pojo.Role;

public interface RoleDao extends BaseDao<Role> {
}
