package cool.xiaokang.familyaccount.dao;

import cool.xiaokang.familyaccount.dao.base.BaseDao;
import cool.xiaokang.familyaccount.pojo.User;

public interface UserDao extends BaseDao<User> {

	/**
	 * 根据用户名或邮箱查找用户
	 */
	public User getUserByName(String user_name);

	public void persistUser(User user);

	public User findUserByName(String user_name);

	public User findUserByEmail(String email);

}
