package cool.xiaokang.familyaccount.dao;

import java.util.List;

import cool.xiaokang.familyaccount.dao.base.BaseDao;
import cool.xiaokang.familyaccount.pojo.OutAccountType;

public interface OutAccoutTypeDao extends BaseDao<OutAccountType> {

	/**
	 * 
	 * @Description: 根据支出类型名称和用户id查找支出类型实体
	 * @return OutAccountType
	 * @param typename
	 */
	public OutAccountType findByTypeNameAndUserId(String typename, Long user_id);

	/**
	 * 根据用户id查找所有支出账单类型名称
	 * 
	 * @param user_id
	 * @return
	 */
	public List<Object> findOutAccountTypeName(Long user_id);

}
