package cool.xiaokang.familyaccount.dao;

import cool.xiaokang.familyaccount.dao.base.BaseDao;
import cool.xiaokang.familyaccount.pojo.Region;

public interface RegionDao extends BaseDao<Region> {
}
