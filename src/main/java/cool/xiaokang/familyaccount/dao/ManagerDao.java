package cool.xiaokang.familyaccount.dao;

import cool.xiaokang.familyaccount.dao.base.BaseDao;
import cool.xiaokang.familyaccount.pojo.Manager;

public interface ManagerDao extends BaseDao<Manager> {
	public Manager findManagerByName(String manager_name);
}
