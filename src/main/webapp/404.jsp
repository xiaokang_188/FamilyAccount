<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
<meta charset="UTF-8">
<title>你来到了神秘地域</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,user-scalable=no">
<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/404.css">
<link rel="icon" href="${pageContext.request.contextPath}/img/xiaokang.png" type="image/x-icon" />
</head>
<body>
	<div class="text">
		<h1>404</h1>
		<h2>Uh, Ohh</h2>
		<h3>对不起，我们找不到你要找的东西，因为这里太黑了</h3>
	</div>
	<div class="torch"></div>
	<script src='${pageContext.request.contextPath}/assets/js/jquery.min.js'></script>
	<script src="${pageContext.request.contextPath}/assets/js/script.js"></script>
</body>
</html>