<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
		<title>用户注册界面</title>
		<link href="${basePath }/css/findpwd.css" />
		<script type="text/javascript">
			window.onload=function(){
				let time=5;
				time--;
				let second=document.getElementById("second");
					let timer=setInterval(function(){
						second.innerHTML=time;
						time--;
						if(time==0){
							clearInterval(timer);
							this.location="${basePath}/login.jsp";
						}
					},1000);
			}
		</script>
	</head>
	<body background="${basePath }/img/bg.jpg">
		<!--container主容器1-->
		<div class="container">
			<div class="page-header">
					<ol class="list-inline" style="margin-bottom:-10px;">
						<li>
							<h3>家庭记账系统 <small style="color: orangered;">用户注册</small></h3>
						</li>
						<li>
							<p>立即<a class="btn btn-default" href="${basePath }/login.jsp" style="font-weight: bolder;">登录</a></p>
						</li>
					</ol>
			</div>
		</div>
		<!--container主容器1end-->
		<!--container主容器2-->
		<div class="container">
			<!--row-->
			<div class="row">
				<div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6" style="margin-top: 10px;font-size: 16px;background-color: #ECEDF1;" align="center">
					<!--内部ROW-->
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<img src="${basePath }/img/success.png" class="img-responsive" />
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<p style="font-size: 18px;color: green;margin-left: 5px;">注册账号成功!</p>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							还有
							<h2 style="color: red;display: inline;" id="second">5</h2>秒钟自动跳转到登录界面！
						</div>
					</div>
					<!--内部ROW END-->
				</div>
				<!--rowDIV END-->
			</div>
			<!--row END-->
		</div>
		<!--container2 END-->
	</body>
</html>