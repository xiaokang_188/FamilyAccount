<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,user-scalable=no">
<title>对不起，服务器内部错误！</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/assets/css/500.css">
<link rel="icon" href="${pageContext.request.contextPath}/imges/xiaokang.png" type="image/x-icon" />
</head>
<body>
	<div id="wrapper">
		<a class="logo" href=""></a>
		<div id="main">
			<div id="header">
				<h1>
					<span class="icon">!</span>500<span class="sub">Internal Server Error</span>
				</h1>
			</div>
			<div id="content">
				<h2>服务器内部错误！</h2>
				<p>当您看到这个页面,表示服务器内部错误,此网站可能遇到技术问题,无法执行您的请求,请稍后重试或联系站长进行处理,感谢您的支持!</p>
				<div class="utilities">
					<div class="input-container"
						style="font: 13px 'TeXGyreScholaRegular', Arial, sans-serif; color: #696969; text-shadow: 0 1px white; text-decoration: none;">
						<span id="totalSecond" style="color: red">5</span>秒后自动跳转…
					</div>
					<a class="button right" href="javascript:history.back(-1)">返回...</a>
					<a class="button right"
						href="http://wpa.qq.com/msgrd?v=3&uin=1181259634&site=qq&menu=yes">联系站长</a>
					<div class="clear"></div>
				</div>
			</div>

		</div>
	</div>

	<!--定义js变量及方法-->
	<script language="javascript" type="text/javascript">
		let second = document.getElementById('totalSecond').textContent;
		if (navigator.appName.indexOf("Explorer") > -1) {
			second = document.getElementById('totalSecond').innerText;
		} else {
			second = document.getElementById('totalSecond').textContent;
		}
		setInterval("redirect()", 1000);
		function redirect() {
			if (second < 0) {
				<!--定义倒计时后跳转页面-->
				location.href = '${pageContext.request.contextPath}/login.jsp';
			} else {
				if (navigator.appName.indexOf("Explorer") > -1) {
					document.getElementById('totalSecond').innerText = second--;
				} else {
					document.getElementById('totalSecond').textContent = second--;
				}
			}
		}
	</script>
</html>