<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,user-scalable=no">
<title>收入账单类型列表</title>
<style>
th {
	padding: 0;
	margin: 0;
	background-color: #d9edf7;
}
#inaccounttype tbody>tr:hover {
	background-color: #449d44;
}
</style>
</head>
<body>
	<%@include file="/WEB-INF/user/common/head.jsp"%>
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li><a href="${basePath }/index.jsp">首页</a></li>
			<li><a href="javascript:;">收入账单</a></li>
			<li>收入账单类型列表</li>
		</ol>

		<div class="well">
			<form class="form-inline">
				<strong style="margin-left: 20px;">支出类型名称:</strong>
				<input placeholder="输入收入账单类型名称" style="margin-right: 25px;" type="text" class="form-control" id="inaccounttype_name" name="inaccounttype_name" value="<s:property value="inaccounttype_name"/>"/>
				<button class="btn btn-primary" id="searchBtn"><span class="glyphicon glyphicon-search"></span>搜索</button>
				<a class="btn btn-success pull-right" href="${basePath }/page_user_inaccounttype_add.action"><span class="glyphicon glyphicon-plus"></span>添加收入账单类型</a>
			</form>
		</div>

		<table id="inaccounttype"></table>

		<!-- ====================修改收入账单类型的模态框================================ -->
		<div class="modal fade" id="editForm" tabindex="-1" role="dialog"
			aria-labelledby="editFormLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" aria-label="Close"
							onclick="closeData()">
							<span aria-hidden="true">×</span>
						</button>
						<h5 class="modal-title" id="exampleModal3Label">
							<span class="glyphicon glyphicon-edit"></span>修改收入类型
						</h5>
					</div>
					<div class="modal-body" align="center">
						<form id="updateForm" action="${basePath }/InAccountTypeAction_update.action" method="post">
							<div class="form-group form-inline">
								<label for="" class="control-label" style="color: red;"><b>编号:</b></label>
								<input type="text"  class="form-control" id="outid" name="inaccounttype_id" readonly="readonly" style="width: 240px;">
							</div>
							<div class="form-group form-inline" style="margin-right: 58px;">
								<label for="recipient-name" class="control-label">收入类型名称:</label>
								<input class="form-control" type="text" style="width: 240px;" name="inaccounttype_name" required="required"/>
							</div>
							<div class="form-group form-inline" style="margin-right: 28px;">
								<label for="recipient-name" class="control-label">创建时间:</label>
								<input type="text" class="form-control" style="width: 240px;" name="inaccounttype_createtime" readonly="readonly"/>
							</div>
							<div class="form-group form-inline">
								<label for="message-text" class="control-label">备注:</label>
								<textarea class="form-control" id="message-text"
									style="width: 240px;" name="inaccounttype_desc"></textarea>
							</div>
							<div class="form-group form-inline">
								<input class="form-control btn btn-warning" value="提交"
								   type="submit" />
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary"
							onclick="closeData()" id="closeData">关闭</button>
					</div>
				</div>
			</div>
		</div>
		<!-- ========================修改收入账单类型的模态框end============================ -->
		<%@include file="/WEB-INF/user/common/foot.jsp"%>
		<script>
			//单个删除
			function delInAccountTypeById(id) {
				Ewin.confirm({message : "确认要删除编号为" + id + "的数据吗？"}).on(function(e) {
					if (e) {
						Ewin.confirm({message : "再次确认要删除编号为"+ id+ "的数据吗？"}).on(function(e) {
							if (e) {
								$.post("${basePath}/InAccountTypeAction_delete.action",{"inaccounttype_id":id},function(data){
			                		 if(data=="0"){
			                			 Ewin.alert({message:"删除成功！"});
			                			 window.location.reload();
			                		 }else{
			                			 Ewin.alert({message:"该收入类型下有账单,无法删除！"});
			                		 }
			                	 })
							}
						});
					}
				});
			}
			function closeData() {
				$('#editForm').modal('hide');
			}
			//打开模态框
			function openData(id) {
				let inaccounttypeid = 0;
				//手动开启
				$('#editForm').modal({
					backdrop : 'static',
					keyboard : false
				}); //禁止esc和点击模态框外部关闭
				//填充模态框表单
				 $.post("${basePath}/InAccountTypeAction_findById.action",{"inaccounttype_id":id},function(data){
						$("input[name=inaccounttype_id]").val(data.inaccounttype_id);
						$("input[name=inaccounttype_name]").val(data.inaccounttype_name);
						$("textarea[name=inaccounttype_desc]").val(data.inaccounttype_desc);
						$("input[name=inaccounttype_createtime]").val(data.inaccounttype_createtime_s);
				 });
			}
		</script>
</body>
<script>
$(()=>{
	//初始化bootstrap-table参数
    let intypeName = $("#inaccounttype_name").val();
    $url = "InAccountTypeAction_listCurrentUser.action?intypeName="+ intypeName ;
	//console.info("初始化>>>"+$url);
    InitTable($url);
	//查询数据
	$("#searchBtn").click(function () {
		let intypeName = $("#inaccounttype_name").val();
	    let $url = "InAccountTypeAction_listCurrentUser.action?intypeName="+ intypeName ;
	    InitTable($url);
	});
});
let InitTable = function (url){
	//console.info("真正调用>>>"+url);
	//先销毁表格
	$('#inaccounttype').bootstrapTable("destroy");
	//加载表格
	$("#inaccounttype").bootstrapTable({
		url : url, //请求地址
		method : 'post',
		dataType: "json",//期待返回数据类型
		locale: "zh-CN",//中文支持
		height:400,
		showLoading : true,
		search: false,
		striped : true, //是否显示行间隔色
		sortable : false, //是否启用排序
		pageNumber : 1, //初始化加载第一页
		pagination : true,//是否分页
		sidePagination : 'client',//server:服务器端分页|client：前端分页
		pageSize : 5,//单页记录数
		pageList : [ 5,10],//可选择单页记录数
		showRefresh : false,//刷新按钮
		columns : [ {
			title : '序号',
			field : 'inaccounttype_id',
			align : 'center',
			valign : 'middle',
			sortable : true
		}, {
			title : '收入类型名称',
			field : 'inaccounttype_name',
			align : 'center',
			valign : 'middle',
		}, {
			title : '收入类型备注',
			field : 'inaccounttype_desc',
			align : 'center',
			valign : 'middle',
		}, {
			title : '添加时间',
			field : 'inaccounttype_createtime_s',
			align : 'center',
			valign : 'middle',
		}, {
			title : '操作',
			field : 'inaccounttype_id',
			width : 120,
			align : 'center',
			valign : 'middle',
			formatter : actionFormatter
		} ]
	});
	return InitTable;
}
//操作栏的格式化
function actionFormatter(value, row, index) {
	let inaccounttype_id = value;
	let result = "";
	result += "<a href='javascript:;' class='label label-primary' onclick=\"openData('"
			+ inaccounttype_id
			+ "')\" title='编辑'><span class='glyphicon glyphicon-pencil'></span></a>";
	result += "<a href='javascript:;' class='label label-danger' onclick=\"delInAccountTypeById('"
			+ inaccounttype_id
			+ "')\" title='删除'><span class='glyphicon glyphicon-remove'></span></a>";
	return result;
}
</script>
</html>