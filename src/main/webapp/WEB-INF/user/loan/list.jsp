<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,user-scalable=no">
<title>借款还贷列表</title>
<style>
th {
	padding: 0;
	margin: 0;
	background-color: #d9edf7;
}
#loan tbody>tr:hover {
	background-color: #449d44;
}
</style>
</head>
<body>
	<%@include file="/WEB-INF/user/common/head.jsp"%>
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li><a href="${basePath }/index.jsp">首页</a></li>
			<li><a href="javascript:;">我的账本</a></li>
			<li>借款还贷列表</li>
		</ol>

		<div class="well">
			<form class="form-inline">
				<b>借款名称:</b>
				<input style="margin-right: 25px;" type="text" class="form-control"
					 value="<s:property value="loan_name"/>"
					name="loan_name" id="loan_name"/>
				<b>日期:</b>
				<input type="text" data-date-format="yyyy-mm-dd" name="startTime"
					id="startTime" class="form-control date" placeholder="选择开始时间"
					readonly="readonly" value="<s:property value="startTime"/>"/>
				<b>-</b>
				<input style="margin-right: 25px;" type="text"
					data-date-format="yyyy-mm-dd" name="endTime" id="endTime"
					class="form-control date" placeholder="选择结束时间" readonly="readonly"
					value="<s:property value="endTime"/>"/>
				<b>备注内容:</b>
				<input style="margin-right: 25px;" type="text" class="form-control"
					id="desc" value="<s:property value="loan_desc"/>"
					name="loan_desc" /> <a class="btn btn-primary"
					onclick="resetForm()"> <span
					class="glyphicon glyphicon-refresh"></span>重置
				</a>
				<button class="btn btn-primary" id="searchBtn">
					<span class="glyphicon glyphicon-search"></span>搜索
				</button>
			</form>
		</div>

		<a style="float:right;" class="btn btn-success"
			href="${basePath }/page_user_loan_add.action"><span
			class="glyphicon glyphicon-plus"></span>添加借款账单</a>

		<table id="loan"></table>

		<!--==================修改借款还贷账单模态框=================-->
		<div class="modal fade" id="editForm" tabindex="-1" role="dialog"
			aria-labelledby="editFormLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" aria-label="Close"
							onclick="closeData()">
							<span aria-hidden="true">×</span>
						</button>
						<h5 class="modal-title" id="exampleModal3Label">
							<span class="glyphicon glyphicon-edit"></span>修改借款账单
						</h5>
					</div>
					<div class="modal-body" align="center">
						<form id="updateForm" action="${basePath }/LoanAction_update.action" method="post">
							<div class="form-group form-inline">
								<label for="" class="control-label" style="color: red;"><b>编号:</b></label>
								<input type="text"  class="form-control" id="loan_id" name="loan_id" readonly="readonly" style="width:300px;">
							</div>
							<div class="form-group form-inline">
								<label for="recipient-name" class="control-label">借款名称:</label>
								<input class="form-control" type="text" style="width: 240px;" name="loan_name" required="required"/>
							</div>
							<div class="form-group form-inline">
								<label for="recipient-name" class="control-label">借款时间:</label>
								<input class="form-control" readonly="readonly" type="text" style="width: 240px;" id="loandatetime" name="loan_datetime" required="required"/>
							</div>
							<div class="form-group form-inline" style="margin-right: 58px;">
								<label for="recipient-name" class="control-label">周期（年）:</label>
								<input class="form-control" type="text" style="width: 240px;" id="loandatetime" name="loan_year" required="required"/>
							</div>
							<div class="form-group form-inline" style="padding-left: 10px">
								<label for="recipient-name" class="control-label">利息率:</label>
								<input class="form-control" type="text" style="width: 240px;" name="interest_rates" required="required"/>
							</div>
							<div class="form-group form-inline">
								<label for="recipient-name" class="control-label">借款金额:</label>
								<input type="text" class="form-control"
									style="width: 240px;" name="loan_money"  required="required"/>
							</div>
							<div class="form-group form-inline">
								<label for="recipient-name" class="control-label">借款来源:</label>
								<input type="text" class="form-control"
									style="width: 240px;" name="loan_source" required="required"/>
							</div>
							<div class="form-group form-inline">
								<label for="recipient-name" class="control-label">创建时间:</label>
								<input type="text" class="form-control" 
									style="width: 240px;" name="loan_createtime" readonly="readonly"/>
							</div>
								<div class="form-group form-inline" style="padding-left: 25px">
								<label for="message-text" class="control-label">备注:</label>
								<textarea class="form-control" style="width: 240px;" name="loan_desc"></textarea>
							</div>
							<div class="form-group form-inline">
								<input class="form-control btn btn-warning" value="提交修改"
								type="submit" />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--==================修改借款还贷模态框end==============-->
		<%@include file="/WEB-INF/user/common/foot.jsp"%>
		<script>
			$(function() {
				let picker1 = $('#startTime').datetimepicker({
					pickerPosition : 'up-left',
					format : 'yyyy-mm-dd ',
					weekStart : 1,
					todayBtn : 1,
					autoclose : 1,
					todayHighlight : 1,
					startView : 2,
					minView : 2,
					forceParse : 0,
					language : 'zh-CN'
				});
				let picker2 = $('#endTime').datetimepicker({
					pickerPosition : 'up-left',
					format : 'yyyy-mm-dd ',
					weekStart : 1,
					todayBtn : 1,
					autoclose : 1,
					todayHighlight : 1,
					startView : 2,
					minView : 2,
					forceParse : 0,
					language : 'zh-CN'
				});
				//动态设置最小值  
				picker1.on('dp.change', function(e) {
					picker2.data('DateTimePicker').minDate(e.date);
				});
				//动态设置最大值  
				picker2.on('dp.change', function(e) {
					picker1.data('DateTimePicker').maxDate(e.date);
				});
			});
			$('#outaccounttime').datetimepicker({
				pickerPosition : 'bottom-right',
				format : 'yyyy-mm-dd ',
				language : 'zh-CN',
				weekStart : 1,
				todayBtn : 1,
				autoclose : 1,
				todayHighlight : 1,
				startView : 2,
				minView : 2,
				forceParse : 0
			});
			//单个删除
			function delLoanAccountById(id) {
				Ewin.confirm({message : "确认要删除编号为" + id + "的数据吗？"}).on(function(e) {
					if (e) {
						Ewin.confirm({message : "再次确认要删除编号为"+ id+ "的数据吗？"}).on(function(e) {
											if (e) {
												location.href = "${basePath}/LoanAction_delete.action?loan_id="+ id;
											}
										});
					}
				});
			}
			function closeData() {
				$('#editForm').modal('hide');
			}
			//打开模态框
			function openData(id) {
				let inaccounttypeid = 0;
				//手动开启
				$('#editForm').modal({
					backdrop : 'static',
					keyboard : false
				}); //禁止esc和点击模态框外部关闭
				 //填充模态框表单
				 $.post("${basePath}/LoanAction_findById.action",{"loan_id":id},function(data){
						$("input[name=loan_id]").val(data.loan_id);
						$("input[name=loan_name]").val(data.loan_name);
						$("input[name=loan_datetime]").val(data.loan_datetime_s);
						$("input[name=interest_rates]").val(data.interest_rates);
						$("input[name=loan_money]").val(data.loan_money);
						$("input[name=loan_year]").val(data.loan_year);
						$("input[name=loan_source]").val(data.loan_source);
						$("input[name=loan_createtime]").val(data.loan_createtime_s);
						$("textarea[name=loan_desc]").val(data.loan_desc);
						$("input[name=inaccount_datetime]").val(data.inaccount_datetime_s);
						$("input[name=inaccount_createtime]").val(data.inaccount_createtime_s);
				 });
			}
		</script>
</body>
<script>
$(()=>{
	//初始化bootstrap-table参数
    let loanname = $("#loan_name").val();
    let startTime = $("#startTime").val();
    let endTime = $("#endTime").val();
    let desc = $("#desc").val();
   	$url = "LoanAction_listCurrentUser.action?loanname=" + loanname + "&startTime="+ startTime + "&endTime=" +endTime + "&desc=" + desc;
	//console.info("初始化>>>"+$url);
    InitTable($url);
	//查询数据
	$("#searchBtn").click(function () {
		let loanname = $("#loan_name").val();
	    let startTime = $("#startTime").val();
	    let endTime = $("#endTime").val();
	    let desc = $("#desc").val();
   		let $url = "LoanAction_listCurrentUser.action?loanname=" + loanname + "&startTime="+ startTime + "&endTime=" +endTime + "&desc=" + desc;
   	    InitTable($url);
	});
});
let InitTable = function (url){
	//console.info("真正调用>>>"+url);
	//先销毁表格
	$('#loan').bootstrapTable("destroy");
	//加载表格
	$("#loan").bootstrapTable({
		url : url, //请求地址
		method : 'post',
		dataType: "json",//期待返回数据类型
		locale: "zh-CN",//中文支持
		height:400,
		showLoading : true,
		search: false,
		striped : true, //是否显示行间隔色
		sortable : false, //是否启用排序
		pageNumber : 1, //初始化加载第一页
		pagination : true,//是否分页
		sidePagination : 'client',//server:服务器端分页|client：前端分页
		pageSize : 5,//单页记录数
		pageList : [ 5,10],//可选择单页记录数
		showRefresh : false,//刷新按钮
		columns : [ {
			title : '序号',
			field : 'SerialNumber',
			align : 'center',
			valign : 'middle',
			formatter : function (value, row, index) {
				return index+1;
			}
		}, {
			title : '借款名称',
			field : 'loan_name',
			align : 'center',
			valign : 'middle',
		}, {
			title : '借款来源',
			field : 'loan_source',
			align : 'center',
			valign : 'middle',
		}, {
			title : '借款时间',
			field : 'loan_datetime_s',
			align : 'center',
			valign : 'middle',
		}, {
			title : '借款周期（年）',
			field : 'loan_year',
			align : 'center',
			valign : 'middle',
		}, {
			title : '借款金额',
			field : 'loan_money',
			align : 'center',
			valign : 'middle',
		},{
			title : '利息率',
			field : 'interest_rates',
			align : 'center',
			valign : 'middle',
		},{
			title : '预期应还利息',
			field : 'lixi',
			align : 'center',
			valign : 'middle',
		},{
			title : '账单创建时间',
			field : 'loan_createtime_s',
			align : 'center',
			valign : 'middle',
		},{
			title : '备注内容',
			field : 'loan_desc',
			align : 'center',
			valign : 'middle',
		}, {
			title : '是否到期',
			field : 'overtime',
			align : 'center',
			valign : 'middle',
		}, {
			title : '操作',
			field : 'loan_id',
			width : 120,
			align : 'center',
			valign : 'middle',
			formatter : actionFormatter
		}]
	});
	return InitTable;
}
//操作栏的格式化
function actionFormatter(value, row, index) {
	let loan_id = value;
	let result = "";
	result += "<a href='javascript:;' class='label label-primary' onclick=\"openData('"
			+ loan_id
			+ "')\" title='编辑'><span class='glyphicon glyphicon-pencil'></span></a>";
	result += "<a href='javascript:;' class='label label-danger' onclick=\"delLoanAccountById('"
			+ loan_id
			+ "')\" title='删除'><span class='glyphicon glyphicon-remove'></span></a>";
	return result;
}
//清空搜索表单
function resetForm() {
	$("#loan_name").val('');
	$("#startTime").val('');
	$("#endTime").val('');
	$("#desc").val('');
}
</script>
</html>