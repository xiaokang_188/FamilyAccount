<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
		<title>主题帖</title>
		<script src="${basePath }/js/box.js"></script>
		<style>
			#topics tbody>tr:hover {
				background-color: #449d44;
			}
		</style>
	</head>
	<body>
		<%@include file="/WEB-INF/user/common/head.jsp" %>
		<div class="container-fluid">
			<div class="well-small" style="margin-top: 10px;">
				<ol class="breadcrumb">
				  <li><a href="javascript:;">首页</a></li>
				  <li><a href="${basePath}/RegionAction_list.action">用户交流区</a></li>
				</ol>
			</div>
			<div class="container">
		 		<div class="row">
					<div class="col-md-2" ></div>
					<div class="col-md-3">
						<ul class="list-unstyled  list-inline pull-right">
							<form class="form-inline">
								<input type="hidden" id="region_id" name="region_id" value="<s:property value="region_id"/>">
								<input type="text" id="keyword" name="keyword" value="<s:property value="keyword"/>" class="form-control" placeholder="关键词">
								<input id="searchBtn" type="submit" value="搜索" class="form-control btn btn-warning">
							</form>
						</ul>
					</div>
				</div>
				<li style="float:right;list-style-type:none;width: 80px;height:30px;line-height:30px; background-color: #0C72B1;cursor: pointer;text-align: center;">
					<a href="${basePath }/page_user_chat_write.action" style="color: white;" >发帖</a>	
				</li>
				<table id="topics"></table>
			</div>
			</div>
		</div><!--container主容器end-->
		<%@include file="/WEB-INF/user/common/foot.jsp" %>
	</body>
	<script>
$(()=>{
	let regionId = $("#region_id").val();
    let keyword = $("#keyword").val();
	let $url = "TopicAction_listCurrentUser.action?regionId="+ regionId + "&keyword=" +keyword;
    InitTable($url);
  	//查询数据
	$("#searchBtn").click(function () {
	    let regionId = $("#region_id").val();
	    let keyword = $("#keyword").val();
   		let $url = "TopicAction_listCurrentUser.action?regionId="+ regionId + "&keyword=" +keyword;
   	    InitTable($url);
	});
});
let InitTable = function (url){
	//先销毁表格
	$('#topics').bootstrapTable("destroy");
	//加载表格
	$("#topics").bootstrapTable({
		url : url, //请求地址
		method : 'post',
		dataType: "json",//期待返回数据类型
		locale: "zh-CN",//中文支持
		classes: "table table-bordered table-striped",
		height:400,
		showLoading : true,
		search: false,
		striped : true, //是否显示行间隔色
		sortable : false, //是否启用排序
		pageNumber : 1, //初始化加载第一页
		pagination : true,//是否分页
		sidePagination : 'client',//server:服务器端分页|client：前端分页
		pageSize : 5,//单页记录数
		pageList : [ 5,10],//可选择单页记录数
		showRefresh : false,//刷新按钮
		columns : [ {
			title : '帖子名称',
			field : 'topic_title',
			align : 'center',
			valign : 'middle',
		}, {
			title : '发帖者',
			field : 'user.user_name',
			align : 'center',
			valign : 'middle',
		}, {
			title : '帖子ID',
			field : 'topic_id',
			align : 'center',
			valign : 'middle',
			formatter: function (value, row, index) {
				let url="${basePath }/TopicAction_showContent.action?topic_id="+value;
				return ['<a href="'+url+'">'+value+'</a>'].join("");
		    }
		}, {
			title : '帖子发布时间',
			field : 'topic_datetime_s',
			align : 'center',
			valign : 'middle',
		}, {
			title : '浏览量',
			field : 'look_count',
			align : 'center',
			valign : 'middle',
		}]
	});
	return InitTable;
}
</script>
</html>