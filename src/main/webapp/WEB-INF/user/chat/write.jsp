<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/common/header.jsp" %>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
		<title>发帖</title>
		<link href="${basePath }/js/kindeditor/themes/qq/qq.css" rel="stylesheet">
		<script src="${basePath }/js/kindeditor/kindeditor-all-min.js"></script>
		<script src="${basePath }/js/kindeditor/lang/zh-CN.js"></script>
	</head>
	<body>
		<%@include file="/WEB-INF/user/common/head.jsp" %>
		<div class="container-fluid">
			<div class="well-small" style="margin-top: 10px;">
				<ol class="breadcrumb">
				  <li><a href="${basePath }/index.jsp">首页</a></li>
				  <li><a href="${basePath }/RegionAction_list.action">用户交流区</a></li>
				  <li>发帖</li>
				</ol>
			</div>
			<form action="${basePath }/TopicAction_add.action" id="topicForm" class="form-inline" method="post">
			<div align="center">
				<div class="row">
				<div class="col-md-10 col-md-offset-1" align="left"><span style="font-weight: bold;">标题：</span><input type="text" class="form-control form-inline" name="topic_title" required="required" style="width: 300px;"/></div>
				<div class="col-md-10 col-md-offset-1" align="left"><span style="font-weight: bold;">内容：</span>
					<textarea name="topic_content" id="topic_content" style="width:750px;height:300px;visibility:hidden;"></textarea>
				</div>
				<div class="col-md-10 col-md-offset-1" style="margin-top: 10px;" align="left"><span style="font-weight: bold;">选择大区：</span>
					<select class="form-control" style="width: 200px" name="region.region_id" id="region">
					</select>
					<script type="text/javascript">
						$(function(){
							$.post("RegionAction_listAjax.action",{},function(data){
								for(let i=0;i<data.length;i++){
									$("#region").append("<option value='"+data[i].region_id+"'>"+data[i].region_name+"</option>");
								}
							},"json");
						});
					</script>
				</div>
				<div class="col-md-10 col-md-offset-1" style="margin-top: 10px;" align="left">
				<input type="button" class="btn btn-success" value="确认发帖" onclick="topicPage.submitForm()"/>
				</div>
			</div>
			</div>
			</form>
			<script type="text/javascript">
			   let editor;
			   $(function(){
					KindEditor.ready(function(K) {
						editor = K.create('textarea[name="topic_content"]',{
							themeType : 'qq',
							resizeType : 1, //0不可更改大小；1可更改高度；2，可更改高度和宽度
							allowImageUpload : true,
							allowImageRemote : false, 
							items:['bold','undo', 'redo','preview', 'code','cut', 'copy', 'paste','plainpaste', 'wordpaste',
								'justifyleft', 'justifycenter', 'justifyright','justifyfull','emoticons','|','image', 'multiimage']						});
					});
			   });
			   let topicPage={
						submitForm:function(){
							editor.sync();
							let title=$("input[name='topic_title']").val();
							let content=$("#topic_content").val();
							if(title==null||title==''){
								Ewin.alert({message:"请输入主题帖标题！"});
								return;
							}
							if(content==null||content==''||content.replace(/&nbsp;/ig,"")==' '){
								Ewin.alert({message:"请输入主题帖的内容！"});
								return;
							}
							Ewin.confirm({message:"确定要发布主题帖吗？"}).on(function(e){
								if(e){
						           $("#topicForm").submit();
								}
							})
						}
				}
			</script>
		</div><!--container主容器end-->
		<%@include file="/WEB-INF/user/common/foot.jsp" %>
	</body>

</html>