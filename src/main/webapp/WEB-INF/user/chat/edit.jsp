<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
		<title>修改帖子</title>
		<link href="${basePath }/js/kindeditor/themes/qq/qq.css" rel="stylesheet">
		<script type="text/javascript" src="${basePath }/js/kindeditor/kindeditor-all-min.js"></script>
		<script type="text/javascript" src="${basePath }/js/kindeditor/lang/zh-CN.js"></script>
	</head>
	<body>
		<%@include file="/WEB-INF/user/common/head.jsp" %>
		<div class="container-fluid">
			<div class="well-small" style="margin-top: 10px;">
				<ol class="breadcrumb">
				  <li><a href="${basePath }/index.jsp">首页</a></li>
				  <li><a href="javascript:;">个人中心</a></li>
				  <li>我的帖子</li>
				  <li>修改帖子</li>
				</ol>
			</div>
			<form action="${basePath }/TopicAction_update.action" class="form-inline" method="post" id="editForm">
			<div align="center">
				<div class="row">
				<input type="hidden" name="topic_id" value='<s:property value="#topic.topic_id"/>'/>
				<div class="col-md-10 col-md-offset-1" align="left"><span style="font-weight: bold;">标题：</span><input type="text" class="form-control form-inline" name="topic_title" required="required" style="width: 300px;" value='<s:property value="#topic.topic_title"/>'/></div>
				<div class="col-md-10 col-md-offset-1" align="left"><span style="font-weight: bold;">内容：</span>
					<textarea name="topic_content" style="width:750px;height:300px;visibility:hidden;"><s:property value="#topic.topic_content"/></textarea>
				</div>
				<div class="col-md-10 col-md-offset-1" style="margin-top: 10px;" align="left"><span style="font-weight: bold;">选择大区：</span>
					<select class="form-control" style="width: 200px" name="region.region_id" id="region">
					</select>
					 <script type="text/javascript">
					 	$(function(){
							 $.post("RegionAction_listAjax.action",{},function(data){
									for(var i=0;i<data.length;i++){
										$("#region").append("<option value='"+data[i].region_id+"'>"+data[i].region_name+"</option>");
									}
								        $("#region").find("option[value='<s:property value="#topic.region.region_id"/>']").attr("selected",true);
								},"json")
					 	})
					 </script>
				</div>
				<div class="col-md-10 col-md-offset-1" style="margin-top: 10px;" align="left">
				    <input type="button" onclick="editParm.submitForm()" class="btn btn-success" value="确认修改" />
				     <input type="reset"  class="btn btn-success" value="重置表单" />
				</div>
			</div>
			</div>
			</form>
			<script type="text/javascript">
		        var editor;
				$(function(){
					KindEditor.ready(function(K) {
						editor = K.create('textarea[name="topic_content"]',{
							themeType : 'qq',
							resizeType : 1, //0不可更改大小；1可更改高度；2，可更改高度和宽度
							allowImageUpload : true,
							allowImageRemote : false, 
							items:['bold','undo', 'redo','preview', 'code','cut', 'copy', 'paste','plainpaste', 'wordpaste',
								'fontname', 'fontsize', 'forecolor', 'hilitecolor','justifyleft', 'justifycenter', 'justifyright','justifyfull','emoticons','|','image', 'multiimage']						});
					});
				});
				var editParm={
						submitForm:function(){
							editor.sync();
							var topic_title=$("input[name=topic_title]").val();
							var topic_content=$("textarea[name=topic_content]").val();
							if(topic_title==null||topic_title==''){
								Ewin.alert({message:"标题不能为空！"})
								return;
							}
							if(topic_content==null||topic_content==''){
								Ewin.alert({message:"请输入主题内容！"})
								return;
							}
							Ewin.confirm({message:"确定要修改此主题帖吗？"}).on(function(e){
								if(e){
									$("#editForm").submit();
								}
							})
						}
				}
			</script>
		</div><!--container主容器end-->
		<%@include file="/WEB-INF/user/common/foot.jsp" %>
	</body>
</html>