<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<title>我的帖子</title>
<link href="${basePath }/js/kindeditor/themes/qq/qq.css" rel="stylesheet">
<script src="${basePath }/js/kindeditor/kindeditor-all-min.js"></script>
<script type="text/javascript" src="${basePath }/js/kindeditor/lang/zh-CN.js"></script>
<style type="text/css">
#myTab li{
	border-radius: 1px
}
#myTab li:hover{
	cursor: pointer;
}
#myTab li a{
	font-weight: bolder;
}
#mytopic tbody>tr:hover {
	background-color: #449d44;
}
</style>
</head>
<body>
	<%@include file="/WEB-INF/user/common/head.jsp"%>
	<div class="container-fluid">
		<div class="well-small" style="margin-top: 10px;">
			<ol class="breadcrumb">
				<li><a href="${basePath }/index.jsp">首页</a></li>
				<li><a href="javascript:;">个人中心</a></li>
				<li>我的帖子</li>
			</ol>
		</div>
		<ul id="myTab" class="nav nav-tabs">
			<li class="active"><a href="#home" data-toggle="tab"> 我发表的帖子</a></li>
			<li><a href="#mymessage" data-toggle="tab">消息中心<span class="badge"><s:property value="#xiaoxi.size"/></span></a></li>
			<li><a href="#myreply" data-toggle="tab">我的回复</a></li>
			<li><a href="${basePath }/page_user_chat_write.action">发表帖子</a></li>
		</ul>
		<div id="myTabContent" class="tab-content">
				<!-- 我发表的帖子 -->
				<div class="tab-pane fade in active" id="home">
					<table id="mytopic"></table>
				</div>
			
			<!-- 我收到的回复 -->
			<div class="tab-pane fade" id="mymessage">
				<ul class="list-unstyled" style="margin: 30px;">
					<s:iterator value="#xiaoxi" var="reply">
					<li style="font-size: 16px;">
						【<s:property value="#reply.user.user_name"/>】在<s:property value="#reply.reply_datetime_s"/>回复了您的主题帖：<a target="_blank" href="TopicAction_showContent.action?topic_id=<s:property value="#reply.topic.topic_id"/>">“<s:property value="#reply.topic.topic_title"/>”</a>|回复内容为：<a>“<s:property value="#reply.reply_content" escape="false"/>”</a>
						<s:if test="#reply.status==0">
						  <a href="javascript:;" onclick="read(<s:property value="#reply.reply_id"/>)"><span class="btn btn-xs btn-danger">确认查看</span></a>
						</s:if>
						<s:else>
						  <span class="btn btn-xs btn-success">已查看</span>
						</s:else>
					</li>
					</s:iterator>
				</ul>
			</div>
			<!-- 我回复的 -->
			<div class="tab-pane fade" id="myreply">
				<ul class="list-unstyled" style="margin: 30px;">
					<s:iterator var="reply2" value="#replys">
					<li style="font-size: 16px;">
						回复了<a target="_blank" href="TopicAction_showContent.action?topic_id=<s:property value="#reply2.topic.topic_id"/>">“主题帖：<s:property value="#reply2.topic.topic_title"/>”</a>|回复内容为：<a>“<s:property value="#reply2.reply_content" escape="false"/>”</a><a class="btn btn-xs btn-danger" href="javascript:;" onclick="deleteById(<s:property value="#reply2.reply_id"/>)">删除</a>
					</li>
					</s:iterator>
				</ul>
			</div>
		</div>
	</div>
	<!--container主容器end-->
	<%@include file="/WEB-INF/user/common/foot.jsp"%>
</body>
<script>
$(()=>{
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	      var activeTab = $(e.target).text(); 
	      var previousTab = $(e.relatedTarget).text(); 
	      $(".active-tab span").html(activeTab);
	      $(".previous-tab span").html(previousTab);
	});
    $url = "TopicAction_findByUser.action";
    InitTable($url);
});
let InitTable = function (url){
	//先销毁表格
	$('#mytopic').bootstrapTable("destroy");
	//加载表格
	$("#mytopic").bootstrapTable({
		url : url, //请求地址
		method : 'post',
		dataType: "json",//期待返回数据类型
		locale: "zh-CN",//中文支持
		classes: "table table-bordered table-striped",
		height:400,
		showLoading : true,
		search: false,
		striped : true, //是否显示行间隔色
		sortable : false, //是否启用排序
		pageNumber : 1, //初始化加载第一页
		pagination : true,//是否分页
		sidePagination : 'client',//server:服务器端分页|client：前端分页
		pageSize : 5,//单页记录数
		pageList : [ 5,10],//可选择单页记录数
		showRefresh : false,//刷新按钮
		columns : [ {
		    field: 'is_end',
		    title: '是否结帖',
		    align : 'center',
			valign : 'middle',
		    formatter: function (value, row, index) {
		    	if(value === '1'){
		        	return '<span class="label label-warning">【已结帖】</span>';
		    	}else {
		        	return '<span class="label label-default">【未结帖】</span>';
		    	}
		    }
		}, {
		    field: 'is_good',
		    title: '是否加精',
		    align : 'center',
			valign : 'middle',
		    formatter: function (value, row, index) {
		    	if(value === '1'){
		        	return '<span class="label label-danger">【加精】</span>';
		    	}else {
		        	return '<span class="label label-default">【未加精】</span>';
		    	}
		    }
		}, {
		    field: 'is_top',
		    title: '是否置顶',
		    align : 'center',
			valign : 'middle',
		    formatter: function (value, row, index) {
		    	if(value === '1'){
		        	return '<span class="label label-success">【置顶】</span> ';
		    	}else {
		        	return '<span class="label label-default">【未置顶】</span> ';
		    	}
		    }
		}, {
			title : '帖子名称',
			field : 'topic_title',
			align : 'center',
			valign : 'middle',
		}, {
			title : '帖子ID',
			field : 'topic_id',
			align : 'center',
			valign : 'middle',
			formatter: function (value, row, index) {
				let url="${basePath }/TopicAction_showContent.action?topic_id="+value;
				return ['<a href="'+url+'">'+value+'</a>'].join("");
		    }
		}, {
			title : '帖子发布时间',
			field : 'topic_datetime_s',
			align : 'center',
			valign : 'middle',
		}, {
			title : '浏览量',
			field : 'look_count',
			align : 'center',
			valign : 'middle',
		}, {
			title : '操作',
			field : 'topic_id',
			width : 120,
			align : 'center',
			valign : 'middle',
			formatter : actionFormatter
		}]
	});
	return InitTable;
}
//操作栏的格式化
function actionFormatter(value, row, index) {
	let topic_id = value;
	let result = "";
	result += "<a href='javascript:;' class='btn btn-xs btn-warning' onclick=\"updateTopic('"
			+ topic_id
			+ "')\" title='编辑'>编辑</a>";
	result += "<a href='javascript:;' class='btn btn-xs btn-danger' onclick=\"deleteTopic('"
			+ topic_id
			+ "')\" title='删除'>删除</a>";
	result += "<a href='javascript:;' class='btn btn-xs btn-primary' onclick=\"endTopic('"
		+ topic_id
		+ "')\" title='结帖'>结帖</a>";
	return result;
}
function updateTopic(id){
	  window.location.href="${basePath}/TopicAction_findById.action?topic_id="+id;
 }
 function deleteTopic(id){
	   Ewin.confirm({message:"确定要删除这条帖子吗？"}).on(function(e){
		   if(e){
			   $.post("TopicAction_deleteById.action",{"topic_id":id},function(data){
				   if(data=="0"){
					   Ewin.alert({message:"删除失败！"});
				   }else{
					   window.location.reload();
				   }
				 })
		   }
	   })
 }
 function read(id){
	   $.post("ReplyAction_read.action",{"reply_id":id},function(data){
		   if(data=="1"){
			   window.location.reload();
		   }else{
			   Ewin.alert({message:"查看失败！"});
		   }
	   })
 }
 function deleteById(id){
	   Ewin.confirm({message:"确定要删除这条回复吗？"}).on(function(e){
		   if(e){
			   $.post("ReplyAction_deleteById.action",{"reply_id":id},function(data){
				   if(data=="1"){
					   window.location.reload();
				   }else{
					   Ewin.alert({message:"删除失败！"});
				   }
			   });
		   }
	   })
 }
 function endTopic(id){
	   Ewin.confirm({message:"确定要结贴吗？"}).on(function(e){
		   if(e){
			   $.post("TopicAction_end.action",{"topic_id":id},function(data){
				   if(data=="1"){
					   window.location.reload();
				   }else{
					   Ewin.alert({message:"结贴失败！"});
				   }
			   });
		   }
	   })
 }
</script>
</html>