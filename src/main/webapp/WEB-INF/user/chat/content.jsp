<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<title><s:property value="#topic.topic_title"/></title>
<link href="${basePath }/css/kindeditor-img.css" rel="stylesheet">
<link href="${basePath }/js/kindeditor/themes/qq/qq.css" rel="stylesheet">
<script type="text/javascript" src="${basePath }/js/kindeditor/kindeditor-all-min.js"></script>
<script type="text/javascript" src="${basePath }/js/kindeditor/lang/zh-CN.js"></script>
<script src="${basePath }/js/script.js"></script>
<style type="text/css">
#bak_top {
    width: 36px;
    height: 114px;
    background: url(${basePath}/img/gotop.png);
    bottom: 30%;
    -webkit-transition: all .5s ease-in-out;
    -moz-transition: all .5s ease-in-out;
    -o-transition: all .5s ease-in-out;
    -ms-transition: all .5s ease-in-out;
    transition: all .5s ease-in-out;
    right: 50%;
    margin-right: -560px;
    position: fixed;
}
</style>
</head>
<body>
	<%@include file="/WEB-INF/user/common/head.jsp" %>
	<div class="container-fluid">
		<div class="well-small" style="margin-top: 10px;">
			<ol class="breadcrumb">
				<li><a href="${basePath }/index.jsp">首页</a></li>
				<li><a href="${basePath }/RegionAction_list.action">用户交流区</a></li>
				<li><a href="${basePath }/TopicAction_list.action?region_id=<s:property value="#topic.region.region_id"/>"><s:property value="#topic.region.region_name"/></a></li>
				<li>主题展示</li>
			</ol>
		</div>
		<div class="container">
			<div class="panel panel-primary">
				<div class="panel-heading"
					style="font-size: 16px; font-weight: bolder;">
					        <s:if test="#topic.is_end=='1'">
			    		       <span class="label label-warning">【已结帖】</span>
			    			</s:if>
			    			<s:else>
			    				<span class="label label-default">【未结帖】</span>
			    			</s:else>
			    			<s:if test="#topic.is_good=='1'">
					          <span class="label label-danger">【加精】</span> 
			    			</s:if>
			    			<s:if test="#topic.is_top=='1'">
					           <span class="label label-success">【置顶】</span> 
			    			</s:if><s:property value="#topic.topic_title"/><a
						class="inline btn btn-default" href="#bottom"
						style="margin-left:4px;">快速回复</a>
				</div>
			</div>
			<div class="media">
				<div class="media-left media-top" style="width: 20%;color: gray;" align="left">
					<ul class="list-unstyled ">
						<li><a href="#"> <img class="media-object"
								src="${basePath }/<s:property value="#topic.user.face"/>" alt="...">
						</a></li>
						<li>用户名：<s:property value="#topic.user.user_name"/></li>
						<li>性别：<s:property value="#topic.user.user_sex"/></li>
						<li>用户等级：
								<s:if test="#topic.user.level==0">
									<span class="label label-success">普通成员</span>
								</s:if>
								<s:if test="#topic.user.level==1">
									<span class="label label-warning">家庭户主</span>
								</s:if>
						</li>
						<li>
							<address>来自：<s:property value="#topic.user.address"/></address>
						</li>
						<li style="font-weight: bolder;font-size: 15px;color: black">#楼主</li>
					</ul>
				</div>
				<div class="media-body"
					style="width: 80%; border-left: 1px solid #F0F0F0; padding-left: 10px;">
					<s:property value="#topic.topic_content" escape="false"/>
				</div>
				<div style="color: gray;" align="right">
					发表于：【<s:property value="#topic.topic_datetime_s"/>】浏览量：【<s:property value="#topic.look_count"/>】回复次数：【<s:property value="#topic.replys.size"/>】 <span class="badge"
						style="background-color: green;cursor: pointer;" onclick="zan(<s:property value="#topic.topic_id"/>)"><span
						class="glyphicon glyphicon-thumbs-up"></span><s:property value="#topic.topic_zan==null?0:#topic.topic_zan"/></span> <span
						class="badge" style="background-color: red;cursor: pointer;"><span
						class="glyphicon glyphicon-thumbs-down"></span><s:property value="#topic.topic_bad==null?0:#topic.topic_bad"/></span> <a
						class="btn-sm btn-danger" href="javascript:;" onclick="bad(<s:property value="#topic.topic_id"/>)">举报</a> <span style="font-weight: bolder;font-size: 15px;color: black">#楼顶</span>
				</div>

			</div>
			<hr>
			<s:iterator value="#replylist" var="reply" status="index">
				<div class="media">
				<div class="media-left media-middle" style="width: 20%;color: gray;" align="left">
					<ul class="list-unstyled ">
						<li><a href="#"> <img class="media-object"
								src="${basePath }/<s:property value="#reply.user.face"/>" alt="...">
						</a></li>
						<li>用户名：<s:property value="#reply.user.user_name"/></li>
						<li>性别：<s:property value="#reply.user.user_sex"/></li>
						<li>用户等级：
							<s:if test="#reply.user.level==0">
									<span class="label label-success">普通成员</span>
								</s:if>
								<s:if test="#reply.user.level==1">
									<span class="label label-warning">家庭户主</span>
								</s:if>
						</li>
						<li>
							<address>来自：<s:property value="#reply.user.address"/></address>
						</li>
						<li>#<s:property value="#index.count"/>楼</li>
					</ul>
				</div>
				<div class="media-body"
					style="width: 80%; border-left: 1px solid #F0F0F0; padding-left: 10px;">
					<s:property value="#reply.reply_content" escape="false"/></div>
				<div style="color: gray;" align="right">
					回复于：【<s:property value="#reply.reply_datetime_s"/>】
					 <span class="badge" style="background-color: green;cursor: pointer;" onclick="replyzan(<s:property value="#reply.reply_id"/>)">
					 	<span class="glyphicon glyphicon-thumbs-up"></span>
					 	<s:property value="#reply.zan==null?0:#reply.zan"/>
					 </span>
					 <span class="badge" style="background-color: red;cursor: pointer;">
					 <span class="glyphicon glyphicon-thumbs-down"></span>
					 	<s:property value="#reply.bad==null?0:#reply.bad"/>
					 </span> 
					 <a class="btn-sm btn-danger" onclick="replybad(<s:property value="#reply.reply_id"/>)">举报</a>
					 <span>#<s:property value="#index.count"/>楼</span>
				</div>
			</div>
			<hr>
			</s:iterator>
			<div name="bottom" id="bottom">
				<s:if test="#topic.is_end=='0'">
					<form action="ReplyAction_add.action" method="post" id="replyForm">
						<input type="hidden" name="topic.topic_id" value='<s:property value="#topic.topic_id"/>'/>
					<table>
						<tr>
							<td style="padding-right: 10px;"><h5>用户回复</h5></td>
							<td>
								<textarea name="reply_content"
									style="width: 800px; height: 200px; visibility: hidden;" id="reply_content"></textarea>
							</td>
						</tr>
						<tr>
							<td colspan="2" align="center" style="padding-top: 10px;"><input
								type="button" value="确认回复" class="btn btn-success"
								style="border-radius: 0px;" id="btn_reply" onclick="replyPage.submitForm()"/>
								<hr></td>
						</tr>
					</table>
					</form>
				</s:if>
				<s:else>
					<h4 style="color: red;">该帖已结贴</h4>
				</s:else>
				<script type="text/javascript">
				    let editor;
					$(function(){
						KindEditor.ready(function(K) {
							editor = K.create('textarea[name="reply_content"]',{
								themeType : 'qq',
								resizeType : 1, //0不可更改大小；1可更改高度；2，可更改高度和宽度
								allowImageUpload : true,
								allowImageRemote : false, 
								items:['bold','undo', 'redo','preview', 'code','cut', 'copy', 'paste','plainpaste', 'wordpaste',
									'justifyleft', 'justifycenter', 'justifyright','justifyfull','emoticons','|','image', 'multiimage']							});
						});
					})
					let replyPage={
							submitForm:function(){
								editor.sync();
								let content=$("#reply_content").val();
								if(content==null||content==''||content.replace(/&nbsp;/ig,"")==' '){
									Ewin.alert({message:"请输入回复的内容！"});
									return;
								}
								Ewin.confirm({message:"确定要回复吗？"}).on(function(e){
									if(e){
							           $("#replyForm").submit();
									}
								})
							}
					}
					function zan(id){
						$.post("TopicAction_zan.action",{"topic_id":id},function(data){
							if(data=="1"){
								window.location.reload();
							}else{
								alert("出现异常！")
							}
						})
					}
					function replyzan(id){
						$.post("ReplyAction_zan.action",{"reply_id":id},function(data){
							if(data=='1'){
								window.location.reload();
							}else{
								alert("出现异常");
							}
						})
					}
					function bad(id){
						Ewin.confirm({message:"你确定要举报吗？"}).on(function(e){
							
							if(e){
								$.post("TopicAction_bad.action",{"topic_id":id},function(data){
									if(data=="1"){
										window.location.reload();
									}else{
										alert("出现异常！")
									}
								})
							}
						})
						
					}
					function replybad(id){
                         Ewin.confirm({message:"你确定要举报吗？"}).on(function(e){
							
							if(e){
								$.post("ReplyAction_bad.action",{"reply_id":id},function(data){
									if(data=='1'){
										window.location.reload();
									}else{
										alert("出现异常");
									}
								})
							}
						})
						
					}
				</script>
			</div>
		</div>
		<a id="bak_top"  title="回到顶部" style="display: none;"></a>
	</div>
	<!--container主容器end-->
	<%@include file="/WEB-INF/user/common/foot.jsp" %>
</body>
</html>