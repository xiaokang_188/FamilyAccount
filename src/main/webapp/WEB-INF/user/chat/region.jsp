<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
		<title>用户交流区</title>
		<script type="text/javascript">
			function img_region(id){
				window.location.href="${basePath}/TopicAction_list.action?region_id="+id;
			}
		</script>
		<style type="text/css">
			#img_region:hover{
			  cursor: pointer;
			  background-color: #DEECFA;
			}
		</style>
	</head>
	<body>
		<%@include file="/WEB-INF/user/common/head.jsp" %>
		<div class="container-fluid">
			<div class="well-small" style="margin-top: 10px;">
				<ol class="breadcrumb">
				  <li><a href="${basePath }/index.jsp">首页</a></li>
				  <li><a href="${basePath }/RegionAction_list.action">用户交流区</a></li>
				  <li>选择大区</li>
				</ol>
			</div>
			<div class="row" >
				<s:iterator var="region" value="#list">
					<div class="col-md-3">
						<div class="thumbnail" id="img_region">
					      <div class="caption">
					        <h3><s:property value="#region.region_name"/></h3>
					        <p><s:property value="#region.region_desc"/></p>
					        <p><a href="javascript:;" class="btn btn-success" role="button" onclick="img_region(<s:property value="#region.region_id"/>)">进入此交流区</a></p>
					      </div>
					     </div>
				   </div>
				</s:iterator>
			</div>
		</div><!--container主容器end-->
		<%@include file="/WEB-INF/user/common/foot.jsp" %>
	</body>
</html>