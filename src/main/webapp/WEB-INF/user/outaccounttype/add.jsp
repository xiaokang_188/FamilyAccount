<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
		<title>添加支出类型</title>
		<style>
			label.error{
				font-size:14px;
				color: red;
			}
		</style>
	</head>
	<body>
		<%@include file="/WEB-INF/user/common/head.jsp" %>
		<div class="container-fluid">
			<ol class="breadcrumb">
				<li>
					<a href="${pageContext.request.contextPath }/index.jsp">首页</a>
				</li>
				<li>
					<a href="${pageContext.request.contextPath }/OutAccountTypeAction_list.action">支出类型列表</a>
				</li>
				<li>
					<span class="glyphicon glyphicon-plus"></span>添加支出类型
				</li>
			</ol>
			
			<div class="container">
				<form id="add_form" action="${pageContext.request.contextPath }/OutAccountTypeAction_add.action" method="post">
				<div class="row">
					<div class="col-md-offset-3 col-md-5" style="border: 1px solid antiquewhite;background-color: ghostwhite;border-radius: 6px;padding: 10px;" align="center">
						<div class="form-group form-inline">
							<label class="control-label">支出类型名称:</label>
							<input style="width: 220px;" required="required" type="text" class="form-control" name="outaccounttype_name" id="outaccounttype_name" style="width: 220px;" placeholder="支出类型名称"/>
						</div>
						<div class="form-group form-inline" style="padding-left: 50px;">
							<label class="control-label">描述:</label>
							<textarea style="width: 220px;" class="form-control" name="outaccounttype_desc"  placeholder="备注，方便记忆"></textarea>
						</div>
						<input type="reset" value="重置" class="btn btn-warning"/>
						<input type="submit" value="提交添加" class="btn btn-danger"/>
					</div>
				</div>
				</form>
			</div>
		</div>
		<%@include file="/WEB-INF/user/common/foot.jsp"%>
	</body>
</html>
<script>
$(function(){
	$("#add_form").validate({
		rules:{
			"outaccounttype_name":{
				"required":true,
				"remote":{
					url:"${basePath}/OutAccountTypeAction_checkName.action",
					type:"post",
					dataType:"json",
					data:{
						outtypeName: function () {
	                         return $("#outaccounttype_name").val();//这个是取要验证的数据
	                     }
					},
					dataFilter: function(data, type) {
							 let jsonData=JSON.parse(data);
							 let result=jsonData.isExist;
	                         return !result;
					}
				}
			}
		},
		messages:{
			"outaccounttype_name":{
				"required":"支出类型名称不能为空",
				"remote":"支出类型名称已经存在"
			}
		}
	});
});
</script>