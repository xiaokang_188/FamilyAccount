<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
		<title>修改个人资料</title>
		<%@include file="/WEB-INF/user/common/head.jsp" %>
		<style type="text/css">
				.infocolor{
					color: darkgray;
				}
				label.error{
					font-size:14px;
					color: red;
				}
		</style>
		<script>
		//自定义校验规则
		$.validator.addMethod(
			"regexp_idcard",
			function(value,element){
				let flag = false;
				//身份证号正则, 支持1/2代(15位/18位数字)
				let idcard = /(^\d{8}(0\d|10|11|12)([0-2]\d|30|31)\d{3}$)|(^\d{6}(18|19|20)\d{2}(0\d|10|11|12)([0-2]\d|30|31)\d{3}(\d|X|x)$)/;   
				if(idcard.test(value)){
					flag=true;
				}
				return flag;
			}
		);
		//自定义校验规则
		$.validator.addMethod(
			"regexp_realname",
			function(value,element){
				let flag = false;
				//中文姓名正则
				let name_CN = /^(?:[\u4e00-\u9fa5·]{2,16})$/;  
				//英文姓名正则
				let name_US = /(^[a-zA-Z]{1}[a-zA-Z\s]{0,20}[a-zA-Z]{1}$)/;
				if(name_CN.test(value) || name_US.test(value)){
					flag=true;
				}
				return flag;
			}
		);
		//自定义校验规则
		$.validator.addMethod(
			"regexp_phone",
			function(value,element){
				let flag = false;
				let length = value.length;  
				//中国手机号正则
				let mobile = /^(?:(?:\+|00)86)?1\d{10}$/;   
				if(length == 11 && mobile.test(value)){
					flag=true;
				}
				return flag;
			}
		);
$(function(){
	$("#editinfo").validate({
		rules:{
			"idcard":{
				"required":true,
				"regexp_idcard":true
			},
			"address":{
				"required":true
			},
			"realname":{
				"required":true,
				"regexp_realname":true
			},
			"user_phone":{
				"required":true,
				"regexp_phone":true
			}
		},
		messages:{
			"idcard":{
				"required":"身份证号不能为空",
				"regexp_idcard":"身份证号不合法"
			},
			"address":{
				"required":"家庭住址不能为空"
			},
			"realname":{
				"required":"真实姓名不能为空",
				"regexp_realname":"姓名不合法"
			},
			"user_phone":{
				"required":"手机号不能为空",
				"regexp_phone":"请输入合法手机号"
			}
		}
	});
});
</script>
	</head>
	<body>
		<!--路径墙-->
		<div class="well-sm" style="padding-bottom: 0px;">
			<ol class="breadcrumb" style="font-size: 15px;">
				<li>
					<a href="javascript:;">个人中心</a>
				</li>
				<li>
					<a href="${basePath }/page_user_common_userdesc.action">个人资料</a>
				</li>
				<li>
					编辑资料
				</li>
			</ol>
		</div>
		<!--路径墙end-->
		<form id="editinfo" action="${basePath }/UserAction_edit.action"  method="post">
		<div class="container">
			 <input type="submit" style="width: 100px;background-color: gray;color: white;" class="form-control pull-right" value="保存修改"  />
		</div>
		<!--用户资料容器-->
		<div class="container">
			<!--main row-->
			<div class="row" align="center">
				<!--row div 用户详细信息-->
				<div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6">
					<!--存用户信息row-->
					<div class="row" style="font-size: 16px;">
						<div class="col-md-6 col-sm-6 col-xs-12" align="center">
							<span class="glyphicon glyphicon-picture" style="color: green;"></span>头&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;像：
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 infocolor">
							<select style="width:200px" class="tech" name="face" id="tech">
						      <option value="img/userface/user0.gif" data-image="${basePath }/img/userface/user0.gif">头像一</option>
						      <option value="img/userface/user1.gif" data-image="${basePath }/img/userface/user1.gif">头像二</option>
						      <option value="img/userface/user2.gif" data-image="${basePath }/img/userface/user2.gif" name="cd">头像三</option>
						      <option value="img/userface/user3.gif"  data-image="${basePath }/img/userface/user3.gif">头像四</option>
						      <option value="img/userface/user4.gif" data-image="${basePath }/img/userface/user4.gif">头像五</option>
						      <option value="img/userface/user5.gif" data-image="${basePath }/img/userface/user5.gif">头像六</option>
						      <option value="img/userface/user6.gif" data-image="${basePath }/img/userface/user6.gif">头像七</option>
						      <option value="img/userface/user7.gif" data-image="${basePath }/img/userface/user7.gif">头像八</option>
						      <option value="img/userface/user8.gif" data-image="${basePath }/img/userface/user8.gif">头像九</option>
						      <option value="img/userface/user9.gif" data-image="${basePath }/img/userface/user9.gif">头像十</option>
						      <option value="img/userface/user10.gif" data-image="${basePath }/img/userface/user10.gif">头像十一</option>
						      <option value="img/userface/user11.gif" data-image="${basePath }/img/userface/user11.gif" name="cd">头像十二</option>
						      <option value="img/userface/user12.gif" data-image="${basePath }/img/userface/user12.gif" name="cd">头像十三</option>
						      <option value="img/userface/user13.gif" data-image="${basePath }/img/userface/user13.gif" name="cd">头像十四</option>
						      <option value="img/userface/user14.gif" data-image="${basePath }/img/userface/user14.gif" name="cd">头像十五</option>
						      <option value="img/userface/user15.gif" data-image="${basePath }/img/userface/user15.gif" name="cd">头像十六</option>
						      <option value="img/userface/user16.gif" data-image="${basePath }/img/userface/user16.gif" name="cd">头像十七</option>
						      <option value="img/userface/user17.gif" data-image="${basePath }/img/userface/user17.gif" name="cd">头像十八</option>
						      <option value="img/userface/user18.gif" data-image="${basePath }/img/userface/user18.gif" name="cd">头像十九</option>
						      <option value="img/userface/user19.gif" data-image="${basePath }/img/userface/user19.gif" name="cd">头像二十</option>
						    </select>
						</div>
						
						<script type="text/javascript">
							$("#tech").msDropdown();
						</script>
						
						<div class="col-md-12 col-sm-12" style="height: 8px;"></div>
						
						<div class="col-md-6 col-sm-6 col-xs-12" align="center">
							<span class="glyphicon glyphicon-user" style="color: green;">	
								</span>真实姓名：
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 infocolor">
							<input style="width: 240px;" class="form-control" type="text" name="realname" id="userName" value="<s:property value="#session.currentUser.realname"/>" />
						</div>
						
						<div class="col-md-12 col-sm-12" style="height: 8px;"></div>

						<div class="col-md-6 col-sm-6 col-xs-12" align="center">
							<img src="${basePath }/img/sex.png" />性&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;别：
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 infocolor">
							<script type="text/javascript">
								$(function(){
									$("#sex option[value='<s:property value="#session.currentUser.user_sex"/>']").prop("selected",'selected');
									$("#tech option[value='<s:property value="#session.currentUser.face"/>']").prop("selected",'selected');
								})
							</script>
							<select class="form-control" style="width: 240px;" name="user_sex" id="sex">
							  <option value='男'>男</option>
							  <option value='女'>女</option>
							  <option value=''>保密</option>
							</select>
						</div>
						
						<div class="col-md-12 col-sm-12" style="height: 8px;"></div>
			
						<div class="col-md-6 col-sm-6 col-xs-12" align="center">
							<span class="glyphicon glyphicon-credit-card"></span>身份证号：
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 infocolor">
							<input style="width: 240px;" type="text" name="idcard" id="idcard" class="form-control" value="<s:property value="#session.currentUser.idcard"/>"/>
						</div>
						
						<div class="col-md-12 col-sm-12" style="height: 8px;"></div>

						<div class="col-md-6 col-sm-6 col-xs-12" align="center">
							<span class="glyphicon glyphicon-phone"></span>手机号码：
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 infocolor">
						    <input style="width: 240px;" type="text" name="user_phone" id="user_phone" class="form-control" value="<s:property value="#session.currentUser.user_phone"/>"/>
						</div>
						
						<div class="col-md-12 col-sm-12" style="height: 8px;"></div>

						<div class="col-md-6 col-sm-6 col-xs-12" align="center">
							<span class="glyphicon glyphicon-envelope"></span>邮箱地址：
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 infocolor">
							<input style="width: 240px;" type="text" name="email" id="email" readonly="readonly" class="form-control" value="<s:property value="#session.currentUser.email"/>"/>
						</div>
						
						<div class="col-md-12 col-sm-12" style="height: 8px;"></div>
						
						<div class="col-md-6 col-sm-6 col-xs-12" align="center">
							<span class="glyphicon glyphicon-home" style="color: red;"></span>家庭地址：
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 infocolor form-inline">
							<input style="width: 360px;" type="text" name="address" id="details" value="<s:property value="#session.currentUser.address"/>" class="form-control"/>
						</div>
						
						<div class="col-md-12 col-sm-12" style="height: 8px;"></div>
						<!-- 百度地图 -->
						<div id="searchResultPanel" style="border:1px solid #C0C0C0;width:150px;height:auto; display:none;"></div>
						<div class="col-md-offset-3 col-md-12 col-sm-offset-3 col-sm-6" id="container" style="height:400px"></div>
						
						<div class="col-md-12 col-sm-12" style="height: 8px;"></div>
						
						<div class="col-md-6 col-sm-6 col-xs-12" align="center">
							<img src="${basePath }/img/birth.png"/>出生日期：
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 infocolor">
							<input type="text" data-date-format="yyyy-mm-dd" readonly="readonly" name="birthday" id="birthday" class="form-control date" style="width: 240px;" value="<s:property value="#session.currentUser.birthday_s"/>"/>
						</div>
					</div>
					<!--存用户信息row end-->
				</div>
				<!--row div 用户详细信息 end-->

			</div>
			<!--main row end-->
		</div>
		<!--用户资料主容器-->
		</form>
		
		<%@include file="/WEB-INF/user/common/foot.jsp" %>
		<script type="text/javascript">
		 	 $('#birthday').datetimepicker({  
		 	 	pickerPosition: 'top-right',
		 	 	format: 'yyyy-mm-dd ',
	            language:  'zh-CN',  
	            weekStart: 1,  
	            todayBtn:  1,  
	            autoclose: 1,  
	            todayHighlight: 1,  
	            startView: 2,  
	            minView: 2,  
	            forceParse: 0  
           });  
		</script>
	</body>
</html>
<script>
	$(function() {
		loadJScript();
	});
	// 百度地图API功能
	function G(id) {
		return document.getElementById(id);
	} //百度地图API功能
	function loadJScript() {
		var script = document.createElement("script");
		script.type = "text/javascript";
		script.src = "//api.map.baidu.com/api?v=3.0&ak=PHswLmkQKFiysz3uWr8VoW4hdqLfG1OM&callback=init";
		document.body.appendChild(script);
	}
	//地图初始化
	function init() {
		// 创建Map实例
		var map = new BMap.Map("container");
		// 创建点坐标
		var point = new BMap.Point(114.98, 38.75);
		map.centerAndZoom(point, 15);
		//建立一个自动完成的对象
		var ac = new BMap.Autocomplete({
			"input": "details",
			"location": map
		});
		//鼠标放在下拉列表上的事件
		ac.addEventListener("onhighlight", function(e) {
			var str = "";
			var _value = e.fromitem.value;
			var value = "";
			if (e.fromitem.index > -1) {
				value = _value.province + _value.city + _value.district + _value.street + _value.business;
			}
			str = "FromItem<br />index = " + e.fromitem.index + "<br />value = " + value;
			value = "";
			if (e.toitem.index > -1) {
				_value = e.toitem.value;
				value = _value.province + _value.city + _value.district + _value.street + _value.business;
			}
			str += "<br />ToItem<br />index = " + e.toitem.index + "<br />value = " + value;
			G("searchResultPanel").innerHTML = str;
		});
		var myValue;
		//鼠标点击下拉列表后的事件
		ac.addEventListener("onconfirm", function(e) {
			var _value = e.item.value;
			myValue = _value.province + _value.city + _value.district + _value.street + _value.business;
			G("searchResultPanel").innerHTML = "onconfirm<br />index = " + e.item.index + "<br />myValue = " + myValue;
			setPlace();
		});

		function setPlace() {
			//清除地图上所有覆盖物
			map.clearOverlays();

			function myFun() {
				//获取第一个智能搜索的结果
				var point = local.getResults().getPoi(0).point;
				// 获取搜索结果的详细地址
				console.info(local.getResults().getPoi(0).address);
				let address = local.getResults().getPoi(0).address;
				// 将获取到的地址填充到表单
				window.document.getElementById("details").value = address
				map.centerAndZoom(point, 15);
				var marker = new BMap.Marker(point);
				//添加标注
				map.addOverlay(marker);
				marker.setAnimation(BMAP_ANIMATION_BOUNCE);
			}
			//智能搜索
			var local = new BMap.LocalSearch(map, {
				onSearchComplete: myFun
			});
			local.search(myValue);
		}
		//添加地图类型控件
		map.addControl(new BMap.MapTypeControl({
			mapTypes: [
				BMAP_NORMAL_MAP,
				BMAP_HYBRID_MAP
			]
		}));
		map.setCurrentCity("唐县"); // 设置地图显示的城市 此项是必须设置的
		//启用滚轮放大缩小
		map.enableScrollWheelZoom();
		// 设置跳动点
		var marker = new BMap.Marker(point);
		map.addOverlay(marker);
		marker.setAnimation(BMAP_ANIMATION_BOUNCE);
		//连续缩放
		map.enableContinuousZoom();
		//添加城市列表控件		
		var size = new BMap.Size(10, 20);
		map.addControl(new BMap.CityListControl({
			anchor: BMAP_ANCHOR_TOP_LEFT,
			offset: size,
			//切换城市之间事件
			onChangeBefore: function() {
				//alert('准备切换，确定即切换！');
			},
			//切换城市之后事件
			onChangeAfter: function() {
				//alert('切换成功！');
			}
		}));
	}

	function theLocation() {
		var city = document.getElementById("cityName").value;
		if (city != "") {
			// 创建Map实例
			var map = new BMap.Map("container");
			// 用城市名设置地图中心点
			map.centerAndZoom(city, 15);
		}
	}
</script>