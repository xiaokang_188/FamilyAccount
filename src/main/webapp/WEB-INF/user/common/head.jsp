<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>页面头部</title>
<script src="${basePath}/js/bootstrap.js"></script>
<link href="${basePath}/js/bootstrap-table-master/dist/bootstrap-table.css" rel="stylesheet" />
<script src="${basePath}/js/bootstrap-table-master/dist/bootstrap-table.js"></script>
<script src="${basePath}/js/bootstrap-table-master/dist/locale/bootstrap-table-zh-CN.js"></script>
<script src="${basePath}/js/jquery.validate.min.js"></script>
<script src="${basePath}/js/jquery.ocupload-1.1.2.js"></script>
<script src="${basePath}/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="${basePath}/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script type="text/javascript" src="${basePath}/js/box.js"></script>
<script src="${basePath}/js/track.js"></script>
<link href="${basePath}/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/js/spinner/bootstrap-spinner.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/js/spinner/jquery.spinner.min.js"></script>
<link rel="stylesheet" type="text/css" href="${basePath}/js/msdropdown/css/msdropdown/dd.css" />
<script src="${basePath}/js/msdropdown/js/msdropdown/jquery.dd.min.js"></script>
<link rel="stylesheet" type="text/css" href="${basePath}/js/msdropdown/css/msdropdown/flags.css" />
<link rel="stylesheet" type="text/css" href="${basePath}/js/msdropdown/css/msdropdown/skin2.css" />
<link rel="stylesheet" type="text/css" href="${basePath}/js/msdropdown/css/msdropdown/sprite.css" />
<style>
img{
	display: inline-block;  height: auto;  max-width: 100%;
}
</style>
<script type="text/javascript">
	//导航菜单--设置鼠标滑过
	$(function() {
		$(".dropdown").mouseover(function() {
			$(this).addClass("open");
		});

		$(".dropdown").mouseleave(function() {
			$(this).removeClass("open");
		})

	})
	function logout() {
		Ewin.confirm({
			message : "确定要注销吗？"
		}).on(function(e) {
			if (e) {
				window.location.href = "${basePath }/UserAction_logout.action";
			}
		});
	}
</script>
</head>
<body>
	<!--nav-->
	<nav class="navbar navbar-default" id="top">
		<!--nav container-fluid-->
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand"
					href="${basePath }/index.jsp"
					style="font-weight: bolder; margin-top: -4px;"><span
					class="glyphicon glyphicon-home" style="font-size: 20px;"></span>&nbsp;家庭记账管理系统</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav" id="menu">
					<li class="dropdown"><a href="javascript:;"
						class="dropdown-toggle" data-toggle="dropdown" role="button"
						aria-haspopup="true" aria-expanded="false"><img
							src="${basePath }/img/out.png" />&nbsp;支出账单 <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="${basePath}/OutAccountAction_list.action"><span
									style="color: red;" class="glyphicon glyphicon-jpy"></span>&nbsp;支出账单列表</a></li>
							<li><a href="${basePath}/OutAccountTypeAction_list.action"><span
									style="color: red;" class="glyphicon glyphicon-list-alt"></span>&nbsp;支出账单类型</a></li>
							<li><a href="${basePath}/page_user_outaccount_report.action"><span
									style="color: red;" class="glyphicon glyphicon-stats"></span>&nbsp;支出报表展示</a></li>
						</ul></li>
					<li class="dropdown"><a href="javascript:;"
						class="dropdown-toggle" data-toggle="dropdown" role="button"
						aria-haspopup="true" aria-expanded="false"><img
							src="${basePath }/img/in.png" />&nbsp;收入账单 <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="${basePath }/InAccountAction_list.action"><span
									style="color: green;" class="glyphicon glyphicon-jpy"></span>&nbsp;收入账单列表</a></li>
							<li><a href="${basePath}/InAccountTypeAction_list.action"><span
									style="color: green;" class="glyphicon glyphicon-list-alt"></span>&nbsp;收入账单类型</a></li>
							<li><a href="${basePath}/page_user_inaccount_report.action"><span
									style="color: green;" class="glyphicon glyphicon-stats"></span>&nbsp;收入报表展示</a></li>
						</ul></li>
					<s:if test="#session.currentUser.level==1">
						<li class="dropdown"><a href="javascript:;"
							class="dropdown-toggle" data-toggle="dropdown" role="button"
							aria-haspopup="true" aria-expanded="false"><img
								src="${basePath }/img/myBook.png" /></span>&nbsp;我的账本<span
								class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="${basePath }/InvestAction_list.action"><span
										style="color: gray;" class="glyphicon glyphicon-log-in"></span>&nbsp;投资理财</a></li>
								<li><a href="${basePath }/LoanAction_list.action"><span
										style="color: gray;" class="glyphicon glyphicon-log-out"></span>&nbsp;借款还贷</a></li>
							</ul>
						</li>
					</s:if>
					<li></li>
				</ul>
				<form class="navbar-form navbar-left"
					action="${basePath }/SearchAction_search.action" method="get">
					<div class="form-group">
						<input type="text" name="keyword" class="form-control" required
							placeholder="关键词" value='<s:property value="keyword"/>'>
					</div>
					<input type="submit" value="搜索" class="btn btn-default">
				</form>
				<ul class="nav navbar-nav navbar-right">
					<li style="margin-top: 15px;">欢迎您的使用：</li>
					<li style="margin-top: 2px;">
						<h5>
							<img alt=""
								src="${basePath }/<s:property value="#session.currentUser.face"/>"
								width="29" height="29">
							<s:property value="#session.currentUser.user_name" />
							<s:if test="#session.currentUser.level==0">
								<span class="label label-success">普通成员</span>
							</s:if>
							<s:if test="#session.currentUser.level==1">
								<span class="label label-warning">家庭户主</span>
							</s:if>
						</h5>
					</li>
					<li class="dropdown"><a href="javascript:;"
						class="dropdown-toggle" data-toggle="dropdown" role="button"
						aria-haspopup="true" aria-expanded="false">个人中心<span
							class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="${basePath }/page_user_common_userdesc.action"><span
									class="glyphicon glyphicon-user"></span>&nbsp;个人资料</a></li>
							<li><a href="${basePath }/TopicAction_myList.action"><span
									class="glyphicon glyphicon-tags"></span>&nbsp;我的帖子</a></li>
							<li><a href="${basePath }/RegionAction_list.action"><span
									class="glyphicon glyphicon-comment"></span>&nbsp;交流中心</a></li>
							<li><a href="${basePath }/page_user_common_editpwd.action"><span
									class="glyphicon glyphicon-edit"></span>&nbsp;修改密码</a></li>
							<li><a href="javascript:;" onclick="logout()"><span
									class="glyphicon glyphicon-off"></span>&nbsp;注销用户</a></li>
						</ul></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>

</body>
</html>