<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<title>主界面</title>
<script src="${basePath}/js/onclock.js"></script>
<script src="${basePath}/js/script.js"></script>
<script src="${basePath}/js/main.js"></script>
<script src="${basePath}/js/typed.min.js"></script>
<style type="text/css">
#bak_top {
    width: 36px;
    height: 114px;
    background: url(${basePath}/img/gotop.png);
    bottom: 30%;
    -webkit-transition: all .5s ease-in-out;
    -moz-transition: all .5s ease-in-out;
    -o-transition: all .5s ease-in-out;
    -ms-transition: all .5s ease-in-out;
    transition: all .5s ease-in-out;
    right: 50%;
    margin-right: -560px;
    position: fixed;
}
</style>
</head>
<body>
	<%@include file="/WEB-INF/user/common/head.jsp"%>
	<!-- 天气预报 -->
	<div class="container-fluid">
		<iframe class="linkcursor" style="float: right;" width="420"
						scrolling="no" height="60" frameborder="0" allowtransparency="true"
						src="http://i.tianqi.com/index.php?c=code&id=12&icon=6&num=3"></iframe>
		<div class="alert alert-info" role="alert" style="margin-bottom: 0px;">
			<small style="color: #B8B8B8;"><span class="typed" style="float: left;">新版每日一句-微信公众号：小康新鲜事儿</span></small>
		</div>
	</div>
	<!--内容主容器-->
	<div class="container-fluid" style="padding-top: 10px;">
		<div class="well">
			<div>
				<span id="timeMsg"></span>&nbsp;
				<h2 style="display: inline;" >
					<span id="hello" style="color: red; font-family: '隶书';"></span>
				</h2>
			</div>
			<p></p>
			<p>
				您本月总支出额为：<span class="badge"
					style="font-size: 18px; background-color: red;"><s:property
						value="#out_money==null?0:#out_money" />¥</span>&nbsp;|&nbsp;您本月总收入额为：<span
					class="badge" style="font-size: 18px; background-color: green;"><s:property
						value="#in_money==null?0:#in_money" />¥</span>
			</p>
			<div id="marqueeDiv"></div>
		</div>
		<!--内容墙 end-->

		<div class="alert alert-info" role="alert" style="margin-bottom: 0px;">系统功能模块</div>
		<!--栅格系统-->
		<div class="row" style="padding-top: 10px;">
			<div class="col-sm-4 col-md-4">
				<div class="thumbnail">
					<div class="caption">
						<h3>账单管理</h3>
						<p>在这里您可以实现支出账单和收入账单的统计、录入、修改、删除</p>
						<p>
							<a
								href="${basePath}/OutAccountAction_list.action"
								class="btn btn-info" role="button">支出账单</a> <a
								href="${basePath}/InAccountAction_list.action"
								class="btn btn-default" role="button">收入账单</a>
						</p>
					</div>
				</div>
			</div>
			<s:if test="#session.currentUser.level==1">
			<div class="col-sm-4 col-md-4">
				<div class="thumbnail">
					<div class="caption">
						<h3>我的账本</h3>
						<p>在这儿您可以进行记账，如别人借你钱或者你借别人钱都可以在这里登记、查看、修改、删除</p>
						<p>
							<a
								href="${basePath}/InvestAction_list.action"
								class="btn btn-info" role="button">投资理财</a> <a
								href="${basePath}/LoanAction_list.action"
								class="btn btn-default" role="button">借款还贷</a>
						</p>
					</div>
				</div>
			</div>
			</s:if>
			<div class="col-sm-4 col-md-4">
				<div class="thumbnail">
					<div class="caption">
						<h3>用户交流区</h3>
						<p>在这里，你可以和其他用户进行交流和讨论，一起在这里玩</p>
						<BR />
						<p>
							<a
								href="${basePath}/RegionAction_list.action"
								class="btn btn-info" role="button">交流大区</a>
						</p>
					</div>
				</div>
			</div>
		</div>
		<!--栅格系统 end-->
		<!--最近发的帖子-->
		<div class="alert alert-success" role="alert"
			style="margin-bottom: 0px;">
			我最近发的帖子<a class="pull-right"
				href="${basePath}/TopicAction_myList.action">查看更多<span
				class="glyphicon glyphicon-triangle-right"></span></a>
		</div>
		<s:if test="#topiclist.size==0">
			<h3>您最近没有发布任何帖子</h3>
		</s:if>
		<s:else>
			<ul class="list-group">
				<s:iterator var="topic" value="#topiclist">
					<li class="list-group-item"><span class="badge">回复数：<s:property
								value="#topic.replys.size" /></span> <span
						style="font-weight: bolder; font-size: 18px;"><s:if
								test="#topic.is_end=='0'">
								<span class="label label-default">【未结帖】</span>
							</s:if> <s:else>
								<span class="label label-warning">【已结帖】</span>
							</s:else>【标题】</span><a
						href="TopicAction_showContent.action?topic_id=<s:property value="#topic.topic_id"/>"><s:property
								value="#topic.topic_title" /></a> <span class="label label-info">发表日期：<s:property
								value="#topic.topic_datetime_s" /></span> <s:if
							test="#topic.is_good=='1'">
							<span class="label label-danger">【加精】</span>
						</s:if> <s:if test="#topic.is_top=='1'">
							<span class="label label-success">【置顶】</span>
						</s:if></li>
				</s:iterator>
			</ul>
		</s:else>
		<!--最近发的帖子 end-->
		<!--最近我的回复-->
		<div class="alert alert-success" role="alert"
			style="margin-bottom: 0px;">
			<span class="glyphicon glyphicon-star"></span>我最近的回复<a
				class="pull-right"
				href="${basePath}/TopicAction_myList.action">查看更多<span
				class="glyphicon glyphicon-triangle-right"></span></a>
		</div>
		<s:if test="#replylist.size==0">
			<H3>您最近没有任何回复</H3>
		</s:if>
		<s:else>
			<ul class="list-group">
			<s:iterator value="#replylist" var="reply">
				<a class="list-group-item" href="javascript:;"> <span class="badge"><span
						class="glyphicon glyphicon-thumbs-up"></span>
					<s:property value="#reply.zan" /></span> <span class="badge">回复时间：<s:property
							value="#reply.reply_datetime_s" /></span>
					<dl>
						<dt>
							<span style="font-weight: bolder;">【标题】</span>
							<s:property value="#reply.topic.topic_title" />
						</dt>
						<dd>
							【我的回复】
							<s:property value="#reply.reply_content" escape="false" />
						</dd>
					</dl>
				</a>
			</s:iterator>
			</ul>
		</s:else>
		<!--最近我的回复end-->
		<!--收入账单排行榜-->
		<div class="alert alert-success" role="alert"
			style="margin-bottom: 0px;">
			<span class="glyphicon glyphicon-star"></span>收入账单排行榜<a
				class="pull-right" href="${basePath}/InAccountAction_list.action">查看更多<span
				class="glyphicon glyphicon-triangle-right"></span></a>
		</div>
		<table id="inaccount"></table>
		<!--收入账单排行榜 end-->
		<!--支出账单排行榜-->
		<div class="alert alert-warning" role="alert"
			style="margin-bottom: 0px;">
			<span class="glyphicon glyphicon-star"></span>支出账单排行榜<a
				class="pull-right" href="${basePath}/OutAccountAction_list.action">查看更多<span
				class="glyphicon glyphicon-triangle-right"></span></a>
		</div>
		<table id="outaccount"></table>
		<!--支出账单排行表end-->
		<a id="bak_top"  title="回到顶部" style="display: none;"></a>
	</div>
	<%@include file="/WEB-INF/user/common/foot.jsp"%>
</body>
<script>
$(()=>{
	let inUrl="InAccountAction_findSixInAccount.action";
	let outUrl="OutAccountAction_findSixOutAccount.action";
    InitTable(inUrl);
    InitTable1(outUrl);
});
let InitTable = function (url){
	//先销毁表格
	$('#inaccount').bootstrapTable("destroy");
	//加载表格
	$("#inaccount").bootstrapTable({
		url : url, //请求地址
		method : 'post',
		dataType: "json",//期待返回数据类型
		locale: "zh-CN",//中文支持
		height:400,
		classes: "table table-bordered table-striped",
		showLoading : true,
		search: false,
		striped : true, //是否显示行间隔色
		sortable : false, //是否启用排序
		pageNumber : 1, //初始化加载第一页
		pagination : false,//是否分页
		sidePagination : 'client',//server:服务器端分页|client：前端分页
		pageSize : 6,//单页记录数
		showRefresh : false,//刷新按钮
		columns : [ {
			title : '序号',
			field : 'SerialNumber',
			align : 'center',
			valign : 'middle',
			formatter : function (value, row, index) {
				return index+1;
			}
		}, {
			title : '收入类型',
			field : 'inAccountType.inaccounttype_name',
			align : 'center',
			valign : 'middle',
		}, {
			title : '收入金额',
			field : 'inaccount_money',
			align : 'center',
			valign : 'middle',
		}, {
			title : '收入时间',
			field : 'inaccount_datetime_s',
			align : 'center',
			valign : 'middle',
		}, {
			title : '账单创建时间',
			field : 'inaccount_createtime_s',
			align : 'center',
			valign : 'middle',
		}, {
			title : '备注',
			field : 'inaccount_desc',
			align : 'center',
			valign : 'middle',
		},{
		    field: 'SerialNumber',
		    title: '名次',
		    align : 'center',
			valign : 'middle',
		    formatter: function (value, row, index) {
		    	let num=index+1;
		    	if(num === 1){
		        	return '第'+num+'名&nbsp;<img src="${basePath}/img/first.png"/>';
		    	}else if(num === 2){
		        	return '第'+num+'名&nbsp;<img src="${basePath}/img/second.png"/>';
		    	}else if(num === 3){
		        	return '第'+num+'名&nbsp;<img src="${basePath}/img/third.png"/>';
		    	}else{
		    		return '第'+num+'名';
		    	}
		    }
		}]
	});
	return InitTable;
}

let InitTable1 = function (url){
	//先销毁表格
	$('#outaccount').bootstrapTable("destroy");
	//加载表格
	$("#outaccount").bootstrapTable({
		url : url, //请求地址
		method : 'post',
		dataType: "json",//期待返回数据类型
		locale: "zh-CN",//中文支持
		height:400,
		classes: "table table-bordered table-striped",
		showLoading : true,
		search: false,
		striped : true, //是否显示行间隔色
		sortable : false, //是否启用排序
		pageNumber : 1, //初始化加载第一页
		pagination : false,//是否分页
		sidePagination : 'client',//server:服务器端分页|client：前端分页
		pageSize : 6,//单页记录数
		showRefresh : false,//刷新按钮
		columns : [ {
			title : '序号',
			field : 'SerialNumber',
			align : 'center',
			valign : 'middle',
			formatter : function (value, row, index) {
				return index+1;
			}
		}, {
			title : '支出类型',
			field : 'outAccountType.outaccounttype_name',
			align : 'center',
			valign : 'middle',
		}, {
			title : '支出金额',
			field : 'outaccount_money',
			align : 'center',
			valign : 'middle',
		}, {
			title : '支出时间',
			field : 'outaccount_datetime_s',
			align : 'center',
			valign : 'middle',
		}, {
			title : '账单创建时间',
			field : 'outaccount_createtime_s',
			align : 'center',
			valign : 'middle',
		}, {
			title : '备注',
			field : 'outaccount_desc',
			align : 'center',
			valign : 'middle',
		},{
		    field: 'SerialNumber',
		    title: '名次',
		    align : 'center',
			valign : 'middle',
		    formatter: function (value, row, index) {
		    	let num=index+1;
		    	if(num === 1){
		        	return '第'+num+'名&nbsp;<img src="${basePath}/img/first.png"/>';
		    	}else if(num === 2){
		        	return '第'+num+'名&nbsp;<img src="${basePath}/img/second.png"/>';
		    	}else if(num === 3){
		        	return '第'+num+'名&nbsp;<img src="${basePath}/img/third.png"/>';
		    	}else{
		    		return '第'+num+'名';
		    	}
		    }
		}]
	});
	return InitTable1;
}
</script>
</html>