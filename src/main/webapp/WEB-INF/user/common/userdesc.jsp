<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
		<title>个人资料</title>
		<style type="text/css">
				.infocolor{
					color: darkgray;
				}
		</style>
	</head>

	<body>
		<%@include file="/WEB-INF/user/common/head.jsp" %>
		<div class="container">
			<ol class="breadcrumb" style="font-size: 15px;">
				<li>
					<a href="${basePath }/index.jsp">首页</a>
				</li>
				<li>
					<a href="javascript:;">个人中心</a>
				</li>
				<li>
					个人资料
				</li>
			</ol>
		</div>
		
		<div class="container">
			<!--main row-->
			<div class="row" align="center">
				<div class="col-md-12 col-sm-12 col-xs-12"><a href="${basePath }/page_user_common_editinfo.action" style="font-size: 15px;" class="pull-right">编辑资料</a></div>
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div style="font-size: 16px;">
						<s:if test="#session.currentUser.face!=null">
						  <img src="${basePath }/<s:property value="#session.currentUser.face"/>"/>
						</s:if>
						<s:else>
							无头像
						</s:else>
						<p><s:property value="#session.currentUser.user_name"/></p></div>
				</div>
				<div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6" style="margin-top: 10px;font-size: 12px;">
					<div class="row" style="font-size: 16px;">
						<div class="col-md-6 col-sm-6 col-xs-12" align="center">
							<span class="glyphicon glyphicon-user" style="color:red;"></span>真实姓名：
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 infocolor"><s:property value="#session.currentUser.realname==null?'暂无数据':#session.currentUser.realname"/></div>
						<div class="col-md-6 col-sm-6 col-xs-12" align="center">
							<img src="${basePath }/img/sex.png" />性&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;别：
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 infocolor"><s:property value="#session.currentUser.user_sex==null?'保密':#session.currentUser.user_sex"/></div>

						<div class="col-md-6 col-sm-6 col-xs-12" align="center">
							<span class="glyphicon glyphicon-credit-card"></span>身份证号：
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 infocolor"><s:property value="#session.currentUser.idcard==null?'暂无数据':#session.currentUser.idcard"/></div>

						<div class="col-md-6 col-sm-6 col-xs-12" align="center">
							<span class="glyphicon glyphicon-phone"></span>手机号码：
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 infocolor"><s:property value="#session.currentUser.user_phone==null?'暂无数据':#session.currentUser.user_phone"/></div>

						<div class="col-md-6 col-sm-6 col-xs-12" align="center">
							<span class="glyphicon glyphicon-envelope"></span>邮箱地址：
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 infocolor"><s:property value="#session.currentUser.email==null?'暂无数据':#session.currentUser.email"/></div>

						<div class="col-md-6 col-sm-6 col-xs-12" align="center">
							<span class="glyphicon glyphicon-home" style="color: red;"></span>家庭地址：
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 infocolor"><s:property value="#session.currentUser.address==null?'暂无数据':#session.currentUser.address"/></div>

						<div class="col-md-6 col-sm-6 col-xs-12" align="center">
							<img src="${basePath }/img/birth.png"/>出生日期：
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 infocolor"><s:property value="#session.currentUser.birthday==null?'暂无数据':#session.currentUser.birthday_s"/></div>

						<div class="col-md-6 col-sm-6 col-xs-12" align="center">
							<img src="${basePath }/img/level.png"/>用户级别：
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<s:if test='#session.currentUser.level==0'>
							 <span class="label label-success">普通成员</span></h5>
							</s:if>
							<s:if test='#session.currentUser.level==1'>
								<span class="label label-warning">家庭户主</span>
							</s:if>
						</div>
					</div>
				</div>
				<div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6" style="margin-top: 4px;font-size: 12px;">
					<div class="row" style="font-size: 16px;">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<img src="${basePath }/img/status.png">账号状态：
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12" style="margin-top:2px;"><span class="label label-success"><s:property value="#session.currentUser.state=='0'?'正常':'被封'"/></span></div>
					</div>
				</div>
				<div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6" style="margin-top: 4px;font-size: 12px;">
					<div class="row" style="font-size: 16px;">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<span class="glyphicon glyphicon-time"></span>注册日期：
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12" style="margin-top:2px;">
							<span class="badge"><s:property value="#session.currentUser.register_date_s"/></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<%@include file="/WEB-INF/user/common/foot.jsp"%>
	</body>
</html>