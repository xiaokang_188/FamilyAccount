<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<title>出现异常</title>
</head>
<body>
	<%@include file="/WEB-INF/user/common/head.jsp" %>
		<div align="center">
			<div align="center" style="width: 600px;height: 400px;background-color: #F8F8FF;">
				<div style="height:160px"></div>
			    <h4 style="color: #E07D3F;">系统出现错误！请检查您的操作！</h4>
			</div>
		</div>
	<%@include file="/WEB-INF/user/common/foot.jsp" %>
</body>
</html>