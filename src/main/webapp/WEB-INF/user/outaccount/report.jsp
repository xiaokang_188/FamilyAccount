<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<script src="${basePath}/js/Highcharts8/code/highcharts.js"></script>
<script src="${basePath}/js/Highcharts8/code/themes/grid-light.js"></script>
<script src="${basePath}/js/Highcharts8/code/highcharts-more.js"></script>
<script src="${basePath}/js/Highcharts8/code/modules/exporting.js"></script>
<script src="${basePath}/js/Highcharts8/code/modules/data.js"></script>
<script src="${basePath}/js/Highcharts8/code/modules/wordcloud.js"></script>
<script src="${basePath}/js/Highcharts8/code/modules/oldie.js"></script>
<script src="${basePath}/js/Highcharts8/code/highcharts-zh_CN.js"></script>
<title>支出账单报表</title>
</head>
<body>
	<%@include file="/WEB-INF/user/common/head.jsp" %>
	<div class="container-fluid">
		<div><select class="form-control form-inline" name="year" id="selectYear" style="width: 300px;background-color:#349BFF;color:white;font-weight: bolder;">
		    <option value="">-请选择年份-</option>
			<option>2010</option>
			<option>2011</option>
			<option>2012</option>
			<option>2013</option>
			<option>2014</option>
			<option>2015</option>
			<option>2016</option>
			<option>2017</option>
			<option>2018</option>
			<option>2019</option>
			<option>2020</option>
			<option>2021</option>
			<option>2022</option>
			<option>2023</option>
			<option>2024</option>
			<option>2025</option>
			<option>2026</option>
			<option>2027</option>
			<option>2028</option>
			<option>2029</option>
			<option>2030</option>
			<option>2031</option>
			<option>2032</option>
			<option>2033</option>
			<option>2035</option>
			<option>2036</option>
			<option>2037</option>
			<option>2038</option>
			<option>2039</option>
			<option>2040</option>
		</select>
		</div>
		<div class="row">
			<div class="col-md-4 col-xs-12"><div id="pie"></div></div>
			<div class="col-md-4 col-xs-12"><div id="wordcloud"></div></div>
			<div class="col-md-4 col-xs-12"><div id="spider"></div></div>
			<div class="col-md-12 col-xs-12"><div id="yearly" style="min-width:400px;height:400px"></div></div>
		</div>
	</div>
	<%@include file="/WEB-INF/user/common/foot.jsp" %>
<script>
$(function () {
	//饼图
	$.post("${basePath}/OutAccountAction_findOutAccountGroupByOutAccountTypeName.action",{},function(data){
    	let options={
    			chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                loading: {
            		labelStyle: {
            			fontStyle: 'italic'
            		},
            		hideDuration: 3000,
            		showDuration: 3000
            	},
                credits: {
                    text: '家庭记账管理系统-Powered By xiaokang',// 显示的文字
                    href: 'http://www.xiaokang.cool' // 链接地址
                },
                title: {
                    text: '不同类型支出占比'
                },
                tooltip: {
                    headerFormat: '{series.name}<br>',
                    pointFormat: '{point.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            },
                        }
                    }
                },
                series: [{
                    name: '不同支出类型占比',
                    data:data
                }]	
    	};
    	let chart=Highcharts.chart('pie',options);
},"json");
	//词云图
	$.post("${basePath}/OutAccountTypeAction_findOutAccountTypeName.action",{},function(data){
		let arr=[];
		for(var key in data){
			let word=key;
			obj = {
					name: word,
					weight: 1
				};
			arr.push(obj);
		}
		//console.info(arr);
		// 注意：这里的代码只是对上面的句子进行分词并计算权重（重复次数）并构建词云图需要的数据，其中 arr.find 和 
//		 	reduce 函数可能在低版本 IE 中无法使用（属于ES6新增的函数），如果不能正常使用（对应的函数有报错），请自行加相应的 Polyfill
		//  array.find 的 ployfill 参见：https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/find#Polyfill
//		 	array.reduce 的 ployfill ：https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce#Polyfill
		let options={
				series: [{
					type: 'wordcloud',
					data: arr
				}],
				loading: {
					labelStyle: {
						fontStyle: 'italic'
					},
					hideDuration: 3000,
					showDuration: 3000
				},
				credits: {
					text: '家庭记账管理系统-Powered By xiaokang',// 显示的文字
					href: 'http://www.xiaokang.cool' // 链接地址
				},
				title: {
					text: '支出类型词云图'
				}	
    	};
    	let chart=Highcharts.chart('wordcloud',options);
	},"json");
	//蜘蛛图
    $.post("${basePath}/OutAccountAction_findOutAccountGroupByOutAccountTypeName.action",{},function(data){
    	let arr=[];
    	let arr1=[];
    	for(let i=0;i<data.length;i++){
    		arr[i]=data[i][0];
    	}
    	for(let i=0;i<data.length;i++){
    		arr1[i]=data[i][1];
    	}
    	let options={
    			chart: {
    				polar: true,
    				type: 'area'
    			},
    			loading: {
    				labelStyle: {
    					fontStyle: 'italic'
    				},
    				hideDuration: 3000,
    				showDuration: 3000
    			},
    			credits: {
    				text: '家庭记账管理系统-Powered By xiaokang',// 显示的文字
    				href: 'http://www.xiaokang.cool' // 链接地址
    			},
    			title: {
    				text: '维度分析'
    			},
    			subtitle :{
                	text: '支出类型与总支出'  
                },
    			pane: {
    				size: '80%'
    			},
    			xAxis: {
    				categories: arr,
    				tickmarkPlacement: 'on',
    				lineWidth: 0,
    				labels:{
    					formatter: function() {
    						if(this.axis.userOptions.selectedX === this.pos) {
    							return '<a href="javascript: void(0)" style="color: #7CB5EC">'+this.value+'</a>';
    						}
    						return '<a href="javascript:highlightCategory('+this.pos+')">' + this.value + '</a>'
    					}
    				}
    			},
    			yAxis: {
    				gridLineInterpolation: 'polygon',
    				lineWidth: 0,
    				min: 0
    			},
    			tooltip: {
    				shared: true,
    				pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:.0f}元</b><br/>'
    			},
    			legend: {
    				align: 'right',
    				verticalAlign: 'top',
    				y: 70,
    				layout: 'vertical'
    			},
    			series: [{
    				name: '支出总计',
    				data: arr1,
    				pointPlacement: 'on',
    				point: {
    					events: {
    						click: function() {
    							highlightCategory(this.x);
    						}
    					}
    				}
    			}]	
    	};
    	let chart=Highcharts.chart('spider',options);
    	function highlightCategory(category) {
        	let xAxis = chart.xAxis[0];
        	if(xAxis.userOptions.selectedX && xAxis.userOptions.selectedX === category) {
        		return false;
        	}
        	xAxis.update({
        		selectedX: category
        	});
        }
},"json");
  	//综合年度分析图表
	$.post("${basePath}/OutAccountAction_findOutAccountWithYearly.action",{},function(data){
		let yearList=[];
	    let monthList=[];
	    let avgJson=[];
		
		yearJson=data[0];
    	let $yearlyData={};
    	for(let i=0;i<yearJson.length;i++){
   			$yearlyData={
				name: yearJson[i][0],
				y: yearJson[i][1],
				color: Highcharts.getOptions().colors[i]
   			}
   			yearList.push($yearlyData);
    	}
    	
    	monthJson=data[1];
    	let $monthData={};
    	for(let i=0;i<monthJson.length;i++){
   			$monthData={
   					type: 'column',
   					name: monthJson[i].shift(),
   					data: monthJson[i]
   	   		}
    		monthList.push($monthData);
    	}
    	
    	avgJson=data[2];
    	//tmp为最终平均值集合
    	let tmp=[0, 0, 0, 0, 0,0,0,0,0,0,0,0];
    	for(let i=0;i<avgJson.length;i++){
    		for(let j=0;j<avgJson[i].length;j++){
    			if(avgJson[i][j]!=0){
        			tmp[j]=avgJson[i][j];
        		}
    		}
    	}
    	//console.info(tmp);
    	//每月各个类型总支出的平均值
    	$avg={
				type: 'spline',
				name: '平均值',
				data: tmp,
				marker: {
					lineWidth: 2,
					lineColor: Highcharts.getOptions().colors[3],
					fillColor: 'white'
				}
			} 
    	//当年不同收入类型总支出
    	let $yearly={
				type: 'pie',
				name: '年支出情况',
				data: yearList,
				center: [100, 60],
				size: 80,
				showInLegend: false,
				dataLabels: {
					enabled: false
				}
			}
    	monthList.push($avg);
    	monthList.push($yearly);
    	
		let options={
   		 	loading: {
   				labelStyle: {
   					fontStyle: 'italic'
   				},
   				hideDuration: 3000,
   				showDuration: 3000
   			},
   			credits: {
   				text: '家庭记账管理系统-Powered By xiaokang',// 显示的文字
   				href: 'http://www.xiaokang.cool' // 链接地址
   			},
			title: {
				text: '年度支出分析'
			},
			 yAxis: {
                 allowDecimals: false,
                 title: {
                     text: '支出金额/元'
                 }
             },
			xAxis: {
				categories: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月']
			},
			plotOptions: {
				series: {
					stacking: 'normal'
				}
			},
			labels: {
				items: [{
					html: '年支出情况',
					style: {
						left: '100px',
						top: '18px',
						color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
					}
				}]
			},
			series: monthList
	};
	let chart=Highcharts.chart('yearly',options);
},"json");
	
    $("#selectYear").change(function(){
    	
        let  year=$("#selectYear").val();
    	$.post("${basePath}/OutAccountAction_findOutAccountGroupByOutAccountTypeName.action",{"year":year},function(data){
    		let options={
        			chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    loading: {
                		labelStyle: {
                			fontStyle: 'italic'
                		},
                		hideDuration: 3000,
                		showDuration: 3000
                	},
                    credits: {
                        text: '家庭记账管理系统-Powered By xiaokang',// 显示的文字
                        href: 'http://www.xiaokang.cool' // 链接地址
                    },
                    title: {
                        text: '不同类型支出占比'
                    },
                    tooltip: {
                        headerFormat: '{series.name}<br>',
                        pointFormat: '{point.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                },
                            }
                        }
                    },
                    series: [{
                        name: '不同支出类型占比',
                        data:data
                    }]	
        	};
        	let chart=Highcharts.chart('pie',options);
    },"json");
        $.post("${basePath}/OutAccountAction_findOutAccountGroupByOutAccountTypeName.action",{"year":year},function(data){
        	let arr=[];
        	let arr1=[];
        	for(let i=0;i<data.length;i++){
        		arr[i]=data[i][0];
        	}
        	for(let i=0;i<data.length;i++){
        		arr1[i]=data[i][1];
        	}
        	let options={
        			chart: {
        				polar: true,
        				type: 'area'
        			},
        			loading: {
        				labelStyle: {
        					fontStyle: 'italic'
        				},
        				hideDuration: 3000,
        				showDuration: 3000
        			},
        			credits: {
        				text: '家庭记账管理系统-Powered By xiaokang',// 显示的文字
        				href: 'http://www.xiaokang.cool' // 链接地址
        			},
        			title: {
        				text: '维度分析'
        			},
        			subtitle :{
                    	text: '支出类型与总支出'  
                    },
        			pane: {
        				size: '80%'
        			},
        			xAxis: {
        				categories: arr,
        				tickmarkPlacement: 'on',
        				lineWidth: 0,
        				labels:{
        					formatter: function() {
        						if(this.axis.userOptions.selectedX === this.pos) {
        							return '<a href="javascript: void(0)" style="color: #7CB5EC">'+this.value+'</a>';
        						}
        						return '<a href="javascript:highlightCategory('+this.pos+')">' + this.value + '</a>'
        					}
        				}
        			},
        			yAxis: {
        				gridLineInterpolation: 'polygon',
        				lineWidth: 0,
        				min: 0
        			},
        			tooltip: {
        				shared: true,
        				pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:.0f}元</b><br/>'
        			},
        			legend: {
        				align: 'right',
        				verticalAlign: 'top',
        				y: 70,
        				layout: 'vertical'
        			},
        			series: [{
        				name: '支出总计',
        				data: arr1,
        				pointPlacement: 'on',
        				point: {
        					events: {
        						click: function() {
        							highlightCategory(this.x);
        						}
        					}
        				}
        			}]	
        	};
        	let chart=Highcharts.chart('spider',options);
        	function highlightCategory(category) {
            	let xAxis = chart.xAxis[0];
            	if(xAxis.userOptions.selectedX && xAxis.userOptions.selectedX === category) {
            		return false;
            	}
            	xAxis.update({
            		selectedX: category
            	});
            }
    },"json");
      //综合年度分析图表
    	$.post("${basePath}/OutAccountAction_findOutAccountWithYearly.action",{"year":year},function(data){
    		let yearList=[];
    	    let monthList=[];
    	    let avgJson=[];
    		
    		yearJson=data[0];
        	let $yearlyData={};
        	for(let i=0;i<yearJson.length;i++){
       			$yearlyData={
    				name: yearJson[i][0],
    				y: yearJson[i][1],
    				color: Highcharts.getOptions().colors[i]
       			}
       			yearList.push($yearlyData);
        	}
        	
        	monthJson=data[1];
        	let $monthData={};
        	for(let i=0;i<monthJson.length;i++){
       			$monthData={
       					type: 'column',
       					name: monthJson[i].shift(),
       					data: monthJson[i]
       	   		}
        		monthList.push($monthData);
        	}
        	
        	avgJson=data[2];
        	//tmp为最终平均值集合
        	let tmp=[0, 0, 0, 0, 0,0,0,0,0,0,0,0];
        	for(let i=0;i<avgJson.length;i++){
        		for(let j=0;j<avgJson[i].length;j++){
        			if(avgJson[i][j]!=0){
            			tmp[j]=avgJson[i][j];
            		}
        		}
        	}
        	//console.info(tmp);
        	//每月各个类型总支出的平均值
        	$avg={
    				type: 'spline',
    				name: '平均值',
    				data: tmp,
    				marker: {
    					lineWidth: 2,
    					lineColor: Highcharts.getOptions().colors[3],
    					fillColor: 'white'
    				}
    			} 
        	//当年不同收入类型总支出
        	let $yearly={
    				type: 'pie',
    				name: '年支出情况',
    				data: yearList,
    				center: [100, 60],
    				size: 80,
    				showInLegend: false,
    				dataLabels: {
    					enabled: false
    				}
    			}
        	monthList.push($avg);
        	monthList.push($yearly);
        	
    		let options={
       		 	loading: {
       				labelStyle: {
       					fontStyle: 'italic'
       				},
       				hideDuration: 3000,
       				showDuration: 3000
       			},
       			credits: {
       				text: '家庭记账管理系统-Powered By xiaokang',// 显示的文字
       				href: 'http://www.xiaokang.cool' // 链接地址
       			},
    			title: {
    				text: '年度支出分析'
    			},
    			yAxis: {
                     allowDecimals: false,
                     title: {
                         text: '支出金额/元'
                     }
                },
    			xAxis: {
    				categories: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月']
    			},
    			plotOptions: {
    				series: {
    					stacking: 'normal'
    				}
    			},
    			labels: {
    				items: [{
    					html: '年支出情况',
    					style: {
    						left: '100px',
    						top: '18px',
    						color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
    					}
    				}]
    			},
    			series: monthList
    	};
    	let chart=Highcharts.chart('yearly',options);
    },"json");
        
    });
});
</script>
</body>
</html>