<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
		<title>添加收入账单</title>
		<style type="text/css">
			.error{
				color: red;
			}
		</style>
		<script type="text/javascript">
			$(function(){
				$("#form1").validate({
					rules:{
						"inAccountType.inaccounttype_id":{
							"required":true
						},
						"inaccount_datetime":{
							"required":true
						},
						"inaccount_money":{
							"required":true,
							"number":true
						}
					},
					messages:{
						"inAccountType.inaccounttype_id":{
							"required":"收入类型不能为空"
						},
						"inaccount_datetime":{
							"required":"收入时间不能为空"
						},
						"inaccount_money":{
							"required":"收入金额不能为空",
							"number":"金额必须为数字"
						}
					}
				});
			})
		</script>
	</head>
	<body>
		<%@include file="/WEB-INF/user/common/head.jsp" %>
		<div class="container-fluid">
			<ol class="breadcrumb">
				<li>
					<a href="${basePath }/index.jsp">首页</a>
				</li>
				<li>
					<a href="javascript:;">收入账单</a>
				</li>
				<li>
					<a href="${basePath }/InAccountAction_list.action">收入账单列表</a>
				</li>
				<li>
					<span class="glyphicon glyphicon-plus"></span>添加收入账单
				</li>
			</ol>
			
			<div class="container">
				<form action="${basePath }/InAccountAction_add.action" method="post" id="form1">
				<div class="row">
					<div class="col-md-offset-3 col-md-5" style="border: 1px solid antiquewhite;background-color: ghostwhite;border-radius: 6px;padding: 10px;" align="center">
						<div class="form-group form-inline">
							<label class="control-label">收入账单类型:</label>
							<script type="text/javascript">
								$(function(){
									//异步请求收入类型
									$.post("${basePath}/InAccountTypeAction_listAjax.action",{},function(data){
										 for(let i=0;i<data.length;i++){
										     $("#inaccounttype").append("<option value='"+data[i].inaccounttype_id+"'>"+data[i].inaccounttype_name+"</option>")
										 }
									},"json")
								});
							</script>
							<select class="form-control" style="width: 220px;" id="inaccounttype" name="inAccountType.inaccounttype_id">
							</select>
						</div>
						<div class="form-group form-inline" style="padding-left: 25px;">
							<label class="control-label">收入时间:</label>
							<input style="width: 220px;" type="text" class="form-control" name="inaccount_datetime" id="outaccounttime" style="width: 220px;" placeholder="点击这里选择日期" readonly="readonly"/>
							<script type="text/javascript">
									 // 日期控件
									 $('#outaccounttime').datetimepicker({
											 	 	pickerPosition: 'up-right',
											 	 	format: 'yyyy-mm-dd ',
										            language:  'zh-CN',  
										            weekStart: 1,  
										            todayBtn:  1,  
										            autoclose: 1,  
										            todayHighlight: 1,  
										            startView: 2,  
										            minView: 2,  
										            forceParse: 0  
									           });
								</script>
						</div>
						<div class="form-group form-inline" style="padding-left: 25px;">
						<label class="control-label">收入金额:</label>
							<div class="input-group spinner" data-trigger="spinner" style="width: 220px;">
					          <input type="text"  required="required" name="inaccount_money" class="form-control text-center" value="11" data-rule="currency">
					          <div class="input-group-addon">
					            <a href="javascript:;" class="spin-up" data-spin="up"><i class="fa fa-caret-up"></i></a>
					            <a href="javascript:;" class="spin-down" data-spin="down"><i class="fa fa-caret-down"></i></a>
					          </div>
					        </div>
						</div>
						<div class="form-group form-inline" style="padding-left: 50px;">
							<label class="control-label">备注:</label>
							<textarea style="width: 220px;" class="form-control" name="inaccount_desc"  placeholder="备注，方便记忆"></textarea>
						</div>
						<input type="reset" value="重置" class="btn btn-warning"/>
						<input type="submit" value="提交添加" class="btn btn-danger"/>
					</div>
				</div>
				</form>
			</div>
			<%@include file="/WEB-INF/user/common/foot.jsp" %>
		</div>
	</body>
</html>