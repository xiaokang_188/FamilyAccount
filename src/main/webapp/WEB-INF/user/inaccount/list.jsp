<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,user-scalable=no">
<title>收入账单列表</title>
<style>
th {
	padding: 0;
	margin: 0;
	background-color: #d9edf7;
}
#inaccount tbody>tr:hover {
	background-color: #449d44;
}
</style>
</head>
<body>
	<%@include file="/WEB-INF/user/common/head.jsp"%>
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li><a href="${basePath }/index.jsp">首页</a></li>
			<li><a href="javascript:;">收入账单</a></li>
			<li>收入账单列表</li>
		</ol>

		<div class="well">
			<form class="form-inline">
				<b>日期:</b>
				<input type="text" data-date-format="yyyy-mm-dd" name="startTime"
					id="startTime" class="form-control date" placeholder="选择开始时间"
					readonly="readonly" value="<s:property value="startTime"/>"/>
				<b>-</b>
				<input style="margin-right: 25px;" type="text"
					data-date-format="yyyy-mm-dd" name="endTime" id="endTime"
					class="form-control date" placeholder="选择结束时间" readonly="readonly"
					value="<s:property value="endTime"/>"/>
				<b>备注内容:</b>
				<input
					style="margin-right: 25px;" type="text" class="form-control"
					id="desc" value="<s:property value="inaccount_desc"/>"
					name="inaccount_desc" /> <a class="btn btn-primary"
					onclick="resetForm()"> <span
					class="glyphicon glyphicon-refresh"></span>重置
				</a>
				<button class="btn btn-primary" id="searchBtn">
					<span class="glyphicon glyphicon-search"></span>搜索
				</button>
			</form>
		</div>

		<a style="float:right;" class="btn btn-success"
			href="${basePath }/page_user_inaccount_add.action"><span
			class="glyphicon glyphicon-plus"></span>添加收入账单</a>
		<div id="toolbar" class="btn-group">
 				<button class="btn btn-danger" onclick="exportExcel()">
					<span class="glyphicon glyphicon-cloud-download"></span>导出
				</button>
				<button class="btn btn-warning" id="upload_btn">
					<span class="glyphicon glyphicon-cloud-upload"></span>导入
				</button>
 		</div>
		<table id="inaccount"></table>

		<!--==================修改收入账单模态框=================-->
		<div class="modal fade" id="editForm" tabindex="-1" role="dialog"
			aria-labelledby="editFormLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" aria-label="Close"
							onclick="closeData()">
							<span aria-hidden="true">×</span>
						</button>
						<h5 class="modal-title" id="exampleModal3Label">
							<span class="glyphicon glyphicon-edit"></span>修改收入账单
						</h5>
					</div>
					<div class="modal-body" align="center">
						<form id="updateForm"
							action="${basePath }/InAccountAction_update.action" method="post">
							<div class="form-group form-inline">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label for=""
									class="control-label" style="color: red;"><b>编号:</b></label> <input
									type="text" class="form-control" id="outid" name="inaccount_id"
									readonly="readonly" style="width: 240px;">
							</div>
							<div class="form-group form-inline">
								<label for="recipient-name" class="control-label">收入类型:</label>
								<select class="form-control" style="width: 240px;"
									name="inAccountType.inaccounttype_id" id="intype">
								</select>
							</div>
							<div class="form-group form-inline">
								<label for="recipient-name" class="control-label">收入金额:</label>
								<input class="form-control" type="text" style="width: 240px;"
									name="inaccount_money" required="required" />
							</div>
							<div class="form-group form-inline">
								<label for="recipient-name" class="control-label">收入时间:</label>
								<input type="text" class="form-control" id="outaccounttime"
									style="width: 240px;" name="inaccount_datetime"
									readonly="readonly" required="required" />
							</div>
							<div class="form-group form-inline">
								<label for="recipient-name" class="control-label">创建时间:</label>
								<input type="text" class="form-control" id="outcreatetime"
									style="width: 240px;" name="inaccount_createtime"
									readonly="readonly" />
							</div>
							<div class="form-group form-inline">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label for="message-text"
									class="control-label">备注:</label>
								<textarea class="form-control" id="message-text"
									style="width: 240px;" name="inaccount_desc"></textarea>
							</div>
							<div class="form-group form-inline">
								<input class="form-control btn btn-warning" value="提交修改"
									type="submit" />
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary"
							onclick="closeData()" id="closeData">关闭</button>
					</div>
				</div>
			</div>
		</div>
		<!--==================修改收入账单模态框 end==============-->
		<%@include file="/WEB-INF/user/common/foot.jsp"%>
		<script>
			$(function() {
				let picker1 = $('#startTime').datetimepicker({
					pickerPosition : 'up-left',
					format : 'yyyy-mm-dd ',
					weekStart : 1,
					todayBtn : 1,
					autoclose : 1,
					todayHighlight : 1,
					startView : 2,
					minView : 2,
					forceParse : 0,
					language : 'zh-CN'
				});
				let picker2 = $('#endTime').datetimepicker({
					pickerPosition : 'up-left',
					format : 'yyyy-mm-dd ',
					weekStart : 1,
					todayBtn : 1,
					autoclose : 1,
					todayHighlight : 1,
					startView : 2,
					minView : 2,
					forceParse : 0,
					language : 'zh-CN'
				});
				//动态设置最小值  
				picker1.on('dp.change', function(e) {
					picker2.data('DateTimePicker').minDate(e.date);
				});
				//动态设置最大值  
				picker2.on('dp.change', function(e) {
					picker1.data('DateTimePicker').maxDate(e.date);
				});
			});
			$('#outaccounttime').datetimepicker({
				pickerPosition : 'bottom-right',
				format : 'yyyy-mm-dd ',
				language : 'zh-CN',
				weekStart : 1,
				todayBtn : 1,
				autoclose : 1,
				todayHighlight : 1,
				startView : 2,
				minView : 2,
				forceParse : 0
			});
			//单个删除
			function delInAccountById(id) {
				Ewin.confirm({message : "确认要删除编号为" + id + "的数据吗？"}).on(function(e) {
					if (e) {
						Ewin.confirm({message : "再次确认要删除编号为"+ id+ "的数据吗？"}).on(function(e) {
											if (e) {
												location.href = "${basePath}/InAccountAction_delete.action?inaccount_id="+ id;
											}
										});
					}
				});
			}
			function closeData() {
				$('#editForm').modal('hide');
			}
			//打开模态框
			function openData(id) {
				let inaccounttypeid = 0;
				//手动开启
				$('#editForm').modal({
					backdrop : 'static',
					keyboard : false
				}); //禁止esc和点击模态框外部关闭
				//填充模态框表单
				$.ajax({
					url : '${basePath}/InAccountAction_findById.action',
					type : 'post',
					data : {
						"inaccount_id" : id
					},
					async : false,
					dataType : 'json',
					success : function(data) {
						$("input[name=inaccount_id]").val(data.inaccount_id);
						$("input[name=inaccount_money]").val(
								data.inaccount_money);
						$("textarea[name=inaccount_desc]").val(
								data.inaccount_desc);
						$("input[name=inaccount_datetime]").val(
								data.inaccount_datetime_s);
						$("input[name=inaccount_createtime]").val(
								data.inaccount_createtime_s);
						inaccounttypeid = data.inAccountType.inaccounttype_id;
					},
					error : function(msg) {
						console.info('查询收入账单失败！');
					}
				});
				let intype = $("#intype");
				$.post("InAccountTypeAction_listAjax.action",{},function(data) {
									$("#intype").empty();
									for (let i = 0; i < data.length; i++) {
										intype.append("<option value='"+data[i].inaccounttype_id+"'>"+ data[i].inaccounttype_name+ "</option>")
										//回显
										$("#intype option[value='"+ inaccounttypeid+ "']").attr("selected", true);
									}
								});
			}
		</script>
</body>
<script>
$(()=>{
	//初始化bootstrap-table参数
    let startTime = $("#startTime").val();
    let endTime = $("#endTime").val();
    let desc = $("#desc").val();
    $url = "InAccountAction_listCurrentUser.action?startTime="+ startTime + "&endTime=" +endTime + "&desc=" + desc;
	//console.info("初始化>>>"+$url);
    InitTable($url);
	//查询数据
	$("#searchBtn").click(function () {
	    let startTime = $("#startTime").val();
	    let endTime = $("#endTime").val();
	    let desc = $("#desc").val();
   		let $url = "InAccountAction_listCurrentUser.action?startTime="+ startTime + "&endTime=" +endTime + "&desc=" + desc;
   	    InitTable($url);
	});
	//导入excel
	$("#upload_btn").upload(
		{
			action : "${basePath}/InAccountAction_upload.action",
			name : "myFile",
			onComplete : function(data) { //提交表单之后
				if (data == "1") {
					Ewin.alert({
								message : "<span style='color:green;'>导入成功！</span>"
							});
					window.location.reload();
				} else {
					Ewin.alert({
								message : "<span style='color:red;'>导入失败！</span>"
							});
				}
			}
	});
});
let InitTable = function (url){
	//console.info("真正调用>>>"+url);
	//先销毁表格
	$('#inaccount').bootstrapTable("destroy");
	//加载表格
	$("#inaccount").bootstrapTable({
		url : url, //请求地址
		method : 'post',
		dataType: "json",//期待返回数据类型
		locale: "zh-CN",//中文支持
		height:400,//height:$(window).height()-11,
		toolbar : "#toolbar",
		showLoading : true,
		search: false,
		striped : true, //是否显示行间隔色
		sortable : false, //是否启用排序
		pageNumber : 1, //初始化加载第一页
		pagination : true,//是否分页
		sidePagination : 'client',//server:服务器端分页|client：前端分页
		pageSize : 5,//单页记录数
		pageList : [ 5,10],//可选择单页记录数
		showRefresh : false,//刷新按钮
		columns : [ {
			title : '序号',
			field : 'inaccount_id',
			align : 'center',
			valign : 'middle',
			sortable : true
		}, {
			title : '收入类型',
			field : 'inAccountType.inaccounttype_name',
			align : 'center',
			valign : 'middle',
		}, {
			title : '收入金额',
			field : 'inaccount_money',
			align : 'center',
			valign : 'middle',
		}, {
			title : '收入时间',
			field : 'inaccount_datetime_s',
			align : 'center',
			valign : 'middle',
		}, {
			title : '账单创建时间',
			field : 'inaccount_createtime_s',
			align : 'center',
			valign : 'middle',
		}, {
			title : '备注',
			field : 'inaccount_desc',
			align : 'center',
			valign : 'middle',
		}, {
			title : '操作',
			field : 'inaccount_id',
			width : 120,
			align : 'center',
			valign : 'middle',
			formatter : actionFormatter
		}]
	});
	return InitTable;
}
//操作栏的格式化
function actionFormatter(value, row, index) {
	let inaccount_id = value;
	let result = "";
	result += "<a href='javascript:;' class='label label-primary' onclick=\"openData('"
			+ inaccount_id
			+ "')\" title='编辑'><span class='glyphicon glyphicon-pencil'></span></a>";
	result += "<a href='javascript:;' class='label label-danger' onclick=\"delInAccountById('"
			+ inaccount_id
			+ "')\" title='删除'><span class='glyphicon glyphicon-remove'></span></a>";
	return result;
}
//清空搜索表单
function resetForm() {
	$("#intype option:first").prop("selected", 'selected');
	$("#startTime").val('');
	$("#endTime").val('');
	$("#desc").val('');
}
//导出excel
function exportExcel() {
	location.href = "${basePath}/InAccountAction_download.action";
}
</script>
</html>