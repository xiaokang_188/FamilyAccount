<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,user-scalable=no">
<title>投资理财列表</title>
<style>
th {
	padding: 0;
	margin: 0;
	background-color: #d9edf7;
}
#invest tbody>tr:hover {
	background-color: #449d44;
}
</style>
</head>
<body>
	<%@include file="/WEB-INF/user/common/head.jsp"%>
	<div class="container-fluid">
		<ol class="breadcrumb">
			<li><a href="${basePath }/index.jsp">首页</a></li>
			<li><a href="javascript:;">我的账本</a></li>
			<li>投资理财列表</li>
		</ol>

		<div class="well">
			<form class="form-inline">
				<b>投资名称:</b>
				<input style="margin-right: 25px;" type="text" class="form-control"
					 value="<s:property value="invest_name"/>"
					name="invest_name" id="invest_name"/>
				<b>日期:</b>
				<input type="text" data-date-format="yyyy-mm-dd" name="startTime"
					id="startTime" class="form-control date" placeholder="选择开始时间"
					readonly="readonly" value="<s:property value="startTime"/>"/>
				<b>-</b>
				<input style="margin-right: 25px;" type="text"
					data-date-format="yyyy-mm-dd" name="endTime" id="endTime"
					class="form-control date" placeholder="选择结束时间" readonly="readonly"
					value="<s:property value="endTime"/>"/>
				<b>备注内容:</b>
				<input style="margin-right: 25px;" type="text" class="form-control"
					id="desc" value="<s:property value="invest_desc"/>"
					name="invest_desc" /> <a class="btn btn-primary"
					onclick="resetForm()"> <span
					class="glyphicon glyphicon-refresh"></span>重置
				</a>
				<button class="btn btn-primary" id="searchBtn">
					<span class="glyphicon glyphicon-search"></span>搜索
				</button>
			</form>
		</div>

		<a style="float:right;" class="btn btn-success"
			href="${basePath }/page_user_invest_add.action"><span
			class="glyphicon glyphicon-plus"></span>添加投资账单</a>

		<table id="invest"></table>

		<!--==================修改投资理财账单模态框=================-->
		<div class="modal fade" id="editForm" tabindex="-1" role="dialog"
			aria-labelledby="editFormLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" aria-label="Close"
							onclick="closeData()">
							<span aria-hidden="true">×</span>
						</button>
						<h5 class="modal-title" id="exampleModal3Label">
							<span class="glyphicon glyphicon-edit"></span>修改投资账单
						</h5>
					</div>
					<div class="modal-body" align="center">
						<form id="updateForm" action="${basePath }/InvestAction_update.action" method="post">
							<div class="form-group form-inline">
								<label for="" class="control-label" style="color: red;"><b>编号:</b></label>
								<input type="text"  class="form-control" id="invest_id" name="invest_id" readonly="readonly" style="width:300px;">
							</div>
							<div class="form-group form-inline">
								<label for="recipient-name" class="control-label">投资名称:</label>
								<input class="form-control" type="text" style="width: 240px;" name="invest_name" required="required"/>
							</div>
							<div class="form-group form-inline">
								<label for="recipient-name" class="control-label">投资时间:</label>
								<input class="form-control" readonly="readonly" type="text" style="width: 240px;" id="investdatetime" name="invest_datetime" required="required"/>
							</div>
							<div class="form-group form-inline" style="margin-right: 58px;">
								<label for="recipient-name" class="control-label">周期（年）:</label>
								<input class="form-control" type="text" style="width: 240px;" id="investdatetime" name="invest_year" required="required"/>
							</div>
							<div class="form-group form-inline" style="padding-left: 25px">
								<label for="recipient-name" class="control-label">利率:</label>
								<input class="form-control" type="text" style="width: 240px;" name="interest_rates" required="required"/>
							</div>
							<div class="form-group form-inline">
								<label for="recipient-name" class="control-label">投资金额:</label>
								<input type="text" class="form-control"
									style="width: 240px;" name="invest_money"  required="required"/>
							</div>
							<div class="form-group form-inline">
								<label for="recipient-name" class="control-label">投资股东:</label>
								<input type="text" class="form-control"
									style="width: 240px;" name="invest_target" required="required"/>
							</div>
							<div class="form-group form-inline">
								<label for="recipient-name" class="control-label">创建时间:</label>
								<input type="text" class="form-control" 
									style="width: 240px;" name="invest_createtime" readonly="readonly"/>
							</div>
								<div class="form-group form-inline" style="padding-left: 25px">
								<label for="message-text" class="control-label">备注:</label>
								<textarea class="form-control" style="width: 240px;" name="invest_desc"></textarea>
							</div>
							<div class="form-group form-inline">
								<input class="form-control btn btn-warning" value="提交修改"
								type="submit" />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--==================修改投资理财模态框end==============-->
		<%@include file="/WEB-INF/user/common/foot.jsp"%>
		<script>
			$(function() {
				let picker1 = $('#startTime').datetimepicker({
					pickerPosition : 'up-left',
					format : 'yyyy-mm-dd ',
					weekStart : 1,
					todayBtn : 1,
					autoclose : 1,
					todayHighlight : 1,
					startView : 2,
					minView : 2,
					forceParse : 0,
					language : 'zh-CN'
				});
				let picker2 = $('#endTime').datetimepicker({
					pickerPosition : 'up-left',
					format : 'yyyy-mm-dd ',
					weekStart : 1,
					todayBtn : 1,
					autoclose : 1,
					todayHighlight : 1,
					startView : 2,
					minView : 2,
					forceParse : 0,
					language : 'zh-CN'
				});
				//动态设置最小值  
				picker1.on('dp.change', function(e) {
					picker2.data('DateTimePicker').minDate(e.date);
				});
				//动态设置最大值  
				picker2.on('dp.change', function(e) {
					picker1.data('DateTimePicker').maxDate(e.date);
				});
			});
			$('#outaccounttime').datetimepicker({
				pickerPosition : 'bottom-right',
				format : 'yyyy-mm-dd ',
				language : 'zh-CN',
				weekStart : 1,
				todayBtn : 1,
				autoclose : 1,
				todayHighlight : 1,
				startView : 2,
				minView : 2,
				forceParse : 0
			});
			//单个删除
			function delInvestAccountById(id) {
				Ewin.confirm({message : "确认要删除编号为" + id + "的数据吗？"}).on(function(e) {
					if (e) {
						Ewin.confirm({message : "再次确认要删除编号为"+ id+ "的数据吗？"}).on(function(e) {
											if (e) {
												location.href = "${basePath}/InvestAction_delete.action?invest_id="+ id;
											}
										});
					}
				});
			}
			function closeData() {
				$('#editForm').modal('hide');
			}
			//打开模态框
			function openData(id) {
				let inaccounttypeid = 0;
				//手动开启
				$('#editForm').modal({
					backdrop : 'static',
					keyboard : false
				}); //禁止esc和点击模态框外部关闭
				//填充模态框表单
				 $.post("${pageContext.request.contextPath}/InvestAction_findById.action",{"invest_id":id},function(data){
						$("input[name=invest_id]").val(data.invest_id);
						$("input[name=invest_name]").val(data.invest_name);
						$("input[name=invest_datetime]").val(data.invest_datetime_s);
						$("input[name=interest_rates]").val(data.interest_rates);
						$("input[name=invest_money]").val(data.invest_money);
						$("input[name=invest_year]").val(data.invest_year);
						$("input[name=invest_target]").val(data.invest_target);
						$("input[name=invest_createtime]").val(data.invest_createtime_s);
						$("textarea[name=invest_desc]").val(data.invest_desc);
						$("input[name=inaccount_createtime]").val(data.inaccount_createtime_s);
				 });
			}
		</script>
</body>
<script>
$(()=>{
	//初始化bootstrap-table参数
    let investname = $("#invest_name").val();
    let startTime = $("#startTime").val();
    let endTime = $("#endTime").val();
    let desc = $("#desc").val();
   	$url = "InvestAction_listCurrentUser.action?investname=" + investname + "&startTime="+ startTime + "&endTime=" +endTime + "&desc=" + desc;
	//console.info("初始化>>>"+$url);
    InitTable($url);
	//查询数据
	$("#searchBtn").click(function () {
		let investname = $("#invest_name").val();
	    let startTime = $("#startTime").val();
	    let endTime = $("#endTime").val();
	    let desc = $("#desc").val();
   		let $url = "InvestAction_listCurrentUser.action?investname=" + investname + "&startTime="+ startTime + "&endTime=" +endTime + "&desc=" + desc;
   	    InitTable($url);
	});
});
let InitTable = function (url){
	//console.info("真正调用>>>"+url);
	//先销毁表格
	$('#invest').bootstrapTable("destroy");
	//加载表格
	$("#invest").bootstrapTable({
		url : url, //请求地址
		method : 'post',
		dataType: "json",//期待返回数据类型
		locale: "zh-CN",//中文支持
		height:400,
		showLoading : true,
		search: false,
		striped : true, //是否显示行间隔色
		sortable : false, //是否启用排序
		pageNumber : 1, //初始化加载第一页
		pagination : true,//是否分页
		sidePagination : 'client',//server:服务器端分页|client：前端分页
		pageSize : 5,//单页记录数
		pageList : [ 5,10],//可选择单页记录数
		showRefresh : false,//刷新按钮
		columns : [ {
			title : '序号',
			field : 'SerialNumber',
			align : 'center',
			valign : 'middle',
			formatter : function (value, row, index) {
				return index+1;
			}
		}, {
			title : '投资名称',
			field : 'invest_name',
			align : 'center',
			valign : 'middle',
		}, {
			title : '投资目标',
			field : 'invest_target',
			align : 'center',
			valign : 'middle',
		}, {
			title : '投资时间',
			field : 'invest_datetime_s',
			align : 'center',
			valign : 'middle',
		}, {
			title : '投资周期（年）',
			field : 'invest_year',
			align : 'center',
			valign : 'middle',
		}, {
			title : '投资金额',
			field : 'invest_money',
			align : 'center',
			valign : 'middle',
		},{
			title : '利率',
			field : 'interest_rates',
			align : 'center',
			valign : 'middle',
		},{
			title : '预期净利润',
			field : 'profit',
			align : 'center',
			valign : 'middle',
		},{
			title : '账单创建时间',
			field : 'invest_createtime_s',
			align : 'center',
			valign : 'middle',
		},{
			title : '备注内容',
			field : 'invest_desc',
			align : 'center',
			valign : 'middle',
		}, {
			title : '是否到期',
			field : 'overtime',
			align : 'center',
			valign : 'middle',
		}, {
			title : '操作',
			field : 'invest_id',
			width : 120,
			align : 'center',
			valign : 'middle',
			formatter : actionFormatter
		}]
	});
	return InitTable;
}
//操作栏的格式化
function actionFormatter(value, row, index) {
	let invest_id = value;
	let result = "";
	result += "<a href='javascript:;' class='label label-primary' onclick=\"openData('"
			+ invest_id
			+ "')\" title='编辑'><span class='glyphicon glyphicon-pencil'></span></a>";
	result += "<a href='javascript:;' class='label label-danger' onclick=\"delInvestAccountById('"
			+ invest_id
			+ "')\" title='删除'><span class='glyphicon glyphicon-remove'></span></a>";
	return result;
}
//清空搜索表单
function resetForm() {
	$("#invest_name").val('');
	$("#startTime").val('');
	$("#endTime").val('');
	$("#desc").val('');
}
</script>
</html>