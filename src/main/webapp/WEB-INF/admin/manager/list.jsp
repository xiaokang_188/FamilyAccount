<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@include file="/WEB-INF/admin/common/header.jsp"%>
<link rel="stylesheet" type="text/css"
	href="${basePath }/css/default.css">
<title>管理员列表</title>
<script type="text/javascript">
	$(function(){
		$("#tb_manager").datagrid({
			columns:[[
				{title:'编号',field:'manager_id',checkbox:true},
				{title:'姓名',field:'manager_name',width:100,align:'center'},
				{title:'身份证',field:'idcard',width:220,align:'center'},
				{title:'性别',field:'manager_sex',width:80,align:'center'},
				{title:'手机号',field:'manager_phone',width:120,align:'center'},
				{title:'地址',field:'address',width:300,align:'center'},
				{title:'拥有角色',field:'rolenames',width:260,align:'center'}
			]],
			url:'${basePath}/ManagerAction_pageQuery.action',
			rownumbers:true,
			striped:true,
			toolbar:[
				<shiro:hasPermission name="manager-add">
					{text:'添加',iconCls:'icon-add',handler:addManager},
				</shiro:hasPermission>
				<shiro:hasPermission name="manager-update">
					{text:'修改',iconCls:'icon-edit',handler:editManager},
				</shiro:hasPermission>
				<shiro:hasPermission name="manager-delete">
					{text:'删除',iconCls:'icon-remove',handler:deleteManager}
				</shiro:hasPermission>
			],
			pagination:true,
			fit : true
		});
	});
	function addManager(){
		location.href="${basePath}/page_admin_manager_add.action"
	}
	function deleteManager(){
		alert("删除管理员");
	}
	function editManager(){
		var rows=$("#tb_manager").datagrid("getSelected");
		if(rows.length==0||rows==null){
			$.messager.alert("提示","请选择!","warning");
			return ;
		}
		$.messager.confirm("提示","您确定要修改"+rows.manager_name+"的信息吗？",function(e){
			if(e){
				location.href="${basePath}/ManagerAction_foredit.action?manager_id="+rows.manager_id;s
			}
		});
	}
	
</script>
</head>
<body class="easyui-layout">
	<div region="center">
		<table id="tb_manager">
		</table>
	</div>
</body>
</html>