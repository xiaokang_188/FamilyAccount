<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@include file="/WEB-INF/admin/common/header.jsp"%>
<link rel="stylesheet" type="text/css" href="${basePath }/css/default.css">
<title>添加管理员</title>
<script type="text/javascript">
	$(function(){
		$("body").css({visibility:"visible"});
		$("#save").click(function(){
		   var v=$("#form").form("validate");
		   if(v){
			   $("#form").submit();
		   }
			
		})
	});
</script>
</head>
<body class="easyui-layout" style="visibility:hidden;">
    <div region="center" style="overflow:auto;padding:5px;" border="false">
       <form id="form" method="post" action="ManagerAction_add.action">
           <table class="table-edit"  width="95%" align="center">
           		<tr class="title"><td colspan="4">基本信息</td></tr>
	           	<tr><td>用户名:</td><td><input type="text" name="manager_name" id="manager_name" class="easyui-validatebox" required="true" /></td>
					<td>密码:</td><td><input type="password" name="manager_pwd" id="manager_pwd" class="easyui-validatebox" required="true" validType="minLength[5]" /></td></tr>
				<tr class="title"><td colspan="4">其他信息</td></tr>
	           	<tr><td>身份证:</td><td><input type="text" name="idcard" id="idcard" class="easyui-numberbox" /></td>
					<td>出生日期:</td><td><input type="text" name="birthday" id="birthday" class="easyui-datebox" /></td></tr>
	           	<tr><td>性别:</td><td>
	           		 <span class="radioSpan">
			                <input type="radio" name="manager_sex" value="男">男</input>
			                <input type="radio" name="manager_sex" value="女">女</input>
			         </span>
	           	</td>
					<td>住址:</td>
					<td>
						<input type="text" name="address" id="address" class="easyui-validatebox" required="true"/>
					</td>
				</tr>
				<tr>
					<td>联系电话</td>
					<td colspan="3">
						<input type="text" name="manager_phone" id="manager_phone" class="easyui-validatebox" required="true" />
					</td>
				</tr>
	           	<tr><td>备注:</td><td colspan="3"><textarea style="width:75%;height: 100px" name="manager_face"></textarea></td></tr>
	           	<tr>
	           		<td>选择角色：</td>
	           		<td colspan="3">
	           			<input class="easyui-combotree" name="roleIds" data-options="url:'RoleAction_listAjax.action',multiple:true,panelHeight:'auto'"> 
	           		</td>
	           	</tr>
	           	<tr>
					<td colspan="3" align="center">
						<a id="save" icon="icon-save" href="javascript:;" class="easyui-linkbutton" plain="true" >保存</a>
					</td>
				</tr>
           </table>
       </form>
	</div>
</body>
</html>