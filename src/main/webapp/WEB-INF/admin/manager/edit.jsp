<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@include file="/WEB-INF/admin/common/header.jsp"%>
<link rel="stylesheet" type="text/css" href="${basePath }/css/default.css">
<title>修改管理员信息</title>
<script type="text/javascript">
	$(function(){
		$("body").css({visibility:"visible"});
		$("#edit").click(function(){
		   let v=$("#form").form("validate");
		   if(v){
			   $("#form").submit();
		   }
			
		})
	});
</script>
</head>
<body class="easyui-layout" style="visibility:hidden;">
    <div region="center" style="overflow:auto;padding:5px;" border="false">
       <form id="form" method="post" action="ManagerAction_update.action">
           <table class="table-edit"  width="95%" align="center">
           		<tr class="title"><td colspan="4">基本信息</td></tr>
           			<input name="manager_id" type="hidden" value="<s:property value="#manager.manager_id"/>"/>
	           	<tr><td>用户名:</td><td><input type="text" name="manager_name" id="manager_name" class="easyui-validatebox" required="true" value='<s:property value="#manager.manager_name"/>' /></td>
					<td>密码:</td><td><input type="password"  id="manager_pwd" class="easyui-validatebox"  name="manager_pwd"/></td></tr>
				<tr class="title"><td colspan="4">其他信息</td></tr>
	           	<tr><td>身份证:</td><td><input type="text" name="idcard" id="idcard" class="easyui-numberbox" value='<s:property value="#manager.idcard"/>'/></td>
					<td>出生日期:</td><td><input type="text" name="birthday" id="birthday" class="easyui-datebox" value='<s:property value="#manager.birthday_s"/>'/></td></tr>
	           	<tr><td>性别:</td><td>
	           		 <span class="radioSpan">
			                <input type="radio" name="manager_sex" value="男" <s:property value="#manager.manager_sex=='男'?'checked':''"/>>男</input>
			                <input type="radio" name="manager_sex" value="女" <s:property value="#manager.manager_sex=='女'?'checked':''"/>>女</input>
			         </span>
	           	</td>
				<td>住址:</td>
					<td>
						<input type="text" value='<s:property value="#manager.address"/>' class="easyui-validatebox" required="true"/>
					</td>
				</tr>
				<tr>
					<td>联系电话</td>
					<td colspan="3">
						<input type="text" name="manager_phone" value='<s:property value="#manager.manager_phone"/>' id="manager_phone" class="easyui-validatebox" required="true" />
					</td>
				</tr>
	           	<tr><td>备注:</td><td colspan="3"><textarea style="width:75%;height: 100px" name="manager_face"><s:property value="#manager.manager_face"/></textarea></td></tr>
	           	<tr>
	           		<td>选择角色：</td>
	           		<td colspan="3" id="role">
	           		</td>
	           		<script>
	           			$(function(){
	           				$('#station').combobox('select',"<s:property value="#manager.address"/>");
	           				$.post("RoleAction_listAjax.action",function(data){
									let arr=[];
									<s:iterator var="role" value="#manager.roles">
				           			   arr.push("<s:property value="#role.role_id"/>");
				           		    </s:iterator>
								for(let i=0;i<data.length;i++){
									let id=data[i].role_id;
									let name=data[i].role_name;
									let ischeck='';
									for(let j=0;j<arr.length;j++){
										if(arr[j]==id){
											ischeck='checked';
										}
									}
		           					$("#role").append("<input id="+id+" type='checkbox' "+ischeck+" name='roleIds' value="+id+"><label for='"+id+"'>"+name+"</label>");									
								}
	           				});
	           			});
	           		</script>
	           	</tr>
	           	<tr>
					<td colspan="3" align="center">
						<a id="edit" icon="icon-save" href="javascript:;" class="easyui-linkbutton" plain="true" >保存</a>
					</td>
				</tr>
           </table>
       </form>
	</div>
</body>
</html>