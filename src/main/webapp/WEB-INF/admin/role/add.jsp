<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@include file="/WEB-INF/admin/common/header.jsp"%>
<link rel="stylesheet" type="text/css" href="${basePath }/css/default.css">
<link rel="stylesheet" href="${basePath }/js/ztree/metroStyle.css" type="text/css">
<script type="text/javascript" src="${basePath }/js/ztree/jquery.ztree.all-3.5.js"></script>
<title>角色添加</title>
<script type="text/javascript">
	$(function(){
		// 授权树初始化
		let setting = {
			data : {
				simpleData : {
					enable : true,
					idKey:"menu_id"
				}
			},
			check : {
				enable : true//使用ztree的勾选效果
			}
		};
		
		$.ajax({
			url : 'MenuAction_authForRole.action',
			type : 'post',
			dataType : 'json',
			success : function(data) {
				$.fn.zTree.init($("#menuTree"), setting, data);
			},
			error : function(msg) {
				console.info('树加载异常!');
			}
		});
		
		// 点击保存
		$('#save').click(function(){
			//表单校验
			let v = $("#roleForm").form("validate");
			if(v){
				//根据ztree的id获取ztree对象
				let treeObj = $.fn.zTree.getZTreeObj("menuTree");
				//获取ztree上选中的节点，返回数组对象
				let nodes = treeObj.getCheckedNodes(true);
				let array = [];
				for(let i=0;i<nodes.length;i++){
					let id = nodes[i].menu_id;
					array.push(id);
				}
				let menuIds = array.join(",");
				//为隐藏域赋值（权限的id拼接成的字符串）
				$("input[name=menuIds]").val(menuIds);
				$("#roleForm").submit();
			}
		});
	});
</script>	
</head>
<body class="easyui-layout">
		<div region="center" style="overflow:auto;padding:5px;" border="false">
			<form id="roleForm" method="post" action="RoleAction_add.action">
				<input type="hidden" name="menuIds">
				<table class="table-edit" width="80%" align="center">
					<tr class="title">
						<td colspan="2">添加角色信息</td>
					</tr>
					<tr>
						<td>名称</td>
						<td><input type="text" name="role_name" class="easyui-validatebox" data-options="required:true" /></td>
					</tr>
					<tr>
						<td>描述</td>
						<td>
							<textarea name="role_desc" rows="4" cols="60"></textarea>
						</td>
					</tr>
					<tr>
						<td>授权</td>
						<td>
							<ul id="menuTree" class="ztree"></ul>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<a id="save" icon="icon-save" href="javascript:;" class="easyui-linkbutton" plain="true" >保存</a>
						</td>
					</tr>
					</table>
			</form>
		</div>
</body>
</html>