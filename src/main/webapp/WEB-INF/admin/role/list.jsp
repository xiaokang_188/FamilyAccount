<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@taglib uri="http://shiro.apache.org/tags" prefix="shiro" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@include file="/WEB-INF/admin/common/header.jsp"%>
<link rel="stylesheet" type="text/css" href="${basePath }/css/default.css">
<title>角色列表</title>
<script type="text/javascript">
	$(function(){
		$("#tb_role").datagrid({
			columns:[[
				{title:'角色id',field:'role_id',checkbox:true},
				{title:'角色名称',field:'role_name',width:200,align:'center'},
				{title:'描述',field:'role_desc',width:200,align:'center'}
			]],
			url:'${basePath}/RoleAction_pageQuery.action',
			rownumbers:true,
			striped:true,
			pageList: [10,15,20],
			toolbar:[
				<shiro:hasPermission name="role-add">
				{text:'添加',iconCls:'icon-add',handler:addRole},
				</shiro:hasPermission>
				<shiro:hasPermission name="role-update">
				{text:'修改',iconCls:'icon-edit',handler:editRole}
				</shiro:hasPermission>
			],
			singleSelect:true,
			pagination:true,
			fit : true,
			fitColumns:true
		});
	});
	
	function addRole(){
		location.href="${basePath}/page_admin_role_add.action"
	}
	function editRole(){
		var rows = $('#tb_role').datagrid('getSelected');
		if(rows==null||rows.length==0){
			$.messager.alert("系统提示","请选择！","warning");
			return;
		}
		$.messager.confirm("提示","你确定要修改这个角色？",function(e){
			if(e){
				location.href="${basePath}/RoleAction_foredit.action?role_id="+rows.role_id;
			}
		});
		
	}
</script>
</head>
<body class="easyui-layout">
	<div region="center">
		<table id="tb_role">
		</table>
	</div>
</body>
</html>