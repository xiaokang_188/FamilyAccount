<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@include file="/WEB-INF/admin/common/header.jsp"%>
<link rel="stylesheet" type="text/css" href="${basePath }/css/default.css">
<link rel="stylesheet" href="${basePath }/js/ztree/metroStyle.css" type="text/css">
<script type="text/javascript" src="${basePath }/js/ztree/jquery.ztree.all-3.5.js"></script>
<title>角色修改</title>
<script type="text/javascript">
	$(function(){
		// 授权树初始化
		let setting = {
			data : {
				key : {
					title : "t"
				},
				simpleData : {
					enable : true,
					idKey:"menu_id"
				}
			},
			check : {
				enable : true//使用ztree的勾选效果
			}
		};
		let treeNode=[];
		$.ajax({
			url : '${basePath}/MenuAction_findByRole.action',
			type : 'post',
			data:{"role_id":"<s:property value="#role.role_id"/>"},
			async:false,
			dataType : 'json',
			success : function(data) {
				treeNode=data;
			},
			error : function(msg) {
				console.info('根据角色查询权限失败！');
			}
		});
		
		$.ajax({
			url : '${basePath}/MenuAction_authForRole.action',
			type : 'post',
			dataType : 'json',
			success : function(data) {
				$.fn.zTree.init($("#menuTree"), setting, data);
				if(treeNode.length>0){
					 let treeObj = $.fn.zTree.getZTreeObj("menuTree");
					 for(let i=0;i<treeNode.length;i++){
						 let nodes = treeObj.getNodesByParam("menu_id", treeNode[i].menu_id, null);
						 treeObj.checkNode(nodes[0],true,true);
					 }
				}
			},
			error : function(msg) {
				alert('树加载异常!');
			}
		});
		// 点击保存
		$('#edit').click(function(){
			//表单校验
			let v = $("#roleForm").form("validate");
			if(v){
				//根据ztree的id获取ztree对象
				let treeObj = $.fn.zTree.getZTreeObj("menuTree");
				//获取ztree上选中的节点，返回数组对象
				let nodes = treeObj.getCheckedNodes(true);
				let array = [];
				for(let i=0;i<nodes.length;i++){
					let id = nodes[i].menu_id;
					array.push(id);
				}
				let menuIds = array.join(",");
				//为隐藏域赋值（权限的id拼接成的字符串）
				$("input[name=menuIds]").val(menuIds);
				$("#roleForm").submit();
			}
		});
	});
</script>	
</head>
<body class="easyui-layout">
		<div region="center" style="overflow:auto;padding:5px;" border="false">
			<form id="roleForm" method="post" action="RoleAction_update.action">
			    <input type="hidden" name="role_id" value='<s:property value="#role.role_id"/>'>
				<input type="hidden" name="menuIds">
				<table class="table-edit" width="80%" align="center">
					<tr class="title">
						<td colspan="2" align="center" style="font-size: 18px">修改角色信息</td>
					</tr>
					<tr>
						<td>名称</td>
						<td><input type="text" name="role_name" class="easyui-validatebox" data-options="required:true" value="<s:property value="#role.role_name"/>"/></td>
					</tr>
					<tr>
						<td>描述</td>
						<td>
							<textarea name="role_desc" rows="4" cols="60"><s:property value="#role.role_desc"/></textarea>
						</td>
					</tr>
					<tr>
						<td>授权</td>
						<td>
							<ul id="menuTree" class="ztree"></ul>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<a id="edit" icon="icon-save" href="javascript:;" class="easyui-linkbutton" plain="true" >保存</a>
						</td>
					</tr>
					</table>
			</form>
		</div>
</body>
</html>