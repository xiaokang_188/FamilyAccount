<%
	pageContext.setAttribute("basePath", request.getContextPath());
%>
<link rel="icon" href="${basePath}/img/xiaokang.png" type="image/x-icon" />
<link id="easyuiTheme" rel="stylesheet" type="text/css" href="${basePath }/js/easyui/themes/bootstrap/easyui.css">
<link rel="stylesheet" type="text/css" href="${basePath }/js/easyui/themes/icon.css">
<script type="text/javascript" src="${basePath }/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${basePath }/js/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${basePath }/js/easyui/locale/easyui-lang-zh_CN.js"></script>