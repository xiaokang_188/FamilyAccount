<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@include file="/WEB-INF/admin/common/header.jsp"%>
<link rel="stylesheet" href="${basePath }/js/ztree/metroStyle.css" type="text/css">
<script type="text/javascript" src="${basePath }/js/ztree/jquery.ztree.all-3.5.js"></script>
<title>家庭记账管理系统后台界面</title>
<script type="text/javascript">
	$(()=>{  
		//提示框
		$.messager.show({
			title:'系统提示',
			msg:'欢迎您管理员【<s:property value="#session.currentManager.manager_name"/>】',
			timeout:5000,
			showType:'slide'
		});
	});
	//动态添加选项卡
	function zTreeOnClick(event,treeId,treeNode,clickFlag){
			if(treeNode.page!=undefined&&treeNode.page!=null&&treeNode.page!=''){
				let content='<iframe frameborder="0" height="100%" width="100%" src="${basePath}/'+treeNode.page+'"></iframe>';
				if($("#tt").tabs("exists",treeNode.name)){
					$("#tt").tabs("select",treeNode.name);
				}else{
					$("#tt").tabs("add",{
						title:treeNode.name,
						content:content,
						closable:true
					})
				}
			}
	}
	//打开修改密码窗口
	function openEditPwdWindow(){
		$("#editPwdWindow").window('open');
	}
	//提交修改密码
	function editPwd(){
		let v=$("#editPasswordForm").form("validate");
		if(v){
			let v1=$("#txtNewPass").val();
			let v2=$("#txtRePass").val();
			if(v1===v2){
				window.location.href="${basePath}/ManagerAction_editPwd.action?manager_pwd="+v1;
			}else{
				$.messager.alert("提示信息","两次密码输入不一致","warning");
			}
		}
	}
	//取消修改密码
	function closeEditPwdWindow(){
		$("#editPwdWindow").window('close');
	}
	//更换皮肤
	function changeTheme(themeName) {
		let $easyuiTheme = $('#easyuiTheme');
		let url = $easyuiTheme.attr('href');
		let href = url.substring(0, url.indexOf('themes')) + 'themes/'
				+ themeName + '/easyui.css';
		$easyuiTheme.attr('href', href);
		let $iframe = $('iframe');
		if ($iframe.length > 0) {
			for ( let i = 0; i < $iframe.length; i++) {
				let ifr = $iframe[i];
				$(ifr).contents().find('#easyuiTheme').attr('href', href);
			}
		}
	};
	
	//退出系统事件
	function exit(){
		$.messager.confirm('确认提示框','确定要退出系统吗?',function(r){ 
			if (r){ 
				window.location.href="${basePath}/ManagerAction_logout.action";
			} 
			});
	}
</script>
</head>
<body class="easyui-layout"> 
	<div data-options="region:'north',split:true" style="height:70px">
           <div style="float: left;">
			   <img src="${basePath }/img/logo.png">
           </div>
           <div style="float: right;line-height: 60px;">
			   欢迎您,管理员<strong>【<s:property value="#session.currentManager.manager_name"/>】</strong>
			<a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#pf',iconCls:'icon-reload'">更换皮肤</a> 
			<a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm',iconCls:'icon-help'">控制面板</a> 
			<div id="mm" style="width:150px;"> 
				<div data-options="iconCls:'icon-edit'" onclick="openEditPwdWindow()">修改密码</div> 
				<div class="menu-sep"></div> 
				<div data-options="iconCls:'icon-cancel'" onclick="exit()">退出系统</div> 
			</div>
			<div id="pf" style="width:150px;"> 
				<div onclick="changeTheme('default');">default</div>
				<div onclick="changeTheme('gray');">gray</div>
				<div onclick="changeTheme('black');">black</div>
				<div onclick="changeTheme('bootstrap');">bootstrap</div>
				<div onclick="changeTheme('metro');">metro</div>
			</div>
           </div>
	</div> 
	<div data-options="region:'west',title:'菜单列表',split:true" style="width:195px;">
			<div class="easyui-accordion" data-options="fit:true">
				<div title="基础菜单" data-options="iconCls:'icon-basic'">
					<ul id="permissions" class="ztree"></ul>
					<script type="text/javascript">
						$(function(){
							let setting={
									data: {
										simpleData: {
											enable: true,
											idKey:"menu_id"
										}
									},
									callback:{
										onClick:zTreeOnClick
									}
							};
							let url1="${basePath}/MenuAction_findMenu.action";
							$.post(url1,function(data){
								 $.fn.zTree.init($("#permissions"),setting,data);
							},"json");
							
							let url2="${basePath}/json/system.json";
							$.post(url2,function(zNodes){
							  	 $.fn.zTree.init($("#roles"),setting,zNodes);
							},"json");
						});
					</script>
				</div>
				<div title="系统菜单" data-options="iconCls:'icon-basic'">
					<ul id="roles" class="ztree"></ul>
				</div>
			</div>
	</div> 
	<div data-options="region:'center',split:true" style="background:#eee;">
		<div id="tt" class="easyui-tabs" data-options="fit:true">
			<div title="首页" data-options="iconCls:'icon-home'" align="center">
				<div style="padding-top: 120px;color:red;">
					<h1>欢迎使用家庭记账后台管理系统</h1>
					<h3>welcome！</h3>
					<p>开发人员：【小康】</p>
				  	<p>开发周期：2020/01/27 --- 2020/02/28</p>
				  	<hr />
				  	<p>系统环境：Windows 10</p>
					<p>开发工具：Eclipse IDE</p>
					<p>Java版本：JDK 1.8</p>
					<p>服务器：tomcat 9.0</p>
					<p>数据库：MySQL 5.7</p>
					<p>系统采用技术： Spring+Struts2+Hibernate+Shiro+Solr+Bootstrap+EasyUI+jQuery+Ajax+面向接口编程</p>
				</div>
			</div>
		</div>
	</div> 
	<div data-options="region:'south',split:true" style="inline-height:40px;">
		<center>
			家庭记账管理系统后台
		</center>
	</div> 
	<!-- 弹出窗口==修改密码 -->
	<div id="editPwdWindow" class="easyui-window" title="修改密码" collapsible="false" minimizable="false" modal="true" closed="true" resizable="false"
        maximizable="false" icon="icon-save"  style="width: 300px; height: 160px; padding: 5px;
        background: #fafafa">
        <div class="easyui-layout" fit="true">
            <div region="center" border="false" style="padding: 10px; background: #fff; border: 1px solid #ccc;">
            <form method="post" id="editPasswordForm">
                <table cellpadding=3>
                    <tr>
                        <td>新密码：</td>
                        <td><input id="txtNewPass" name="manager_pwd" type="password" data-options="validType:'length[6,12]'" class="txt01 easyui-validatebox" required="true"/></td>
                    </tr>
                    <tr>
                        <td>确认密码：</td>
                        <td><input id="txtRePass" name="okpwd" type="password" data-options="validType:'length[6,12]'" class="txt01 easyui-validatebox" required="true"/></td>
                    </tr>
                </table>
            </div>
            </form>
            <div region="south" border="false" style="text-align: right; height: 30px; line-height: 30px;">
                <a onclick="editPwd()" class="easyui-linkbutton" icon="icon-ok" href="javascript:void(0)" >确定</a> 
                <a onclick="closeEditPwdWindow()" class="easyui-linkbutton" icon="icon-cancel" href="javascript:void(0)">取消</a>
            </div>
	    </div>
	</div>
</body> 
</html>