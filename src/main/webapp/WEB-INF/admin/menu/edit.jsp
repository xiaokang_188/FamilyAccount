<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@include file="/WEB-INF/admin/common/header.jsp"%>
<title>修改权限</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/css/default.css">	
<script type="text/javascript">
	$(function(){
		// 点击保存
		$('#edit').click(function(){
			//表单校验
			var v = $("#menuForm").form("validate");
			if(v){
				$("#menuForm").submit();
			}
		});
	});
</script>	
</head>
<body class="easyui-layout">
<div data-options="region:'center'">
	<form id="menuForm" method="post" action="MenuAction_update.action">
				<table class="table-edit" width="80%" align="center">
					<tr class="title">
						<td colspan="2">功能权限信息</td>
					</tr>
					<tr>
						<td width="200">关键字</td>
						<td>
							<input type="hidden" name="menu_id" value="<s:property value="#model.menu_id"/>"/>
							<input type="text" name="menu_code" class="easyui-validatebox" data-options="required:true" value="<s:property value="#model.menu_code"/>"/>						
						</td>
					</tr>
					<tr>
						<td>名称</td>
						<td><input type="text" name="menu_name" class="easyui-validatebox" data-options="required:true" value="<s:property value="#model.menu_name"/>"/></td>
					</tr>
					<tr>
						<td>访问路径</td>
						<td><input type="text" name="menu_page"  value="<s:property value="#model.menu_page"/>"/></td>
					</tr>
					<tr>
						<td>是否生成菜单</td>
						<td>
							<script type="text/javascript">
								$(function(){
									$("#menu").combobox('select',"<s:property value="#model.generatemenu"/>")
								})
							</script>
							<select name="generatemenu" data-options="panelHeight:'auto'" class="easyui-combobox" id="menu">
								<option value="0">不生成</option>
								<option value="1">生成</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>优先级</td>
						<td>
							<input type="text" name="zindex" class="easyui-numberbox" data-options="required:true" value="<s:property value="#model.zindex"/>" />
						</td>
					</tr>
					<tr>
						<td>父功能点</td>
						<td>
							<s:property value="#model.parentMenu.menu_name"/><input class="easyui-combotree" name="parentMenu.menu_id" style="width:170px;" data-options="url:'MenuAction_listAjax.action'" id="ct">
						</td>
					</tr>
					<tr>
						<td>描述</td>
						<td>
							<textarea name="menu_desc" rows="4" cols="60"><s:property value="#model.menu_desc"/></textarea>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<a id="edit" icon="icon-save" href="javascript:;" class="easyui-linkbutton" plain="true" >保存</a>
						</td>
					</tr>
					</table>
			</form>
</div>
</body>
</html>