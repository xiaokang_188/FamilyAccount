<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@include file="/WEB-INF/admin/common/header.jsp"%>
<title>添加权限</title>
<link rel="stylesheet" type="text/css" href="${basePath }/js/easyui/ext/portal.css">
<link rel="stylesheet" type="text/css" href="${basePath }/css/default.css">	
<script type="text/javascript" src="${basePath }/js/easyui/ext/jquery.cookie.js"></script>
<script type="text/javascript">
	$(function(){
		// 点击保存
		$('#save').click(function(){
			//表单校验
			var v = $("#menuForm").form("validate");
			if(v){
				$("#menuForm").submit();
			}
		});
	});
</script>	
</head>
<body class="easyui-layout">
<div data-options="region:'center'">
	<form id="menuForm" method="post" action="MenuAction_add.action">
				<table class="table-edit" width="80%" align="center">
					<tr class="title">
						<td colspan="2">功能权限信息</td>
					</tr>
					<tr>
						<td width="200">关键字</td>
						<td>
							<input type="text" name="menu_code" class="easyui-validatebox" data-options="required:true" />						
						</td>
					</tr>
					<tr>
						<td>名称</td>
						<td><input type="text" name="menu_name" class="easyui-validatebox" data-options="required:true" /></td>
					</tr>
					<tr>
						<td>访问路径</td>
						<td><input type="text" name="menu_page"/></td>
					</tr>
					<tr>
						<td>是否生成菜单</td>
						<td>
							<select name="generatemenu" data-options="panelHeight:'auto'" class="easyui-combobox">
								<option value="0">不生成</option>
								<option value="1">生成</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>优先级</td>
						<td>
							<input type="text" name="zindex" class="easyui-numberbox" data-options="required:true" />
						</td>
					</tr>
					<tr>
						<td>父功能点</td>
						<td>
							 <input class="easyui-combotree" name="parentMenu.menu_id"
							 	data-options="url:'MenuAction_listAjax.action',panelHeight:'auto'" 
							 	style="width:170px;">
						</td>
					</tr>
					<tr>
						<td>描述</td>
						<td>
							<textarea name="menu_desc" rows="4" cols="60"></textarea>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<a id="save" icon="icon-save" href="javascript:;" class="easyui-linkbutton" plain="true" >保存</a>
						</td>
					</tr>
					</table>
			</form>
</div>
</body>
</html>