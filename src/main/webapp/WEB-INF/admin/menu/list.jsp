<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@taglib uri="http://shiro.apache.org/tags" prefix="shiro" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@include file="/WEB-INF/admin/common/header.jsp"%>
<link rel="stylesheet" type="text/css" href="${basePath }/css/default.css">
<title>权限列表</title>
<script type="text/javascript">
	$(function(){
		$("#grid").datagrid({
			toolbar : [
				<shiro:hasPermission name="menu-add">
				{
					id : 'add',
					text : '添加权限',
					iconCls : 'icon-add',
					handler : function(){
						location.href='${basePath}/page_admin_menu_add.action';
					}
				},
				</shiro:hasPermission>
				<shiro:hasPermission name="menu-update">
				{
					id : 'update',
					text : '修改权限',
					iconCls : 'icon-edit',
					handler : function(){
						  let rows =$("#grid").datagrid("getSelected");
						  if(rows==null||rows.length==0){
							  $.messager.alert("提示","你还没有选择！","warning");
							  return ;
						  }
						  $.messager.confirm("提示","你确定要修改吗？",function(e){
								if(e){
									location.href="MenuAction_foredit.action?menu_id="+rows.menu_id;
								}
							});
					}
				}
				</shiro:hasPermission>
			],
			url : '${basePath}/MenuAction_pageQuery.action',
			pagination : true,
			singleSelect:true,
			fit:true,
			fitColumns:true,
			columns : [[
			  {
				  field : 'menu_id',
				  title : '编号',
				  width : 200,
				  checkbox:true
			  },
			  {
				  field : 'menu_name',
				  title : '名称',
				  width : 200
			  },  
			  {
				  field : 'menu_desc',
				  title : '描述',
				  width : 200
			  },  
			  {
				  field : 'generatemenu',
				  title : '是否生成菜单',
				  width : 150,
				  formatter : function(data,row, index){
						if(data=="1"){
							return "是";
						}else{
							return "否";
						}
					}
			  },  
			  {
				  field : 'zindex',
				  title : '优先级',
				  width : 200
			  },  
			  {
				  field : 'menu_page',
				  title : '路径',
				  width : 240
			  }
			]]
		});
	});
</script>	
</head>
<body class="easyui-layout">
	<div data-options="region:'center'">
		<table id="grid"></table>
	</div>
</body>
</html>