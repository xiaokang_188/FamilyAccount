<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,user-scalable=no">
<title>添加收入类型</title>
<style>
label.error {
	font-size: 14px;
	color: red;
}
</style>
</head>
<body>
	<div class="container" style="padding: 100px;" align="center">
		<form id="add_form" action="${basePath }/RegionAction_add.action"
			method="post">
			<div class="row">
				<div class="col-md-offset-3 col-md-5"
					style="padding: 10px;"
					align="center">
					<div class="form-group form-inline">
						<label class="control-label">大区名称:</label> <input
							style="width: 220px;" required="required" type="text"
							class="form-control" name="region_name" id="region_name"
							style="width: 220px;" placeholder="请输入大区名称" />
					</div>
					<div class="form-group form-inline" style="padding-left: 20px;">
						<label class="control-label">描述:</label>
						<textarea style="width: 220px;" class="form-control"
							name="region_desc" placeholder="请填写大区描述"></textarea>
					</div>
					<input type="reset" value="重置" class="btn btn-warning" /> <input
						type="submit" value="提交添加" class="btn btn-danger" />
				</div>
			</div>
		</form>
	</div>
</body>
</html>