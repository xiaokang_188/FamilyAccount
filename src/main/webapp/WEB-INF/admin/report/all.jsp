<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="${basePath}/js/Highcharts8/code/highcharts.js"></script>
<script src="${basePath}/js/Highcharts8/code/themes/grid-light.js"></script>
<script src="${basePath}/js/Highcharts8/code/highcharts-more.js"></script>
<script src="${basePath}/js/Highcharts8/code/modules/exporting.js"></script>
<script src="${basePath}/js/Highcharts8/code/modules/data.js"></script>
<script src="${basePath}/js/Highcharts8/code/highcharts-zh_CN.js"></script>
<title>数据条目可视化报表</title>
</head>
<body>
	<div id="container"></div>
	<div id="wrapper">
		<button id="plain">普通</button>
		<button id="inverted">反转</button>
		<button id="polar">极地图</button>
	</div>
	<!-- ${intotal }-${outtotal }-${investtotal }-${loantotal } -->
	<script>
		$(()=>{ 
			let chart = Highcharts.chart('container', {
				loading: {
					labelStyle: {
						fontStyle: 'italic'
					},
					hideDuration: 3000,
					showDuration: 3000
				},
				credits: {
					text: '家庭记账管理系统-Powered By xiaokang',// 显示的文字
					href: 'http://www.xiaokang.cool' // 链接地址
				},
				title: {
					text: '数据条目可视化报表'
				},
				subtitle: {
					text: '普通的'
				},
				yAxis: {
                    allowDecimals: false,
                    title: {
                        text: '数据/条'
                    }
                },
				xAxis: {
					categories: ['收入账单', '支出账单', '投资理财', '借款还贷']
				},
				series: [{
					type: 'column',
					colorByPoint: true,
					data: [${intotal}, ${outtotal}, ${investtotal}, ${loantotal}],
					showInLegend: false
				}]
			});
			// 给 wrapper 添加点击事件
			Highcharts.addEvent(document.getElementById('wrapper'), 'click', function(e) {
				let target = e.target,
					button = null;
				if(target.tagName === 'BUTTON') { // 判断点的是否是 button
					button = target.id;
					switch(button) {
						case 'plain': 
							chart.update({
								chart: {
									inverted: false,
									polar: false
								},
								subtitle: {
									text: '普通的'
								}
							});
							break;
						case 'inverted': 
							chart.update({
								chart: {
									inverted: true,
									polar: false
								},
								subtitle: {
									text: '反转'
								}
							}); 
							break;
						case 'polar': 
							chart.update({
								chart: {
									inverted: false,
									polar: true
								},
								subtitle: {
									text: '极地图'
								}
							});
							break;
					}
				}
			});
		});
	</script>
</body>
</html>