<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@taglib uri="http://shiro.apache.org/tags" prefix="shiro" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@include file="/WEB-INF/admin/common/header.jsp"%>
<link rel="stylesheet" type="text/css" href="${basePath }/css/default.css">
<title>Solr索引库维护</title>
<script type="text/javascript">
	$(function(){
		$("#tb_topic").datagrid({
			columns:[[
				{title:'索引编号',field:'topic_id',checkbox:true},
				{title:'标题',field:'topic_title',width:200,align:'center'},
				{title:'创建时间',field:'topic_datetime_s',width:200,align:'center'},
				{title:'solr索引是否删除',field:'solrDel',width:200,align:'center',formatter:function(v){
					if(v=='1'){
						return "<span style='color:red;'>是</span>";
					}else{
						return "否";
					}
				}}
			]],
			url:'${basePath}/SolrAction_pageQuery.action',
			rownumbers:true,
			striped:true,
			pageList: [10,15,20],
			toolbar:[
				<shiro:hasPermission name="solr-add">
				{text:'一键导入索引库',iconCls:'icon-add',handler:addtopic},
				</shiro:hasPermission>
				<shiro:hasPermission name="solr-delete">
				 {text:'删除索引',iconCls:'icon-remove',handler:deletetopic}
				</shiro:hasPermission>
			],
			pagination:true,
			fit : true,
			fitColumns:true
		});
	});
	
	function addtopic(){
		$.post("${basePath}/SolrAction_importIndex.action",{},function(data){
			if(data===1){
				$.messager.alert("提示信息","导入索引库成功！","info");
			}else{
				$.messager.alert("提示信息","导入索引库失败！","warning");
			}
		},"json");
	}
	function deletetopic(){
		let rows = $('#tb_topic').datagrid('getSelections');
		if(rows==null||rows.length==0){
			$.messager.alert("系统提示","请选择！","warning");
			return;
		}
		$.messager.confirm("提示","你确定要删除这"+rows.length+"条主题帖？",function(e){
			if(e){
				let arr=new Array();
				for(let i=0;i<rows.length;i++){
					arr.push(rows[i].topic_id);
				}
				let topic_ids=arr.join(",");
				$.post("${basePath}/SolrAction_delete.action",{"topic_ids":topic_ids},function(data){
					//console.info(data);
					if(data===1){
						$.messager.alert("提示信息","删除索引成功！","info");
						//重新加载当前页
						$("#tb_topic").datagrid("reload");
					}else{
						$.messager.alert("提示信息","删除索引失败！","warning");
					}
				},"json");
			}
		});
	}
</script>
</head>
<body class="easyui-layout">
	<div region="center">
		<table id="tb_topic">
		</table>
	</div>
</body>
</html>