<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
		<title>用户注册界面</title>
		<link href="${basePath}/css/findpwd.css" />
		<script src="${basePath}/js/jquery.validate.min.js" type="text/javascript"></script>
		<style>
			label.error{
				font-size:14px;
				color: red;
			}
		</style>
		<script type="text/javascript">
		//自定义手机号格式校验规则
		$.validator.addMethod(
			"regexp_phone",
			function(value,element){
				let flag = false;
				let length = value.length;  
				//中国手机号正则
				let mobile = /^(?:(?:\+|00)86)?1\d{10}$/;   
				if(length == 11 && mobile.test(value)){
					flag=true;
				}
				return flag;
			}
		);
		//自定义邮箱格式校验规则
		$.validator.addMethod(
			"regexp_email",
			function(value,element){
				let flag = false;
				//邮箱地址(email)正则
				let email = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;   
				if(email.test(value)){
					flag=true;
				}
				return flag;
			}
		);
		//自定义用户名格式校验规则
		$.validator.addMethod(
			"regexp_username",
			function(value,element){
				let flag = false;
				//用户名正则
				let user_name = /^[a-zA-Z][-_a-zA-Z0-9]{5,19}$/;   
				if(user_name.test(value)){
					flag=true;
				}
				return flag;
			}
		);
$(function(){
		$("#regist_form").validate({
			rules:{
				"user_name":{
					"required":true,
					"regexp_username":true,
					"remote":{
						url:"${basePath}/UserAction_checkName.action",
						type:"post",
						dataType:"json",
						data:{
							user_name: function () {
		                         return $("#user_name").val();//这个是取要验证的数据
		                     }
						},
						dataFilter: function(data, type) {
								 let jsonData=JSON.parse(data);
								 let result=jsonData.isExist;
		                         return !result;
						}
					}
				},
				"user_password":{
					"required":true,
					"rangelength":[6,12]
				},
				"repassword":{
					"required":true,
					"rangelength":[6,12],
					"equalTo":"[name='user_password']"
				},
				"email":{
					"required":true,
					"regexp_email":true,
					"remote":{
						url:"${basePath}/UserAction_checkEmail.action",
						type:"post",
						dataType:"json",
						data:{
							email: function () {
		                         return $("#email").val();//这个是取要验证的数据
		                     }
						},
						dataFilter: function(data, type) {
								 let jsonData=JSON.parse(data);
								 let result=jsonData.isExist;
		                         return !result;
						}
					}
				},
				"user_phone":{
					"required":true,
					"regexp_phone":true
				},
				"checkcode":{
					"required":true,
					"remote":{
						url:"${basePath}/UserAction_checkCode.action",
						type:"post",
						dataType:"json",
						data:{
							checkcode: function () {
		                         return $("#checkcode").val();//这个是取要验证的数据
		                     }
						},
						dataFilter: function(data, type) {
								 let jsonData=JSON.parse(data);
								 let result=jsonData.isExist;
		                         return result;
						}
					}
				}
			},
			messages:{
				"user_name":{
					"required":"用户名不能为空",
					"regexp_username":"用户名6至20位，以字母开头，可包含字母、数字、减号、下划线",
					"remote":"用户名已经存在"
				},
				"user_password":{
					"required":"密码不能为空",
					"rangelength":"密码长度6-12位"
				},
				"repassword":{
					"required":"确认密码不能为空",
					"rangelength":"密码长度6-12位",
					"equalTo":"两次密码不一致"
				},
				"email":{
					"required":"邮箱不能为空",
					"regexp_email":"邮箱格式不正确",
					"remote":"该邮箱已经注册过账号了！"
				},
				"user_phone":{
					"required":"手机号不能为空",
					"regexp_phone":"请输入合法手机号"
				},
				"checkcode":{
					"required":"验证码不能为空",
					"remote":"验证码输入有误"
				}
			}
		});
});
</script>
	</head>
	<body background="${basePath}/img/bg.jpg">
		<!--container主容器1-->
		<div class="container">
			<div class="page-header">
					<ol class="list-inline" style="margin-bottom:-10px;">
						<li>
							<h3>家庭记账管理系统 <small style="color: orangered;">用户注册</small></h3>
						</li>
						<li>
							<p>我已注册，现在就<a class="btn btn-default" href="${basePath}/login.jsp" style="font-weight: bolder;">登录</a></p>
						</li>
					</ol>
			</div>
		</div>
		<!--container主容器1end-->
		<!--container主容器2-->
		<div class="container">
			<!--row-->
			<div class="row">
				<div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6" style="margin-top: 10px;font-size: 12px;">
					<div class="row" align="center">
						<form id="regist_form" class="form-horizontal" name="registerForm" action="${basePath }/UserAction_regist.action" method="post">
							<div class="form-group">
								<label class="control-label col-md-4 col-sm-4 hidden-xs">用户名</label>
								<div class="col-md-5 col-sm-5">
									<input type="text" id="user_name" name="user_name" class="form-control" placeholder="请设置用户名（用于登录）"/>
								</div>
								<div class="col-md-3 col-sm-3" style="margin-left:-50px;margin-top: 10px;">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4 col-sm-4 hidden-xs">邮箱地址</label>
								<div class="col-md-5 col-sm-5">
									<input type="text" id="email" name="email" class="form-control" placeholder="请输入邮箱地址可用于找回密码"/>
								</div>
								<div class="col-md-3 col-sm-3" style="margin-left:-50px;margin-top: 10px;">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4 col-sm-4 hidden-xs">手机号码</label>
								<div class="col-md-5 col-sm-5">
									<input type="text" name="user_phone"   id="number" class="form-control" placeholder="手机号"/>
								</div>
								<div class="col-md-3 col-sm-3" style="margin-left:-50px;margin-top: 10px;">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4 col-sm-4 hidden-xs">密码</label>
								<div class="col-md-5 col-sm-5" >
									<input type="password" id="password"  required="required" id="password" name="user_password" class="form-control" placeholder="请设置登录密码"/>
								</div>
								<div class="col-md-3 col-sm-3" style="margin-left:-50px;margin-top: 10px;">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4 col-sm-4 hidden-xs">确认密码</label>
								<div class="col-md-5 col-sm-5">
									<input type="password" name="repassword"  class="form-control" placeholder="再输入一次密码"/>
								</div>
								<div class="col-md-3 col-sm-3" style="margin-left:-50px;margin-top: 10px;">
								</div>
							</div>
							<div class="form-group">
								<label for="checkcode" class="col-md-offset-1 col-md-3 col-sm-3 hidden-xs control-label">验证码</label>
								<div class="col-sm-5 col-md-5 col-lg-5 col-xs-6">
									<input type="text" id="checkcode" name="checkcode" class="form-control"  placeholder="请输入验证码" />
								</div>
								<img id="loginform:vCode" src="${basePath }/verificationcode.jsp"
								onclick="javascript:document.getElementById('loginform:vCode').src='${basePath }/verificationcode.jsp?'+new Date().getTime();" />
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-4 col-sm-4 hidden-xs">身份</label>
								<div class="col-md-5 col-sm-5">
									<select class="form-control form-inline" name="level" id="selectLevel" style="background-color:#349BFF;color:white;font-weight: bolder;">
										<option value="0">普通成员</option>
										<option value="1">家庭户主</option>
									</select>
								</div>
								<div class="col-md-3 col-sm-3" style="margin-left:-50px;margin-top: 10px;">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-offset-4 col-md-2 col-sm-offset-4 col-sm-2">
									<input type="submit" class="btn btn-danger" value="提交注册" />
								</div>
								<div class="col-md-5 col-sm-5">
									<input type="reset" class="btn btn-warning" value="重置表单" />
								</div>
							</div>
						</form>
					</div>
					<!--内部row-->
				</div>
				<!--rowDIV END-->
			</div>
			<!--row END-->
		</div>
		<!--container2 END-->
	</body>
</html>