<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Refresh" content="1;url=./index.jsp"/>
<title>正在跳转到主界面....</title>
</head>
<body style="height:100%;border: 0 none;line-height: 1;text-align:center;background-color:#F9F9F9;">
    <img alt="页面跳转" src="${pageContext.request.contextPath }/img/loading.gif">
    <h3>正在进入系统主界面，请稍后...</h3>
</body>
</html>