<%@taglib uri="/struts-tags" prefix="s" %>
<%
	pageContext.setAttribute("basePath", request.getContextPath());
%>
<link rel="icon" href="${basePath}/img/xiaokang.png" type="image/x-icon" />
<link href="${basePath}/css/bootstrap.css" rel="stylesheet">
<script src="${basePath}/js/jquery-1.11.3.min.js"></script>
<script src="${basePath}/js/bootstrap.min.js"></script>
<script src="${basePath}/js/track.js"></script>