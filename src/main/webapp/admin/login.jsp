<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="/common/header.jsp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
		<title>后台登录界面</title>
		<link href="${basePath}/css/admin.css" rel="stylesheet" />
		<script type="text/javascript">
			if (window.self != window.top) {
				window.top.location = window.location;
			}
		</script>
	</head>
	<body>
		<div class="container">
			<video autoplay loop muted>
				<source src="${basePath }/js/video/Ipad.mp4" type="video/mp4"/>
			</video>
			<div class="row" style="height:120px;"></div>
			<div class="row">
				<!--ROWdiv-->
				<div class="col-md-offset-4 col-sm-offset-4 col-sm-4 col-md-4">
					<!--表单row-->
					<div class="row" id="login">
						<form class="form-horizontal" method="post" action="${basePath }/ManagerAction_login.action">
							<div class="form-group">
								<div class="col-md-12 col-sm-12 hidden-xs" id="header">
									<h2>管理员登录界面</h2>
								</div>
							</div>
							<div class="form-group">
								<label id="manager_name" for="user_name" class="col-lg-3 col-md-3 col-sm-3 hidden-xs control-label">账&nbsp;&nbsp;&nbsp;&nbsp;号</label>
								<div class="col-md-7 col-sm-7">
									<input type="text" name="manager_name" required="required" class="form-control" placeholder="请输入账号" />
								</div>
							</div>
							<div class="form-group">
								<label id="manager_pwd" for="user_password" class="col-lg-3 col-md-3 col-sm-3 hidden-xs control-label">密&nbsp;&nbsp;&nbsp;&nbsp;码</label>
								<div class="col-sm-7">
									<input type="password" required="required" name="manager_pwd" class="form-control" placeholder="请输入密码" />
								</div>
							</div>
							<div class="form-group">
								<label id="checkcode" for="checkCode" class="col-md-3 col-sm-3 hidden-xs control-label">验证码</label>
								<div class="col-sm-5 col-md-5 col-lg-5 col-xs-6">
									<input type="text" name="checkcode" class="form-control" placeholder="请输入验证码" />
								</div>
									<img id="loginform:vCode" src="${basePath }/verificationcode.jsp"
								onclick="javascript:document.getElementById('loginform:vCode').src='${basePath }/verificationcode.jsp?'+new Date().getTime();" />
							</div>
							<div class="form-group">
								<div class="col-sm-10 col-sm-offset-1">
									<input type="submit" value="登录" name="submit" class="btn btn-info btn-block">
								</div>
							</div>
							<div align="center">
								<b style="color: red;font-size: 15px;list-style-type: none;">
									<s:actionerror/>
								</b>
							</div>
						</form>
					</div><!--表单rowend-->
				</div>
			</div>
		</div>
	</body>
</html>