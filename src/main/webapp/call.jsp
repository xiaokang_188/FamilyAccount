<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>联系管理员</title>
	<!--框架样式-->
	<link href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css" rel="stylesheet">
	<!--主要样式-->
	<link href="${pageContext.request.contextPath}/assets/css/call.css" rel="stylesheet">
	<link rel="icon" href="${pageContext.request.contextPath}/img/xiaokang.png" type="image/x-icon" />
</head>
<body>
<div class="modal-box">
	<button type="button" class="btn show-modal" data-toggle="modal" data-target="#myModal">查看详情</button>
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div class="modal-body">
					<div class="icon"> ! </div>
					<h3 class="title">您的登录受限！</h3>
					<p class="description">无权限访问</p>
					<a href="http://wpa.qq.com/msgrd?v=3&uin=1181259634&site=qq&menu=yes" class="subscribe">联系管理员</a>
				</div>
			</div>
		</div>
	</div>
	<div align="center" style="margin-top: 72px;">
			<strong>您来到了非法区域</strong><br/>
			<div align="center" style="margin-top: 11px;">
				<img alt="微信公众号：小康新鲜事儿" title="微信公众号：小康新鲜事儿" src="${pageContext.request.contextPath}/img/gzh.jpg">
			</div>
		</div>
</div>
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</body>
</html>