﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.Random"%>
<%@ page import="java.io.OutputStream"%>
<%@ page import="java.awt.*"%>
<%@ page import="java.awt.image.BufferedImage"%>
<%@ page import="javax.imageio.ImageIO"%>
<%
	//定义验证码图片的宽和高
	int width = 88;
	int height = 32;
	//创建一个图片缓冲区，作为画布
	BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
	//获取这个“画布”上画图的“画笔”
	Graphics2D g = (Graphics2D) image.getGraphics();
	//设置画笔的颜色背景色
	g.setColor(new Color(0xFAEBD7));
	//使用画笔绘制画布的背景
	g.fillRect(0, 0, width, height);
	//修改画笔颜色为黑色
	g.setColor(Color.black);
	//绘制画布边框
	g.drawRect(0, 0, width - 1, height - 1);
	//创建一个随机数生成对象
	Random r = new Random();
	//定义一个字符数组，作为验证码字符源
	char[] chars = "0123456789QWERTYUIOPASDFGHJKLZXCVBNMqwertyuipasdfghjkzxcvbnm".toCharArray();
	// 在图片上生成干扰点
	for (int i = 0; i < 300; i++) {
		g.setColor(new Color(r.nextInt(150) + 50, r.nextInt(150) + 50, r.nextInt(150) + 50));
		int x = r.nextInt(width);
		int y = r.nextInt(height);
		g.drawOval(x, y, 0, 0);
	}
	//创建一个字符串缓冲区，用来保存要输出到图片中的所有字符
	StringBuilder sb = new StringBuilder();
	//循环往图片中写入字符
	for (int i = 0, left = 8; i < 4; i++, left += 21) {
		//从字符源中随机获取一个字符
		char ch = chars[r.nextInt(chars.length)];
		//将字符添加到字符串缓冲区中
		sb.append(ch);
		//设置画笔颜色
		g.setColor(new Color(r.nextInt(100), r.nextInt(100), r.nextInt(100)));
		//设置字符的字体
		g.setFont(new Font("Candara", Font.BOLD, 24));
		//设置一个弧度
		double theta = Math.PI / 180 * (45 - r.nextInt(75));
		//旋转画笔输出的方向，旋转弧度为上面获取的弧度
		g.rotate(theta, left, 24);
		//往画布中输出字符
		g.drawString(ch + "", left, 24);
		//将画笔的输出方向调整回来
		g.rotate(-theta, left, 24);
	}
	//释放画笔资源
	g.dispose();
	//将输出到画布中的字符保存到session中
	session.setAttribute("key", sb.toString());
	//设置相应流的响应内容格式
	response.setContentType("image/jpeg");
	//因为jsp页面执行完毕后会释放所有PageContestObject对象，并调用response的getWriter方法，
	//而response对象不能同时使用getWriter方法和getOutputStream()方法，
	//为了下面能够使用response对象的getOutputStream()方法获取输出字节流，往浏览器输出图片，
	//所以需要先对其进行处理
	out.clear();
	out = pageContext.pushBody();
	//获取返回给浏览器客户端的响应流
	OutputStream output = response.getOutputStream();
	//将画布内容往响应流中输出
	ImageIO.write(image, "jpeg", output);
	//关闭流，释放资源
	output.close();
%>