/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 5.7.28 : Database - familyaccount
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`familyaccount` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `familyaccount`;

/*Table structure for table `manager_role` */

DROP TABLE IF EXISTS `manager_role`;

CREATE TABLE `manager_role` (
  `manager_id` varchar(255) NOT NULL,
  `role_id` varchar(255) NOT NULL,
  PRIMARY KEY (`manager_id`,`role_id`),
  KEY `FKko847awa6mm1up2olqc1aa4xc` (`role_id`),
  CONSTRAINT `FKfhqknratfgv6cxcl8gi6hfndf` FOREIGN KEY (`manager_id`) REFERENCES `t_manager` (`manager_id`),
  CONSTRAINT `FKko847awa6mm1up2olqc1aa4xc` FOREIGN KEY (`role_id`) REFERENCES `t_role` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `manager_role` */

insert  into `manager_role`(`manager_id`,`role_id`) values 
('1','4028812062b975760162c393ba210000');

/*Table structure for table `role_menu` */

DROP TABLE IF EXISTS `role_menu`;

CREATE TABLE `role_menu` (
  `menu_id` varchar(255) NOT NULL,
  `role_id` varchar(255) NOT NULL,
  PRIMARY KEY (`role_id`,`menu_id`),
  KEY `FKesysc8jafaoxjq8lrt5u84ntp` (`menu_id`),
  CONSTRAINT `FK6bxd17eu322xbqt0tn69xea4t` FOREIGN KEY (`role_id`) REFERENCES `t_role` (`role_id`),
  CONSTRAINT `FKesysc8jafaoxjq8lrt5u84ntp` FOREIGN KEY (`menu_id`) REFERENCES `t_menu` (`menu_id`),
  CONSTRAINT `FKhvck87h9upyt3v9u6spwtis4v` FOREIGN KEY (`menu_id`) REFERENCES `t_menu` (`menu_id`),
  CONSTRAINT `FKt6cjqbvfp9kkg1e2piclhfe5r` FOREIGN KEY (`role_id`) REFERENCES `t_role` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `role_menu` */

insert  into `role_menu`(`menu_id`,`role_id`) values 
('4028812062b460750162b464e0f20000','4028812062b975760162c393ba210000'),
('40288a81629118a20162911b86be0000','4028812062b975760162c393ba210000'),
('40288a8162913c080162913c2b640000','4028812062b975760162c393ba210000'),
('40288a81629141dd0162914ec2550000','4028812062b975760162c393ba210000'),
('40288a81629141dd0162914f75ff0001','4028812062b975760162c393ba210000'),
('40288a81629141dd01629150cef40002','4028812062b975760162c393ba210000'),
('40288a81629141dd01629151c3a20003','4028812062b975760162c393ba210000'),
('40288a81629141dd016291534beb0004','4028812062b975760162c393ba210000'),
('40288a81629141dd0162915438380005','4028812062b975760162c393ba210000'),
('40288a81629141dd01629155385d0006','4028812062b975760162c393ba210000'),
('40288a81629141dd01629159c3150007','4028812062b975760162c393ba210000'),
('40288a81629141dd0162915a7b6d0008','4028812062b975760162c393ba210000'),
('40288a81629141dd0162915b18ae0009','4028812062b975760162c393ba210000'),
('40288a81629141dd0162915c3f4b000a','4028812062b975760162c393ba210000'),
('40288a81629141dd0162915da057000b','4028812062b975760162c393ba210000'),
('40288a81629141dd0162915e6d8a000c','4028812062b975760162c393ba210000'),
('40288a81629141dd0162915f16f9000d','4028812062b975760162c393ba210000'),
('40288a81629141dd0162915fa5fc000e','4028812062b975760162c393ba210000'),
('40288a81629141dd016291606a45000f','4028812062b975760162c393ba210000'),
('40288a81629141dd01629164ced40010','4028812062b975760162c393ba210000'),
('40288a81629141dd01629165926f0011','4028812062b975760162c393ba210000'),
('40288a81629141dd016291665d390012','4028812062b975760162c393ba210000'),
('40288a81629141dd0162916711ec0013','4028812062b975760162c393ba210000'),
('40288a81629643ba0162964467590000','4028812062b975760162c393ba210000'),
('40288a81629643ba0162964bd3fd0005','4028812062b975760162c393ba210000'),
('40288a81629643ba0162964ca2630006','4028812062b975760162c393ba210000'),
('40288a81629643ba0162964d2cb00007','4028812062b975760162c393ba210000'),
('40288a81629643ba0162964d86c90008','4028812062b975760162c393ba210000'),
('40288a81629643ba0162964e128a0009','4028812062b975760162c393ba210000'),
('40288a81629643ba0162964ed726000a','4028812062b975760162c393ba210000'),
('40288a81629643ba0162964f59bf000b','4028812062b975760162c393ba210000'),
('40288a81629643ba0162964ff3c3000c','4028812062b975760162c393ba210000'),
('40288a81629643ba01629650b505000d','4028812062b975760162c393ba210000'),
('40288a816297294a0162972aef370000','4028812062b975760162c393ba210000'),
('40288a8162972bef0162972cf5e00000','4028812062b975760162c393ba210000'),
('40288a816297478401629749042e0000','4028812062b975760162c393ba210000'),
('40288a816297508a0162975231180000','4028812062b975760162c393ba210000'),
('40288a81629759680162975b4d3c0000','4028812062b975760162c393ba210000');

/*Table structure for table `t_inaccount` */

DROP TABLE IF EXISTS `t_inaccount`;

CREATE TABLE `t_inaccount` (
  `inaccount_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `inaccount_datetime` datetime DEFAULT NULL,
  `inaccount_money` double DEFAULT NULL,
  `inaccount_createtime` datetime DEFAULT NULL,
  `inaccount_desc` varchar(255) DEFAULT NULL,
  `in_user_id` bigint(20) DEFAULT NULL,
  `in_type_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`inaccount_id`),
  KEY `FKs69btfay3q86dukfgam52bojw` (`in_user_id`),
  KEY `FKqngwk58yf76oilr9mqmb1oamu` (`in_type_id`),
  CONSTRAINT `FKqngwk58yf76oilr9mqmb1oamu` FOREIGN KEY (`in_type_id`) REFERENCES `t_inaccounttype` (`inaccounttype_id`),
  CONSTRAINT `FKs69btfay3q86dukfgam52bojw` FOREIGN KEY (`in_user_id`) REFERENCES `t_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

/*Data for the table `t_inaccount` */

insert  into `t_inaccount`(`inaccount_id`,`inaccount_datetime`,`inaccount_money`,`inaccount_createtime`,`inaccount_desc`,`in_user_id`,`in_type_id`) values 
(1,'2019-12-07 00:00:00',11,'2020-03-12 14:15:29','公众号推文，慢慢来哦！！！修改测试',2,5),
(3,'2020-02-08 00:00:00',30,'2020-02-08 14:11:34','公众号收入',2,5),
(4,'2020-02-08 00:00:00',100,'2020-02-08 14:36:51','阿里云服务器优惠推广',2,6),
(5,'2020-02-08 00:00:00',12000,'2020-02-08 19:27:54','月薪12K到手',7,4),
(6,'2020-02-08 00:00:00',2000,'2020-02-08 19:30:03','寒假辅导学生两周时间',7,8),
(7,'2020-02-08 00:00:00',3000,'2020-02-08 19:30:42','项目提成',7,9),
(8,'2020-02-07 00:00:00',10.58,'2020-02-18 17:38:22','公众号推文，慢慢来哦！！！',2,5),
(9,'2020-03-01 00:00:00',10,'2020-02-21 12:46:41','测试月总计！！！',2,6),
(10,'2020-01-30 00:00:00',15,'2020-01-30 15:30:19','一月测试',2,6),
(11,'2020-03-01 00:00:00',99,'2020-02-22 15:31:49','只要99，流量带走',2,7),
(12,'2020-05-15 00:00:00',300,'2020-02-22 18:09:32','做兼职赚的钱',2,13),
(13,'2020-02-22 00:00:00',66,'2020-02-22 18:10:07','收红包',2,12),
(14,'2020-02-18 00:00:00',99,'2020-02-22 18:10:33','报销资金',2,11),
(15,'2020-06-12 00:00:00',30,'2020-02-22 19:01:03','测试数据',2,6),
(16,'2020-11-08 00:00:00',50,'2020-03-12 17:09:01','测试数据——修改',2,5),
(17,'2020-06-06 00:00:00',18,'2020-02-23 10:15:11','流量测试',2,7),
(18,'2020-01-16 00:00:00',0.32,'2020-02-23 13:07:04','公众号收入！！！',2,5),
(19,'2020-03-12 00:00:00',11.22,'2020-03-12 17:05:45','测试公众号收入',2,5),
(20,'2020-03-12 00:00:00',24.35,'2020-03-12 17:14:11','外快测试数据',2,10),
(21,'2020-03-12 00:00:00',11.11,'2020-03-12 17:20:12','流量测试数据',2,7),
(22,NULL,11,'2020-05-02 12:47:42','',2,5);

/*Table structure for table `t_inaccounttype` */

DROP TABLE IF EXISTS `t_inaccounttype`;

CREATE TABLE `t_inaccounttype` (
  `inaccounttype_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `inaccounttype_name` varchar(255) DEFAULT NULL,
  `inaccounttype_createtime` datetime DEFAULT NULL,
  `inaccounttype_desc` varchar(255) DEFAULT NULL,
  `it_user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`inaccounttype_id`),
  KEY `FK2y808k7iqt6274okduikpth3h` (`it_user_id`),
  CONSTRAINT `FK2y808k7iqt6274okduikpth3h` FOREIGN KEY (`it_user_id`) REFERENCES `t_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

/*Data for the table `t_inaccounttype` */

insert  into `t_inaccounttype`(`inaccounttype_id`,`inaccounttype_name`,`inaccounttype_createtime`,`inaccounttype_desc`,`it_user_id`) values 
(3,'工资','2020-01-30 10:27:20','干活工资',8),
(4,'工资','2020-02-06 15:02:31','税后工资',7),
(5,'公众号','2020-02-07 20:29:05','公众号推文收入',2),
(6,'推广','2020-02-08 11:54:59','阿里云服务器推广',2),
(7,'流量','2020-02-08 11:55:20','无限流量售卖',2),
(8,'兼职','2020-02-08 19:28:57','各种小兼职',7),
(9,'奖金','2020-02-08 19:29:27','项目提成',7),
(10,'外快','2020-02-22 18:07:40','一些小外快',2),
(11,'报销','2020-02-22 18:08:17','报销资金',2),
(12,'收红包','2020-02-22 18:08:35','微信红包',2),
(13,'兼职','2020-02-22 18:08:54','做兼职赚的钱',2),
(17,'直播','2020-03-13 15:19:27','测试分类',2),
(18,'奖金','2020-03-28 10:32:17','测试收入类型',2),
(19,'奖金','2020-05-02 17:50:47','测试奖金',25);

/*Table structure for table `t_invest` */

DROP TABLE IF EXISTS `t_invest`;

CREATE TABLE `t_invest` (
  `invest_id` varchar(255) NOT NULL,
  `invest_name` varchar(255) DEFAULT NULL,
  `invest_datetime` datetime DEFAULT NULL,
  `invest_year` int(11) DEFAULT NULL,
  `invest_target` varchar(255) DEFAULT NULL,
  `invest_createtime` datetime DEFAULT NULL,
  `interest_rates` double DEFAULT NULL,
  `invest_money` double DEFAULT NULL,
  `invest_desc` varchar(255) DEFAULT NULL,
  `invest_user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`invest_id`),
  KEY `FKhc9y38xh2136tc50doqrfqjej` (`invest_user_id`),
  CONSTRAINT `FKhc9y38xh2136tc50doqrfqjej` FOREIGN KEY (`invest_user_id`) REFERENCES `t_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_invest` */

insert  into `t_invest`(`invest_id`,`invest_name`,`invest_datetime`,`invest_year`,`invest_target`,`invest_createtime`,`interest_rates`,`invest_money`,`invest_desc`,`invest_user_id`) values 
('40286f816fe6a130016fe6aae8300000','股票11','2020-01-27 00:00:00',5,'小康','2020-01-27 19:02:37',1,1000,'2020.1.27测试',7),
('40286f8170d348c10170d351101e0000','股票223','2020-03-13 00:00:00',2,'小康','2020-03-13 17:54:29',10,1024,'测试中111---222',2),
('40286f8170d348c10170d351c04a0001','股票333','2018-02-01 00:00:00',2,'小康','2020-03-13 17:55:15',15,3000,'测试666',2),
('40286f8170d348c10170d352929e0002','证券666','2020-03-13 00:00:00',3,'小康','2020-03-13 17:56:09',16,6000,'测试7777',2),
('40286f8170d35bc80170d35f48910000','混合基金777','2020-03-13 00:00:00',3,'小康','2020-03-13 18:10:01',10,5000,'测试中11111',2),
('4028812062869c320162895f32d80000','股票888','2019-12-18 00:00:00',2,'xx公司','2018-04-03 10:39:00',1,1000,'111',2);

/*Table structure for table `t_loan` */

DROP TABLE IF EXISTS `t_loan`;

CREATE TABLE `t_loan` (
  `loan_id` varchar(255) NOT NULL,
  `loan_name` varchar(255) DEFAULT NULL,
  `loan_datetime` datetime DEFAULT NULL,
  `loan_year` int(11) DEFAULT NULL,
  `interest_rates` double DEFAULT NULL,
  `loan_money` double DEFAULT NULL,
  `loan_source` varchar(255) DEFAULT NULL,
  `loan_desc` varchar(255) DEFAULT NULL,
  `loan_createtime` datetime DEFAULT NULL,
  `loan_user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`loan_id`),
  KEY `FKf2abem5rlqlxbfr60f4f89389` (`loan_user_id`),
  CONSTRAINT `FKf2abem5rlqlxbfr60f4f89389` FOREIGN KEY (`loan_user_id`) REFERENCES `t_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_loan` */

insert  into `t_loan`(`loan_id`,`loan_name`,`loan_datetime`,`loan_year`,`interest_rates`,`loan_money`,`loan_source`,`loan_desc`,`loan_createtime`,`loan_user_id`) values 
('0000000071d7cd320171f7f0b3130000','测试借款','2020-05-09 00:00:00',6,0.97,10000,'支付宝','测试借款','2020-05-09 13:37:59',2),
('40286f81700aa60c01700aceef870000','分期手机','2020-02-03 00:00:00',2,0.24,2000,'朋友','为了手机，借了借了','2020-02-03 19:28:18',2),
('4028812062b41f6e0162b43577ff0000','贷款','2019-12-04 00:00:00',1,1,1000,'111','1111','2019-12-04 18:17:06',2);

/*Table structure for table `t_manager` */

DROP TABLE IF EXISTS `t_manager`;

CREATE TABLE `t_manager` (
  `manager_id` varchar(255) NOT NULL,
  `manager_name` varchar(255) DEFAULT NULL,
  `manager_pwd` varchar(255) DEFAULT NULL,
  `manager_phone` varchar(255) DEFAULT NULL,
  `manager_sex` char(1) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `idcard` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`manager_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_manager` */

insert  into `t_manager`(`manager_id`,`manager_name`,`manager_pwd`,`manager_phone`,`manager_sex`,`birthday`,`idcard`,`address`) values 
('1','admin','767c7058518071829df8da11384dd846','12345678910','男','1997-11-24 00:00:00','11111122224455668','河北省保定市唐县');

/*Table structure for table `t_menu` */

DROP TABLE IF EXISTS `t_menu`;

CREATE TABLE `t_menu` (
  `menu_id` varchar(255) NOT NULL,
  `menu_name` varchar(255) DEFAULT NULL,
  `menu_code` varchar(255) DEFAULT NULL,
  `menu_desc` varchar(255) DEFAULT NULL,
  `menu_page` varchar(255) DEFAULT NULL,
  `zindex` int(11) DEFAULT NULL,
  `generatemenu` varchar(255) DEFAULT NULL,
  `pid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`menu_id`),
  KEY `FKn7jhkriaf11nvfin60cysoibs` (`pid`),
  CONSTRAINT `FKjh2khbh77ljauns27vxk6ed09` FOREIGN KEY (`pid`) REFERENCES `t_menu` (`menu_id`),
  CONSTRAINT `FKn7jhkriaf11nvfin60cysoibs` FOREIGN KEY (`pid`) REFERENCES `t_menu` (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_menu` */

insert  into `t_menu`(`menu_id`,`menu_name`,`menu_code`,`menu_desc`,`menu_page`,`zindex`,`generatemenu`,`pid`) values 
('2c91655e70ddd4df0170dde770f20000','数据库监控','druid','数据库监控','',9,'1',NULL),
('40286f81708f6a2801708f75c40d0000','账单账本条目统计','statistics','可视化报表显示','ManagerAction_reportAll.action',3,'1','40288a81629643ba0162964467590000'),
('40286f8170c3cb6e0170c3cb6e5e0000','Solr索引维护','solr','Solr索引维护',NULL,8,'1',NULL),
('40286f8170c3d0210170c3d021c30000','索引列表','solr-list','索引列表','SolrAction_list.action',7,'1','40286f8170c3cb6e0170c3cb6e5e0000'),
('40286f8170c3f7b80170c3f7b85d0000','一键导入索引','solr-add','一键导入索引','SolrAction_importIndex.action',2222,'0','40286f8170c3d0210170c3d021c30000'),
('40286f8170c3f7fc0170c3f7fcf60000','删除索引','solr-delete','删除索引','SolrAction_delete.action',2222,'0','40286f8170c3d0210170c3d021c30000'),
('40286f8170ddf0740170ddf074930000','DruidMonitor','druid-index','DruidMonitor','druid/index.html',8,'1','2c91655e70ddd4df0170dde770f20000'),
('4028812062b460750162b464e0f20000','用户管理 ','user','用户管理 ','',13,'1',NULL),
('40288a81629118a20162911b86be0000','用户列表','user-list','用户列表','page_admin_user_list.action',12,'1','4028812062b460750162b464e0f20000'),
('40288a8162913c080162913c2b640000','交流大区管理','region','交流大区管理',NULL,11,'1',NULL),
('40288a81629141dd0162914ec2550000','交流大区列表','region-list','交流大区列表','page_admin_region_list.action',4,'1','40288a8162913c080162913c2b640000'),
('40288a81629141dd0162914f75ff0001','添加交流大区','region-add','添加交流大区','page_admin_region_add.action',5,'1','40288a8162913c080162913c2b640000'),
('40288a81629141dd01629150cef40002','主题帖管理','topic','主题帖管理',NULL,10,'1',NULL),
('40288a81629141dd01629151c3a20003','主题帖列表','topic-list','主题帖列表','page_admin_topic_list.action',7,'1','40288a81629141dd01629150cef40002'),
('40288a81629141dd016291534beb0004','主题待审核','topic-confirm-page','主题待审核','page_admin_topic_waitconfirm.action',8,'1','40288a81629141dd01629150cef40002'),
('40288a81629141dd0162915438380005','回复管理','reply','回复管理',NULL,9,'1',NULL),
('40288a81629141dd01629155385d0006','回复待审核','reply-confirm-page','回复待审核','page_admin_reply_confirm.action',10,'1','40288a81629141dd0162915438380005'),
('40288a81629141dd01629159c3150007','修改等级','user-level','修改等级','UserAction_editLevel.action',2222,'0','40288a81629118a20162911b86be0000'),
('40288a81629141dd0162915a7b6d0008','封禁用户','user-stop','封禁用户','UserAction_stop.action',2222,'0','40288a81629118a20162911b86be0000'),
('40288a81629141dd0162915b18ae0009','解封用户','user-open','解封用户','UserAction_open.action',2222,'0','40288a81629118a20162911b86be0000'),
('40288a81629141dd0162915c3f4b000a','删除主题帖','topic-delete','删除主题帖','TopicAction_deleteBatch.action',2222,'0','40288a81629141dd01629151c3a20003'),
('40288a81629141dd0162915da057000b','恢复删除主题帖','topic-undelete','恢复删除主题帖','TopicAction_noDelete.action',2222,'0','40288a81629141dd01629151c3a20003'),
('40288a81629141dd0162915e6d8a000c','主题帖加精','topic-good','主题帖加精','TopicAction_good.action',2222,'0','40288a81629141dd01629151c3a20003'),
('40288a81629141dd0162915f16f9000d','取消加精','topic-nogood','取消加精','TopicAction_noGood.action',2222,'0','40288a81629141dd01629151c3a20003'),
('40288a81629141dd0162915fa5fc000e','主题帖置顶','topic-top','主题帖置顶','TopicAction_top.action',2222,'0','40288a81629141dd01629151c3a20003'),
('40288a81629141dd016291606a45000f','取消置顶','topic-notop','取消置顶','TopicAction_noTop.action',2222,'0','40288a81629141dd01629151c3a20003'),
('40288a81629141dd01629164ced40010','结贴','topic-end','结贴','TopicAction_adminend.action',2222,'0','40288a81629141dd01629151c3a20003'),
('40288a81629141dd01629165926f0011','取消结贴','topic-unend','取消结贴','TopicAction_unend.action',2222,'0','40288a81629141dd01629151c3a20003'),
('40288a81629141dd016291665d390012','通过审核','topic-confirm','通过审核','TopicAction_confirm.action',2222,'0','40288a81629141dd01629151c3a20003'),
('40288a81629141dd0162916711ec0013','通过审核','reply-confirm','通过审核','ReplyAction_confirm.action',2222,'0','40288a81629141dd0162915438380005'),
('40288a81629643ba0162964467590000','账单统计分析','account','账单使用情况分析',NULL,9,'1',NULL),
('40288a81629643ba0162964bd3fd0005','管理员管理','manager','管理员管理',NULL,8,'1',NULL),
('40288a81629643ba0162964ca2630006','管理员列表','manager-list','管理员列表','page_admin_manager_list.action',4,'1','40288a81629643ba0162964bd3fd0005'),
('40288a81629643ba0162964d2cb00007','管理员添加','manager-add','管理员添加','page_admin_manager_add.action',4,'1','40288a81629643ba0162964bd3fd0005'),
('40288a81629643ba0162964d86c90008','角色管理','role','角色管理',NULL,6,'1',NULL),
('40288a81629643ba0162964e128a0009','角色列表','role-list','角色列表','page_admin_role_list.action',5,'1','40288a81629643ba0162964d86c90008'),
('40288a81629643ba0162964ed726000a','添加角色','role-add','添加角色','page_admin_role_add.action',5,'1','40288a81629643ba0162964d86c90008'),
('40288a81629643ba0162964f59bf000b','权限管理','menu','权限管理',NULL,7,'1',NULL),
('40288a81629643ba0162964ff3c3000c','权限列表','menu-list','权限列表','page_admin_menu_list.action',6,'1','40288a81629643ba0162964f59bf000b'),
('40288a81629643ba01629650b505000d','添加权限','menu-add','添加权限','page_admin_menu_add.action',6,'1','40288a81629643ba0162964f59bf000b'),
('40288a816297294a0162972aef370000','修改交流大区','region-edit','修改交流大区','RegionAction_update.action',2222,'0','40288a8162913c080162913c2b640000'),
('40288a8162972bef0162972cf5e00000','删除交流大区','region-delete','删除交流大区','RegionAction_delete.action',22222,'0','40288a8162913c080162913c2b640000'),
('40288a816297478401629749042e0000','修改权限','menu-update','修改权限','MenuAction_update.action',2222,'0','40288a81629643ba0162964f59bf000b'),
('40288a816297508a0162975231180000','修改管理员信息','manager-update','修改管理员信息','ManagerAction_update,ManagerAction_foredit.action',2222,'0','40288a81629643ba0162964bd3fd0005'),
('40288a81629759680162975b4d3c0000','修改角色','role-update','修改角色','RoleAction_update.action,RoleAction_foredit.action',2222,'0','40288a81629643ba0162964d86c90008');

/*Table structure for table `t_outaccount` */

DROP TABLE IF EXISTS `t_outaccount`;

CREATE TABLE `t_outaccount` (
  `outaccount_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `outaccount_datetime` datetime DEFAULT NULL,
  `outaccount_money` double DEFAULT NULL,
  `outaccount_desc` varchar(255) DEFAULT NULL,
  `outaccount_createtime` datetime DEFAULT NULL,
  `out_user_id` bigint(20) DEFAULT NULL,
  `out_type_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`outaccount_id`),
  KEY `FK4ycmfb8yx9m02a0rpoei5icvq` (`out_user_id`),
  KEY `FK99cr9qskvvruni8j1uhx2mehk` (`out_type_id`),
  CONSTRAINT `FK4ycmfb8yx9m02a0rpoei5icvq` FOREIGN KEY (`out_user_id`) REFERENCES `t_user` (`user_id`),
  CONSTRAINT `FK99cr9qskvvruni8j1uhx2mehk` FOREIGN KEY (`out_type_id`) REFERENCES `t_outaccounttype` (`outaccounttype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

/*Data for the table `t_outaccount` */

insert  into `t_outaccount`(`outaccount_id`,`outaccount_datetime`,`outaccount_money`,`outaccount_desc`,`outaccount_createtime`,`out_user_id`,`out_type_id`) values 
(2,'2020-02-05 00:00:00',300,'和康总吃饭','2020-02-05 19:33:18',1,2),
(41,'2020-02-03 00:00:00',10,'零食','2020-02-03 15:30:11',5,5),
(44,'2019-12-27 00:00:00',19.2,'超市购物','2019-12-27 15:27:25',2,1),
(45,'2019-12-27 00:00:00',7.5,'炒饭','2019-12-27 12:33:18',2,1),
(46,'2019-12-27 00:00:00',2.3,'饮料','2019-12-27 17:44:50',2,1),
(47,'2019-12-28 00:00:00',10.5,'炒饭','2019-12-28 19:58:42',2,3),
(49,'2019-12-25 00:00:00',110,'约会','2019-12-25 23:09:47',1,2),
(51,'2019-12-30 00:00:00',10,'1111','2019-12-30 13:27:43',2,1),
(52,'2019-12-30 00:00:00',20,'22','2019-12-30 14:25:42',2,1),
(53,'2019-12-30 00:00:00',10,'1111','2019-12-30 14:28:06',2,1),
(54,'2019-12-30 00:00:00',20,'22','2019-12-30 14:33:05',2,1),
(55,'2020-02-08 00:00:00',50,'每个月基本上大概50左右','2020-02-08 15:33:18',2,4),
(56,'2019-11-14 00:00:00',9.6,'2019-学校伙食测试数据','2020-02-29 13:08:38',2,6),
(57,'2020-03-03 00:00:00',181.33,'分期付款第7期','2020-03-03 08:02:52',2,1),
(58,'2020-03-03 00:00:00',5.88,'金针菇一包','2020-03-03 08:04:36',2,3),
(59,'2020-03-03 00:00:00',50,'日常电费支出','2020-03-03 08:06:17',2,4),
(60,'2020-03-07 00:00:00',181.33,'花呗分期还款','2020-03-08 17:28:44',2,1),
(61,'2020-03-12 00:00:00',10,'日常支出消费10元！！！','2020-03-12 19:30:07',2,3),
(62,'2020-04-01 00:00:00',110,'四月支付宝支出测试','2020-03-22 19:07:02',2,1),
(63,'2020-04-02 00:00:00',500,'四月支出测试房租','2020-03-22 19:07:45',2,10),
(64,'2020-03-28 00:00:00',66,'测试出行数据','2020-03-28 08:11:47',2,11),
(65,'2020-04-16 00:00:00',11,'dewd ','2020-04-05 10:29:07',5,5),
(66,'2020-05-02 00:00:00',181,'Alipay','2020-05-02 18:03:07',2,1),
(67,'2020-05-03 00:00:00',11,'test','2020-05-03 08:57:31',2,6),
(68,'2020-05-03 00:00:00',11,'test','2020-05-04 15:09:57',2,4);

/*Table structure for table `t_outaccounttype` */

DROP TABLE IF EXISTS `t_outaccounttype`;

CREATE TABLE `t_outaccounttype` (
  `outaccounttype_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `outaccounttype_name` varchar(255) DEFAULT NULL,
  `outaccounttype_createtime` datetime DEFAULT NULL,
  `outaccounttype_desc` varchar(255) DEFAULT NULL,
  `ot_user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`outaccounttype_id`),
  KEY `FKfd355iflto4u5dkb4aknhktvn` (`ot_user_id`),
  CONSTRAINT `FKfd355iflto4u5dkb4aknhktvn` FOREIGN KEY (`ot_user_id`) REFERENCES `t_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `t_outaccounttype` */

insert  into `t_outaccounttype`(`outaccounttype_id`,`outaccounttype_name`,`outaccounttype_createtime`,`outaccounttype_desc`,`ot_user_id`) values 
(1,'支付宝','2019-12-25 16:22:33','alipay-update',2),
(2,'餐饮','2019-12-25 16:24:42','eat~~~',1),
(3,'微信','2019-12-27 10:26:31','wxpay',2),
(4,'电费','2019-12-27 10:26:49','This is 电费',2),
(5,'伙食','2019-12-27 18:45:49','This is 伙食',5),
(6,'学校伙食','2019-12-30 19:50:02','日常吃饭开销',2),
(8,'微信','2020-01-30 09:56:06','微信支付',7),
(9,'吃饭','2020-02-16 09:39:04','日常进食支出！！！',8),
(10,'房租','2020-03-13 16:11:44','每月一交的房租',2),
(11,'出行','2020-03-28 08:10:22','出行这一块',2),
(12,'娱乐','2020-03-28 09:03:36','测试娱乐支出类型',2),
(13,'医疗','2020-03-28 09:31:44','测试医疗支出类型',2),
(14,'奖金','2020-04-10 22:20:10','测试奖金',12);

/*Table structure for table `t_region` */

DROP TABLE IF EXISTS `t_region`;

CREATE TABLE `t_region` (
  `region_id` int(11) NOT NULL AUTO_INCREMENT,
  `region_name` varchar(255) DEFAULT NULL,
  `region_desc` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`region_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `t_region` */

insert  into `t_region`(`region_id`,`region_name`,`region_desc`) values 
(1,'系统使用区','家庭记账管理系统-系统使用区'),
(2,'用户交流区','富于智慧和温馨的互助乐园，快来一起交流吧!'),
(3,'用户反馈区','发现系统的Bug，一起来反馈！');

/*Table structure for table `t_reply` */

DROP TABLE IF EXISTS `t_reply`;

CREATE TABLE `t_reply` (
  `reply_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `reply_content` text,
  `reply_datetime` datetime DEFAULT NULL,
  `zan` int(11) DEFAULT NULL,
  `bad` int(11) DEFAULT NULL,
  `reply_user_id` bigint(20) DEFAULT NULL,
  `reply_topic_id` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`reply_id`),
  KEY `FKlouafjjkyf07j6agiiyyaxyri` (`reply_user_id`),
  KEY `FKhy1i047m2ghabs5n52djt5dup` (`reply_topic_id`),
  CONSTRAINT `FKhy1i047m2ghabs5n52djt5dup` FOREIGN KEY (`reply_topic_id`) REFERENCES `t_topic` (`topic_id`),
  CONSTRAINT `FKlouafjjkyf07j6agiiyyaxyri` FOREIGN KEY (`reply_user_id`) REFERENCES `t_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

/*Data for the table `t_reply` */

insert  into `t_reply`(`reply_id`,`reply_content`,`reply_datetime`,`zan`,`bad`,`reply_user_id`,`reply_topic_id`,`status`) values 
(4,'This is for test<!---->','2020-01-30 10:34:17',1,0,8,3,1),
(5,'This is for test---<span style=\"background-color:#FF9900;\">xiaokang</span><!---->','2020-02-06 18:33:57',0,0,7,4,1),
(6,'<img src=\"/FamilyAccount/js/kindeditor/plugins/emoticons/images/44.gif\" alt=\"\" border=\"0\" />测试一下子<!---->','2020-02-16 13:11:52',0,0,11,3,1),
(7,'<img src=\"/FamilyAccount/upload/1582110176000.jpg\" alt=\"\" /><!---->','2020-02-19 19:03:09',0,1,2,3,0),
(8,'<img src=\"/FamilyAccount/upload/1583139416661.jpg\" alt=\"\" /><!---->','2020-03-02 16:57:21',1,11,2,8,0),
(9,'<img src=\"/FamilyAccount/upload/1583139798245.jpg\" alt=\"\" /><!---->','2020-03-02 17:03:27',1,10,2,8,0),
(10,'<img src=\"/FamilyAccount/upload/1583140490920.jpg\" alt=\"\" /><!---->','2020-03-02 17:15:10',0,0,2,8,0),
(12,'<img src=\"/FamilyAccount/upload/image/20200311/20200311090246_23.jpg\" alt=\"\" />图片测试<!---->','2020-03-11 09:03:06',0,0,2,3,0),
(14,'<img src=\"/FamilyAccount/upload/image/20200315/20200315112702_839.png\" alt=\"\" />测试一下子<!---->','2020-03-15 11:27:14',0,0,2,3,0),
(15,'路过留赞','2020-03-26 07:11:42',0,0,2,3,0),
(16,'测试一波<img src=\"http://localhost:8080/FamilyAccount/js/kindeditor/plugins/emoticons/images/20.gif\" alt=\"\" border=\"0\" /><br />\r\n<div>\r\n	<!----><!----><!---->\r\n</div>','2020-04-24 18:33:10',0,0,2,14,0),
(17,'回复测试一下','2020-04-25 15:38:47',0,0,12,10,0),
(18,'<p>\r\n	<img src=\"/FamilyAccount/upload/image/20200425/20200425171957_118.jpg\" alt=\"\" />\r\n</p>\r\n<p>\r\n	bigdata、cloud\r\n</p>','2020-04-25 17:20:13',0,0,2,4,0),
(19,'测试回复一波<img src=\"http://localhost:8080/FamilyAccount/js/kindeditor/plugins/emoticons/images/28.gif\" border=\"0\" alt=\"\" />','2020-04-25 17:22:19',0,0,2,13,0),
(20,'测试留言<img src=\"http://localhost:8080/FamilyAccount/js/kindeditor/plugins/emoticons/images/0.gif\" border=\"0\" alt=\"\" />','2020-05-02 17:46:46',2,0,24,3,0),
(21,'留言一波<img src=\"http://familyaccount.xiaokang.cool/js/kindeditor/plugins/emoticons/images/4.gif\" border=\"0\" alt=\"\" />','2020-05-02 18:26:51',1,0,2,3,0),
(22,'<img src=\"http://familyaccount.xiaokang.cool/js/kindeditor/plugins/emoticons/images/1.gif\" alt=\"\" border=\"0\" />\r\n<div>\r\n	<!----><!----><!---->\r\n</div>','2020-05-03 09:12:46',0,0,2,3,0);

/*Table structure for table `t_role` */

DROP TABLE IF EXISTS `t_role`;

CREATE TABLE `t_role` (
  `role_id` varchar(255) NOT NULL,
  `role_name` varchar(20) DEFAULT NULL,
  `role_desc` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_role` */

insert  into `t_role`(`role_id`,`role_name`,`role_desc`) values 
('4028812062b975760162c393ba210000','超级管理员','这是超级管理员');

/*Table structure for table `t_topic` */

DROP TABLE IF EXISTS `t_topic`;

CREATE TABLE `t_topic` (
  `topic_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `topic_title` varchar(255) DEFAULT NULL,
  `topic_content` text,
  `topic_datetime` datetime DEFAULT NULL,
  `is_top` char(1) DEFAULT NULL,
  `is_good` char(1) DEFAULT NULL,
  `is_end` char(1) DEFAULT NULL,
  `look_count` int(11) DEFAULT NULL,
  `topic_user_id` bigint(20) DEFAULT NULL,
  `topic_region_id` int(11) DEFAULT NULL,
  `topic_zan` int(11) DEFAULT NULL,
  `topic_bad` int(11) DEFAULT NULL,
  `del` char(1) DEFAULT NULL,
  `solrDel` char(1) DEFAULT '0',
  PRIMARY KEY (`topic_id`),
  KEY `FKlp5rhp1513qrmf594su8iypd3` (`topic_user_id`),
  KEY `FKaiod5khtalh6c2c7i39oo3fvh` (`topic_region_id`),
  CONSTRAINT `FKaiod5khtalh6c2c7i39oo3fvh` FOREIGN KEY (`topic_region_id`) REFERENCES `t_region` (`region_id`),
  CONSTRAINT `FKlp5rhp1513qrmf594su8iypd3` FOREIGN KEY (`topic_user_id`) REFERENCES `t_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `t_topic` */

insert  into `t_topic`(`topic_id`,`topic_title`,`topic_content`,`topic_datetime`,`is_top`,`is_good`,`is_end`,`look_count`,`topic_user_id`,`topic_region_id`,`topic_zan`,`topic_bad`,`del`,`solrDel`) values 
(1,'喂喂喂','大萨达撒','2020-01-12 20:38:55','1','1','0',9,2,1,0,0,'1','0'),
(2,'关于系统介绍','<span style=\"font-size:18px;\"><b>家庭记账管理系统</b></span>','2020-01-12 20:47:12',NULL,NULL,'0',6,2,1,0,0,'1','0'),
(3,'系统介绍','家庭记账管理系统介绍\r\n<h3 id=\"h-1\" style=\"color:inherit;font-weight:bold;font-size:1em;\">\r\n	<span style=\"font-size:inherit;line-height:inherit;background:#0074D9 none repeat scroll 0% 0%;color:#FFFFFF;\">支出账单</span>\r\n</h3>\r\n<p style=\"font-size:inherit;color:inherit;\">\r\n	<strong>支出账单的添加、删除、修改、查看、搜索、查看报表，对支出账单类型进行维护</strong>\r\n</p>\r\n<h3 id=\"h-2\" style=\"color:inherit;font-weight:bold;font-size:1em;\">\r\n	<span style=\"font-size:inherit;line-height:inherit;background:#0074D9 none repeat scroll 0% 0%;color:#FFFFFF;\">收入账单</span>\r\n</h3>\r\n<p style=\"font-size:inherit;color:inherit;\">\r\n	<strong>收入账单的添加、删除、修改、查看、搜索、查看报表，对收入账单类型进行维护</strong>\r\n</p>\r\n<h3 id=\"h-3\" style=\"color:inherit;font-weight:bold;font-size:1em;\">\r\n	<span style=\"font-size:inherit;line-height:inherit;background:#0074D9 none repeat scroll 0% 0%;color:#FFFFFF;\">我的账本（家庭户主）</span>\r\n</h3>\r\n<p style=\"font-size:inherit;color:inherit;\">\r\n	<strong>可以对投资理财、借款还贷进行添加、删除、修改、查看、搜索</strong>\r\n</p>\r\n<h3 id=\"h-4\" style=\"color:inherit;font-weight:bold;font-size:1em;\">\r\n	<span style=\"font-size:inherit;line-height:inherit;background:#0074D9 none repeat scroll 0% 0%;color:#FFFFFF;\">用户交流区</span>\r\n</h3>\r\n<p style=\"font-size:inherit;color:inherit;\">\r\n	<strong>用户可以在不同大区进行发帖、回复、浏览，并且用户可以查看自己发的帖子并修改、删除和结贴，有回复时用户会收到动态消息，用户可以查看自己已经回复的内容。</strong>\r\n</p>','2020-01-12 20:49:50','1','1','0',163,2,1,27,0,'0','0'),
(4,'图片上传测试','<img src=\"/FamilyAccount/upload/1522676909389.jpg\" alt=\"\" />','2020-01-12 21:48:48',NULL,NULL,'0',35,2,2,1,4,'0','0'),
(5,'测试','微信公众号：<span style=\"color:#E53333;\">小康新鲜事儿</span><!---->','2020-01-27 19:01:16','0','0','1',11,7,2,3,2,'0','0'),
(6,'功能基本上都还可以','大大大大大大<!---->','2020-01-30 11:28:03','0','0','1',4,7,2,0,0,'0','0'),
(7,'小康使用体验','<p>\r\n	家庭成员可以进行收入支出的添加、修改以及查看报表，家庭户主在前者基础上还可以有投资理财和借款还贷功能。\r\n</p>\r\n<h1>\r\n	<span style=\"background-color:#FF9900;color:#E53333;\"><strong>功能正在完善中！！！<img src=\"http://localhost:8080/FamilyAccount/js/kindeditor/plugins/emoticons/images/47.gif\" alt=\"\" border=\"0\" /></strong></span>\r\n</h1>\r\n<p>\r\n	<!---->\r\n</p>','2020-02-16 13:18:16','0','0','0',2,11,1,0,0,'0','0'),
(8,'反馈Bug','<h3 align=\"left\">\r\n	整体功能还可以，图片上传马上<span style=\"background-color:#FF9900;\">测试</span>哦！！！\r\n</h3>\r\n<div id=\"translate-man-app\" class=\"content-3WfBL_0\" style=\"background-color:#FFFFFF;\">\r\n	<div class=\"outputBox-qe9A4_0\">\r\n		<div class=\"outputBox-3oESn_0\">\r\n			<span class=\"outputBox-13Ovx_0\">测试</span><!---->\r\n		</div>\r\n		<div class=\"outputBox-1GLb__0\">\r\n			<div class=\"outputBox-it-2__0\">\r\n				[Cèshì]\r\n			</div>\r\n			<div class=\"outputBox-onVZH_0\">\r\n				<img src=\"moz-extension://cf2a1d87-66fe-4d7a-bbd5-4c8e52e9b1f5/static/sound.svg\" class=\"icon-tprjJ_0\" />\r\n			</div>\r\n		</div>\r\n		<div class=\"outputBox-2sJgr_0\">\r\n			<div class=\"outputBox-GomOI_0\">\r\n				<span class=\"outputBox-1Xaaq_0\">名词</span><span class=\"outputBox-2liU7_0\">test; examination</span>\r\n			</div>\r\n		</div>\r\n		<div class=\"outputBox-17RAm_0\">\r\n			<!----><!----><!---->\r\n			<div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>','2020-03-02 16:02:29','0','0','0',36,2,3,0,5,'0','0'),
(9,'测试图片上传！','<img src=\"/FamilyAccount/upload/image/20200308/20200308094833_143.jpg\" alt=\"\" />微信公众号：小康新鲜事儿<!---->','2020-03-08 09:49:57','0','0','0',5,2,2,0,0,'0','0'),
(10,'图片测试111','<img src=\"/FamilyAccount/upload/image/20200308/20200308095624_110.png\" alt=\"\" /><!---->','2020-03-08 09:57:12','0','0','0',7,2,2,0,0,'0','0'),
(11,'图片上传2222','<img src=\"/FamilyAccount/upload/image/20200308/20200308101024_840.jpg\" alt=\"\" />测试一下图片宽度<!---->','2020-03-08 10:11:12','0','0','0',3,2,2,0,0,'0','1'),
(12,'dom4j','<img src=\"/FamilyAccount/upload/image/20200308/20200308102028_898.png\" alt=\"\" /><!---->','2020-03-08 10:20:43','0','0','0',1,2,2,0,0,'1','0'),
(13,'大大大大大大','<p>\r\n	<img src=\"/FamilyAccount/upload/image/20200308/20200308103839_196.png\" alt=\"\" />\r\n</p>\r\n<p>\r\n	测试图片大小<br />\r\n<!---->\r\n</p>','2020-03-08 10:39:10','0','0','0',7,2,2,0,0,'0','1'),
(14,'图片上传33333','<p>\r\n	<img src=\"/FamilyAccount/upload/image/20200308/20200308104059_261.png\" alt=\"\" />\r\n</p>\r\n<p>\r\n	3333333333<br />\r\n<!---->\r\n</p>','2020-03-08 10:41:13','0','0','0',7,2,2,0,23,'1','0'),
(15,'多图测试','<p>\r\n	<img src=\"/FamilyAccount/upload/image/20200308/20200308104233_367.jpg\" alt=\"\" /><img src=\"/FamilyAccount/upload/image/20200308/20200308104233_516.jpg\" alt=\"\" /><img src=\"/FamilyAccount/upload/image/20200308/20200308104233_377.png\" alt=\"\" />\r\n</p>\r\n<p>\r\n	多图上传测试中<br />\r\n<!---->\r\n</p>','2020-03-08 10:43:15','0','0','1',21,2,2,0,0,'0','0');

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `idcard` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `realname` varchar(255) DEFAULT NULL,
  `user_phone` varchar(255) DEFAULT NULL,
  `user_sex` char(1) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `register_date` datetime DEFAULT NULL,
  `face` varchar(255) DEFAULT NULL,
  `state` char(1) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `loginDate` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

/*Data for the table `t_user` */

insert  into `t_user`(`user_id`,`user_name`,`user_password`,`idcard`,`address`,`birthday`,`realname`,`user_phone`,`user_sex`,`email`,`register_date`,`face`,`state`,`level`,`loginDate`) values 
(1,'family','5dec22551c666529e3af4908533fdfa1',NULL,NULL,NULL,NULL,'15380618292','男','1852435678@qq.com','2018-03-21 23:20:04','img/userface/user1.gif','1',0,'2020-04-20 17:53:45'),
(2,'xiaokangxxs','7f7ae06f25a2124da4990a346722ff7d','1234','河北省保定市唐县西雹水村委会','1997-11-24 00:00:00','小康','15832235513','男','xiaokang.188@qq.com','2020-01-20 23:20:07','img/userface/user19.gif','0',1,'2020-05-09 13:41:21'),
(3,'k1583223','d8c16ea3a9aef5116800f15457e66037',NULL,NULL,NULL,NULL,'13062538890',NULL,'835663101@qq.com','2018-03-21 23:45:51','img/userface/user13.gif','0',0,'2020-03-18 15:33:18'),
(4,'g1500813439','12e0ee1a6c4151984db21b5fbe519e42',NULL,NULL,NULL,NULL,'15077880393',NULL,'1500813439@qq.com','2018-03-22 11:46:44','img/userface/user13.gif','0',1,'2020-02-19 08:20:05'),
(5,'g1379370111','e1e3ba9128a44da1afe38f487fde07f6',NULL,NULL,NULL,NULL,'18251978317',NULL,'1379370111@qq.com','2018-03-22 18:41:05','img/userface/user0.gif','0',1,'2020-02-19 14:38:18'),
(6,'z544158188','5f57a064cf5416bb007b1b1fba86607c',NULL,NULL,NULL,NULL,'17798531336',NULL,'544158188@qq.com','2018-03-23 18:39:33','img/userface/user1.gif','0',0,'2020-04-25 15:36:31'),
(7,'xiaokang','7e2547da38bb28998e9d92c1f5f46ddd','111111222244556688','唐县','1997-11-24 00:00:00','小康','12332235513','男','605656286@qq.com','2020-01-27 18:39:45','img/userface/user0.gif','0',1,'2020-05-09 13:45:28'),
(8,'z1029181448','2bb7cd482fb5dc6a1a6e22ab6f31a404','111111222244556688','雹水乡西雹水村','2020-01-30 00:00:00','小康','12332235513','男','1029181448@qq.com','2020-01-30 10:24:08','img/userface/user15.gif','0',0,'2020-02-19 07:42:21'),
(9,'ll765268279','039b514d35e4b90a1aab77b1c97b3a97','11111122224455668','河北省张家口市宣化区胜利路30号院1号楼','1997-11-24 00:00:00','小康','12332235513',NULL,'765268279@qq.com','2020-02-16 12:08:09','img/userface/user0.gif','0',1,'2020-02-19 07:46:33'),
(10,'j1171653263','88d8bce33f4a0ddc0634898b4f2b3532','11111122224455668','河北省张家口市经济开发区长城西大街5号','2020-02-18 00:00:00','ddddddd','12332235513','女','1171653263@qq.com','2020-02-16 12:24:29','img/userface/user10.gif','0',1,'2020-02-20 17:01:14'),
(11,'h1047055548','48b4f4ba5ec5dda0fd945d4cafc8b709','11111122224455668','板正北大街与沟中街交叉口西南100米','2020-02-18 00:00:00','dasdfsf','15832235513',NULL,'1047055548@qq.com','2020-02-16 12:29:23','img/userface/user4.gif','0',1,'2020-02-18 20:33:31'),
(12,'xk1997','3048844548369308a12d018bd2efd965','110101200006024892','河北省保定市唐县西雹水村委会','2000-06-02 00:00:00','xiaokang','15832235513','男','308632747@qq.com','2020-02-24 15:22:44','img/userface/user0.gif','0',0,'2020-05-03 09:14:38'),
(13,'l2537844686','471fe100bd5a4efb9f61d582fff48418',NULL,NULL,NULL,NULL,'12332235513',NULL,'2537844686@qq.com','2020-02-24 16:02:13','img/userface/user13.gif','0',1,'2020-02-24 16:03:01'),
(14,'w2351444574','90f071b77d9c2dda6c8eaac61edf16d7',NULL,NULL,NULL,NULL,'12332235513',NULL,'2351444574@qq.com','2020-02-24 16:06:23','img/userface/user13.gif','0',0,'2020-02-24 16:06:43'),
(15,'y3386664952','3b7321ca9602b1becca7503292d9f621',NULL,NULL,NULL,NULL,'12332235513',NULL,'3386664952@qq.com','2020-02-24 16:11:52','img/userface/user13.gif','0',0,'2020-02-24 16:16:54'),
(16,'z2365845217','3053d5700bc9042bf1fa591033b04497',NULL,NULL,NULL,NULL,'12332235513',NULL,'2365845217@qq.com','2020-02-24 16:13:18','img/userface/user13.gif','0',0,'2020-02-24 16:18:54'),
(17,'n2918880896','bc2b86a548bcb494cc38536bfd00627b',NULL,NULL,NULL,NULL,'12332235513',NULL,'2918880896@qq.com','2020-02-24 16:15:42','img/userface/user13.gif','0',0,'2020-02-24 16:22:54'),
(18,'xiaokangxxs1','ed5ae0b48124f9f0f6184a04ad14f72a',NULL,NULL,NULL,NULL,'12332235513',NULL,'1955107359@qq.com','2020-03-01 09:14:48','img/userface/user13.gif','0',0,NULL),
(19,'c3338028998','a2c827ebe091070d10a8fe4cc5e8d729',NULL,NULL,NULL,NULL,'12532235513',NULL,'3338028998@qq.com','2020-03-02 14:59:57','img/userface/user13.gif','0',0,NULL),
(20,'l1977583862','0a2d376ced8c07fd8b560bafe3855d35',NULL,NULL,NULL,NULL,'12532235513',NULL,'1977583862@qq.com','2020-03-02 15:13:53','img/userface/user0.gif','0',1,NULL),
(21,'y3110368688','cbf6bdba0a834e6b0fbdaa0cf6eca441',NULL,NULL,NULL,NULL,'12532235513',NULL,'3110368688@qq.com','2020-03-03 13:49:38','img/userface/user15.gif','0',0,'2020-03-03 13:50:24'),
(22,'A123456','a602aeed7d860eb12c7ffe58c1a23148',NULL,NULL,NULL,NULL,'18849333906',NULL,'1500819493@qq.com','2020-03-11 09:09:03','img/userface/user15.gif','0',0,NULL),
(23,'username','5666eb00a3d34ed7fbbd39b27bee8f57',NULL,NULL,NULL,NULL,'13603263576',NULL,'aa544158188@qq.com','2020-04-13 16:31:10','img/userface/user0.gif','0',1,NULL),
(24,'xiaokangxxs1111','872d0937b86f75ae0bfe32e12f28fe17',NULL,NULL,NULL,NULL,'15832235513',NULL,'xiaokang.1888@qq.com','2020-05-02 17:43:44','img/userface/user15.gif','0',0,'2020-05-02 17:48:50'),
(25,'xiaokangxxs2222','332856d7e7d491bbe4452e2781fec592',NULL,NULL,NULL,NULL,'15832235513',NULL,'xiaokang.18888@qq.com','2020-05-02 17:49:27','img/userface/user0.gif','0',1,'2020-05-02 17:52:33'),
(26,'dasfafda','5af59dd7170d72c304565a381bf4ab52',NULL,NULL,NULL,NULL,'15832235513',NULL,'xiaokang.188881@qq.com','2020-05-02 18:22:29','img/userface/user15.gif','0',0,NULL),
(27,'adaa45','c7e4bbd213f25caab5f40ee1f9e78812',NULL,NULL,NULL,NULL,'15832235513',NULL,'xiaokang.288@qq.com','2020-05-03 08:06:48','img/userface/user15.gif','0',0,NULL),
(28,'dafdfae','c0431abad514a26ed96b134318533fdf',NULL,NULL,NULL,NULL,'15832235513',NULL,'xiaokang.388@qq.com','2020-05-03 08:50:30','img/userface/user15.gif','0',0,NULL),
(29,'xiaokangxxs444','0c6372489e3acde8a26888e43f2cd75a',NULL,NULL,NULL,NULL,'15832235513',NULL,'xiaokang.28888@qq.com','2020-05-03 10:48:49','img/userface/user15.gif','0',0,NULL),
(30,'adsfsdfaee','ce26d0e1c03d308292a559ff211048c5',NULL,NULL,NULL,NULL,'15832235513',NULL,'xiaokang.48888@qq.com','2020-05-03 10:58:31','img/userface/user15.gif','0',0,NULL),
(31,'adfadfgdasd','d60349db07500dd7e788e94d26331890',NULL,NULL,NULL,NULL,'15832235513',NULL,'xiaokang.1188@qq.com','2020-05-06 14:27:10','img/userface/user15.gif','0',0,NULL),
(32,'adfadfdde','e3285ef8751e9b0eb08f086250d472da',NULL,NULL,NULL,NULL,'15832235513',NULL,'xiaokang.1288@qq.com','2020-05-06 16:33:53','img/userface/user15.gif','0',0,NULL),
(33,'xiaokangxxs555','105b4af4b47c3956dfc4c13039cdd607',NULL,NULL,NULL,NULL,'15832235513',NULL,'xiaokang.12288@qq.com','2020-05-06 16:34:42','img/userface/user15.gif','0',0,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
